//
//  HeaderConstant.swift
//  EthOS
//
//  Created by Sujal Bandhara on 03/12/2015.
//  Copyright (c) 2015 byPeople Technologies Pvt Limited. All rights reserved.
//

//Facebook iOS Test: - Credential -
//1023479197712148

let FB_APP_ID = "1693506034219607"


//MARK: - ALERT -

let ALERT_NO_INTERNET  =  "ALERT_NO_INTERNET".localized
let ALERT_NO_INTERNET_SEARCH_AGAIN  =  "Internet connection appears to be offline.Please search again."

let ALERT_TITLE = "Dealtaps"
let ALERT_OK    = "OK"
let ALERT_CANCEL = "Cancel"
let ALERT_NO = "No"
let ALERT_YES = "Yes"

let ALERT_SETTINGS = "Settings"
let ALERT_DONT_ACCESS = "Don't Access"
let ALERT_ACCESS = "Access"

//the account exists but has bad login record
let ALERT_TURN_ON_LOCATION_FROM_APP     = "Turn On Location Service to Determine Your Location"
let ALERT_REQUIRE_GPS_TRACKING          = "Currently all tasks require GPS tracking. Confirm that you do not want to give access to your location"
let ALERT_INFORM_NOT_GPS_TRACKING       = "We will not track your location. Note, however, that until you give access to GPS (by turning it on in settings) you will not receive any tasks."
let ALERT_INFORM_NOT_PUSH_NOTIFICATION       = "We will not send you push notification. Note, however, that until you give access to Push Notification (by turning it on in settings) you will not receive any tasks."
let ALERT_TURN_ON_LOCATION_FROM_DEVICE_PRIVACY  = "\(ALERT_TITLE) doesn't work without the Location Services enabled. To turn it on, go to Settings > Privacy > Location Services > On"
let ALERT_GEOFANCING_NOT_SUPPORT_ON_DEVICE  = "Geofencing is not supported on this device!"
let ALERT_GRANT_GEOTIFY_PERMISSION          = "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location."
let ALERT_LOGOUT_INTIMATION             = "Are you sure you want to Delete Account? Type your password to allow this."
let ALERT_SESSION_TIME_OUT              = "Session Timeout. Please login again."
let ALERT_FAIL_TO_UPLOAD             = "Sorry!!! Fail to update Profile Picture."
let ALERT_404_FOUND                     = "HTTP standard response code indicating that the client was able to communicate with a given server, but the server could not find what was requested."
let ALERT_TITLE_404                     = "404 - File or directory not found"
let ALERT_ALEADY_MEMBER                 = "The phone number is already a member"
let ALERT_INVALID_PHONE_NUMBER          = "The phone number is not valid"
let ALERT_LOGOUT_CONFIRMATION           =  "Are you sure you want to logout?"

let ALERT_CAMERA_ACCESS_IS_NOT_ALLOWED = "Enable Dealtaps to use your camera in your phone settings to start scanning"
let ALERT_ENTER_A_MALL_TO_VIEW_STORE = "Please enter a mall to view stores"
let ALERT_WELCOME_MSG = "Enjoy your visit and save money with Dealtaps"
let ALERT_THANK_YOU = "Thank you for visiting"
let ALERT_HAVE_A_GREATE_DAY = "Have a great day!"
let ALERT_ACTION_OK = "OK"

//MARK: - SERVER URL's -


let BYPT_LOCAL_SERVER = "http://192.168.0.201/dealtap/"

let DEALTAP_DEV_SERVER = "http://52.35.93.122/dealtap_dev/"
let DEALTAP_PRODUCTION_SERVER = "http://52.35.93.122/dealtap/"

let DEALTAP_LIVE_SERVER = "https://manage.dealtaps.com/"
let DEALTAP_STORE_LIVE_SERVER = "https://stores.dealtaps.com/"

//let BASE_URL_IP = DEALTAP_LIVE_SERVER
//let BASE_URL_IP_FOR_DEAL = DEALTAP_STORE_LIVE_SERVER

let BASE_URL_IP = DEALTAP_DEV_SERVER
let BASE_URL_IP_FOR_DEAL = DEALTAP_DEV_SERVER

let BASE_URL = "\(BASE_URL_IP)index.php?r=api/"

let STORE_LOGO_BASE_URL = "\(BASE_URL_IP)assets/upload/avatar/store/"
let STORE_IMAGE_BASE_URL = "\(BASE_URL_IP)assets/upload/avatar/store_photo/"
let SCAN_DEAL_IMAGE_BASE_URL = "\(BASE_URL_IP_FOR_DEAL)assets/upload/avatar/qrcode_advertise/"
let CATEGORY_IMAGE_BASE_URL = "\(BASE_URL_IP)assets/upload/avatar/category_icon/"
let DEAL_IMAGE_BASE_URL = "\(BASE_URL_IP_FOR_DEAL)assets/upload/avatar/advertise/"


let CONTENT_TYPE_ENCODED = "urlencoded"
let CONTENT_TYPE_JSON    = "json"
//MARK: - API Keys -

let kAPIErrorCode                   =   "errorCode"
let kAPIUser                        =   "user"
let kAPIUserName                    =   "username"
let kAPIPassword                    =   "password"
let kAPIAccessToken                 =   "access_token"
let kAPIAgreeToGpsTracking          =   "AgreeToGpsTracking"
let kAPIAgreeToPushNotifications    =   "AgreeToPushNotifications"
let kAPIAuthToken                   =   "APIAuthToken"
let kAPIEventIdString               =   "EventId"
let kAPISocialType                  =   "SocialType"
let kAPISocialId                       =   "SocialId"
let kAPISocialToken                    =   "SocialToken"

let kAPIIsAlreadyInFavDeal         =    "isThisDealIsInFavDeal"
let kAPIDealLogicalDeletedFromFav  =    "dealLogicallyDeletedFromFav"

//MARK: MallList

let kAPIMallId                     =    "mall_id"
let kAPIMallName                   =    "mall_name"
let kAPIDistance                   =    "DISTANCE"
let kAPIRadius                     =    "radius"
let kAPIMallCategoryId             =    "category_id"

let kAPIShopNo                     =    "shopNo"
let kAPIEndTime                    =    "storeClosingTime"
let kAPIStartTime                  =    "storeStartTime"
let kAPIStorePhoto                 =    "store_photo"
let kAPIStoreId                    =    "store_id"
let kAPIStoreName                  =    "storeName"
let kAPIStoreLogo                  =    "storeLogo"
let kAPIStoreLevel                 =    "level"
let kAPIStoreMainPhoto             =    "store_main_photo"
let kAPIStoreDisplayImage          =    "DisplayImage"
let kAPIStorePhoto2                =    "store_photo2"
let kAPIStorePhoto3                =    "store_photo3"
let kAPIStoreDescription           =    "description"
let kAPIStorePhoneNumber           =    "store_phone_number"
let kAPIAccountContactNumber       =    "phone_number"

//MARK: Category

let kAPICategory                   =    "category"
let kAPICategoryIcon               =    "category_icon"
let kAPICategoryName               =    "category_name"
let kAPICategoryId                 =    "product_category_id"
let kAPICategoryAsDefault          =    "defaultCategory"

//MARK: Deal

let kAPIDealImage1                 =    "dealImage1"
let kAPIDealImage2                 =    "dealImage2"
let kAPIDealImage3                 =    "dealImage3"
let kAPIDealImage4                 =    "dealImage4"
let kAPIDealImage5                 =    "dealImage5"
let kAPIDealSaleTypeId             =    "sale_type_id"
let kAPIDealSaleType               =    "saleType"
let kAPIDealEndDate                =    "dealEndDate"
let kAPIDealStartDate              =    "dealStartDate"
let kAPIStoreClosingTime           =    "storeClosingTime"
let kAPIDealDesc                   =    "dealDesc"
let kAPIDealConstraint             =    "dealConstraint"
let kAPIScanDealId                 =    "qr_advertise_id"
let kAPISimpleDealId               =    "advertise_id"
let kAPIDealBannerTitle            =    "banner_title"

let kAPIStatus                     =    "status"
let kAPIMessage                    =    "message"
let kAPIData                       =    "data"
let kAPILatitude                   =    "latitude"
let kAPILongitude                  =    "longitude"


//MARK: Report

let kAPIUserUUID                   = "UUID"
let kAPIReportTitle                = "report_title"
let kAPIReportType                 = "report_type"
let kAPIReportDesc                 = "report_desc"
let kAPIReportDealType             = "deal_type"
let kAPIReportId                   = "report_id"
let kAPIDealId                     = "deal_id"

//MARK: QRCode Scan
let kAPIQrCOde                     = "qrcode"
let kAPIIsScanDeal                 = "isScanDeal"


let Radius                     =    200.0


//MARK: - UserDefault Key -

let USER_DEFAULT_FAV_DEALS          = "favDealList"
let USER_DEFAULT_NEAR_BY_MALL_LIST  = "nearByMallList" 

let USER_DEFAULT_AUTHENTICATION_DETAIL          =   "authenticationDetail"
let USER_DEFAULT_LOGIN_USER_DATA                =   "loginUserData"
let USER_DEFAULT_LOCATION_PERMISSION_ALERT      =   "isLocationPermissionAlertAsk"
let USER_DEFAULT_SIGNUP_ALL_STEPS_FINISHED      =   "isSignUpProcessFinish"
let USER_DEFAULT_DEVICE_CROSS_5KM_REGION        =   "DeviceCross5kmregion"
let USER_DEFAULT_OFFICE                         =   "Office"
let USER_DEFAULT_PUSH_NOTIFICATION_PERMISSION_ALERT      =   "isPushNotificationPermissionAlertAsk"
let USER_DEFAULT_ACCOUNT_SETUP_FROM_LOGIN      =   "accountSetupWithUsingLoginCredential"
let USER_DEFAULT_SOCIAL_TYPE      =   "SocialType"
let USER_DEFAULT_FB_ACCESS_TOKEN      =   "oauthToken"

let USER_DEFAULT_GOOGLE_ACCESS_TOKEN  =   "idToken"
let kGoogleUserId                     =   "userId"
let kGoogleIdToken                    =   "idToken"
let kGoogleName                       =   "name"
let kGoogleEmail                      =   "email"

let USER_DEFAULT_SAVE_FACEBOOK_DATA      =   "loginWithFacebookData"
let USER_DEFAULT_SAVE_GOOGLE_DATA      =   "loginWithGoogleData"

//MARK:- Database Keys -
let DB_DELETE_ALL_REGIONS           =   "Delete ALL Regions"
let DB_ADD_NEW_REGION               =   "Add New Region"
let DB_FIRE_EXIT_REGION             =   "Exit Region"
let DB_FIRE_ENTER_REGION            =   "Enter Region"
let DB_UPDATED_TO_SERVER            = "Updated To Server"
let DB_UPDATED_TO_SERVER_ApiLogging            = "ApiLogging Updated To Server "
let DB_RESCHEDLE_REGION             =   "Reschdule Region"
let DB_START_RESCHEDLE_REGION             =   "Start Reschdule Region"
let DB_APP_UPDATE                   = "App update"
let DB_LOCATION_MANAGER_OFF         = "Location Manager Off"

let DB_UPDATED_TO_SERVER_EXIT_REGION = "Updated Exit To Server"

//MARK: - Survey Download Key -
let TIME_STAMP_DATE_FORMATE         =   "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ"
let kAPIExpires                     =   "Expires"
let kAPIFileSize                    =   "FileSize"
let kAPISeconds                     =   "Seconds"
let kAPISurveyCode                  =   "SurveyCode"
let kAPIUrl                         =   "Url"
let kAPIWaveCode                    =   "WaveCode"
let kAPINotificationType            =   "NotificationType"
let kAPINotificationDate            =   "NotificationDate"
let kAPINotifications               =   "Notifications"


let kAPIUserId                      =   "user_id"
let kAPIId                          =   "id"


//MARK: - Notification Key -
let NOTIFICATION_LOCATION_UPDATE = "LocationUpdateNotification"
let NOTIFICATION_PUSH_UPDATE = "pushNotificationNofity"

let AWS_ACCESS_KEY_ID =  "AKIAILCQ5V2W64C2TBDA"
let AWS_SECRET_ACCESS_KEY = "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"
let AWS_STATING_BUCKET_TEMP = "com-ethos-staging-tmp"
let AWS_STATING_BUCKET_MEDIA =  "com-ethos-staging-media"


let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
let STORY_BOARD = UIStoryboard(name: "Main", bundle: nil)
let IS_ENTRY = "isEntry"
let CURRENT_MALL_DETAILS = "currentMallDetails"
let NSNOTIFICATION_ON_MALL_ENTER = "updateStoreListOnEnter"
let NSNOTIFICATION_ON_MALL_EXIT = "updateStoreListOnExit"
let NSNOTIFICATION_ON_MALL_ENTER_FOR_DEAL = "updateDealListOnEnter"
let NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL = "updateDealListOnExit"
let NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL_DETAILS = "updateDealDetailsOnExit"
let NSNOTIFICATION_ON_MALL_EXIT_FOR_STORE_DETAILS = "updateStoreDetailsOnExit"
let NSNOTIFICATION_ON_MALL_EXIT_FOR_REPORT = "updateReportVCOnExit"

//MARK: - Enum -
enum EventType: Int {
    case OnEntry = 0
    case OnExit
}

enum ReportType: Int {
    case ReportTypeStore = 1
    case ReportTypeDeal = 2
}

enum DealType: Int {
    case DealTypeSimple = 1
    case DealTypeScan = 2
}


enum CLError : Int {
    case LocationUnknown // location is currently unknown, but CL will keep trying
    case Denied // Access to location or ranging has been denied by the user
    // ...
}
enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

enum APIName : Int
{
    case MyAccount                  = 1
    case AuthenticationTokens       = 2
    case ProfileImages              = 3
    case GpsTrackingData            = 4
    case GpsPointsOfInterest        = 5
    case ApiLogging                 = 6
    case socialauthenticationtokens = 9
}

enum APINameInString : String
{
    case APIGetStoreList                = "getStoreList"
    case APIGetNearByMallList           = "getNearbyMallList"
    case APIGetStoreDetails             = "getStoreDetails"
    case APIGetDealListOfMall           = "getDealList"
    case APISendReportDetails           = "sendReportDetails"
    case APIGetAllCategoryListForMall   = "getAllCategoryListForMall"
    case APIGetScanDealListByQrCode     = "getScanDealByQrCodeList"
    case APIClickedDeal                 = "clickdDeal"
    case APIShareDeal                   = "sharedDeal"
    case APIFavouriteDeal               = "favoriteDeal"
    case APIGetAllCategoryListForStore  = "getAllCategoryListForStore"
}
enum DefaultCategoryAs : String
{
    case AllStores = "1"
    case HotDeals  = "2"
}

enum FontForApp : String
{
    case FontRegular                = "Roboto-Regular"
    case FontLight                  = "Roboto-Light"
    case FontBold                   = "Roboto-Bold"
    case FontBlack                  = "Roboto-Black"
    case FontMedium                 = "Roboto-Medium"
    case FontMediumItalic           = "Roboto-MediumItalic"
    case FontBoldItalic             = "Roboto-BoldItalic"
    case FontThinItalic             = "Roboto-ThinItalic"
    case FontThin                   = "Roboto-Thin"
    case FontBlackItalic            = "Roboto-BlackItalic"
    case FontItalic                 = "Roboto-Italic"
    case FontLightItalic            = "Roboto-LightItalic"
    case FontCondensedLight         = "RobotoCondensed-Light"
    case FontCondensedRegular       = "RobotoCondensed-Regular"
}

enum Gender : Int
{
    case Male                       = 1
    case Female                     = 2
    case Neither                    = 3
}

enum NotificationGapRequired : Int
{
    case NoDelay                    = 1
    case OncePerDay                 = 2
    case EveryFewDays               = 3
}
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPHONE            = UIDevice.currentDevice().userInterfaceIdiom == .Phone
}

//#define BASE_URL_GLOBAL @"http://byptserver.com/blowngo/index.php?r=api/"


let arrayGender = ["Male","Female"]
let arrayMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

struct AnimationType
{
    static let Default              = 1
    static let ScrollView           = 2
}

public enum Method: String {
    case OPTIONS = "OPTIONS"
    case GET = "GET"
    case HEAD = "HEAD"
    case POST = "POST"
    case PUT = "PUT"
    case PATCH = "PATCH"
    case DELETE = "DELETE"
    case TRACE = "TRACE"
    case CONNECT = "CONNECT"
}

let ANIMATION_FOR_PUSH_POP = false

//MARK: - COLOR -

let COLOR_LINE_GRAY = UIColor(red: 223.0/255.0, green: 230.0/255.0, blue: 237.0/255.0, alpha: 1.0)
let COLOR_TEXT_GRAY = UIColor(red: 161.0/255.0, green: 181.0/255.0, blue: 197.0/255.0, alpha: 1.0)
let COLOR_TEXT_GREEN = UIColor(red: 89.0/255.0, green: 214.0/255.0, blue: 64.0/255.0, alpha: 1.0)
let COLOR_TEXT_CELL_GRAY = UIColor(red: 89.0/255.0, green: 112.0/255.0, blue: 128.0/255.0, alpha: 1.0)

import UIKit
import Foundation
import CoreLocation

class HeaderConstant: BaseVC  {
    
    //    @IBOutlet var scrollView: UIScrollView!
    
    //MARK: - View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let instanceOfCustomObject: ObjCToSwift = ObjCToSwift()
        instanceOfCustomObject.someMethod()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    internal func test()
    {
        
    }
}

let LOCATION_UPDATE_INTERVAL_TIME = 300.0 // 5 min
let LOCATION_PERMISSION_ALERT_NOT_ASKED = "Not Started"

//ALpha One - 51,1035
//Edward test location mall 2  - mallId = 60
//Edward test location mall 2  - mallId = 58
//orija Mall - mallId = 59
//premier fashion mall - 1039

let TEST_MALL_ID = "1050"  //1050

let IS_TESTING = true

let USED_STATIC_IMAGE = "1"
let USED_KINGFISHER = "2"
let USED_ALOMFIRE_IMAGES = "3"

let USED_IMAGES = USED_KINGFISHER

