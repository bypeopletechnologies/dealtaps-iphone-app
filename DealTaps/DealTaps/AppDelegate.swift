//
//  AppDelegate.swift
//  DealTaps
//
//  Created by Nimisha on 10/02/16.
//  Copyright © 2016 Nimisha. All rights reserved.
//


import UIKit
import CoreLocation
import ReachabilitySwift
import SystemConfiguration
import SwiftyJSON
import Alamofire
import Fabric
import Crashlytics

let kSavedItemsKey = "mallList"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    var navigationController: UINavigationController?
    var backgroundUpdateTask: UIBackgroundTaskIdentifier = 0
    var backgroundLocationTask: UIBackgroundTaskIdentifier = 1

    var reachability: Reachability!
    
    // =====================For Location Allcation=====================
    
    var locationAccuracy : CLLocationAccuracy = CLLocationAccuracy()
    var myLocations: [CLLocation] = []
    var locationAuthorizationStatus: CLAuthorizationStatus!
    var locationManager: CLLocationManager!
    var locationStatus : NSString = LOCATION_PERMISSION_ALERT_NOT_ASKED
    var latitude: CLLocationDegrees = 0.0
    var longitude: CLLocationDegrees = 0.0
    var isFirstTimeLocationIsUpdate:Bool = false
    
    // ============================ Geotification =======================================
    
    var geotifications = [Geotification]()
    var timerUpdateLocation = NSTimer()
    var isNetworkReachable : Bool = false
    var arrayNearByMall:Array<JSON>?
    var currentMallDetails = Dictionary<String,AnyObject>?()
    //MARK: Application Life Cycle Method
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        sleep(3)
        self.initAPP()
        self.loadFirstViewController("MallStoreListVC")
        BaseVC.sharedInstance.logToFile("Application Lunch")
        BaseVC.sharedInstance.logToFile("Document - \(BaseVC.sharedInstance.getDocumentDirPath())")
        
        if launchOptions?[UIApplicationLaunchOptionsLocationKey] != nil
        {
            BaseVC.sharedInstance.logToFile("It's a location event")
            self.getDeviceLocation()
           // self.locationManager.startMonitoringSignificantLocationChanges()
        }
        
        self.loadAllGeotifications()

    return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        self.backgroundUpdateTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({
            
            self.startUpdatingLocationInterval()
            
            // self.endBackgroundUpdateTask()
        })
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        application.applicationIconBadgeNumber = 0
        
        self.endBackgroundUpdateTask()
        
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        if !locationStatus.isEqualToString(LOCATION_PERMISSION_ALERT_NOT_ASKED)
        {
            if (CLLocationManager.locationServicesEnabled())
            {
                if CLLocationManager.authorizationStatus() == .Denied
                {
                    self.alertTurnOnLocationFromAppSetting()
                }
                else if CLLocationManager.authorizationStatus() == .NotDetermined
                {
                    self.alertTurnOnLocationFromAppSetting()
                }
            }
            else
            {
                self.alertTurnOnLocationFromDevicePrivacySetting()
            }
        }
        
      //  self.locationManager.stopUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()

    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
            }
    
    func applicationDidBecomeActive(application: UIApplication) {
        
        // Need to stop regular updates first
        
//        self.locationManager.stopMonitoringSignificantLocationChanges()
//        self.locationManager.startUpdatingLocation()


        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
        BaseVC.sharedInstance.DLog("notification of region change \(notification.userInfo)")
        
        if let mallInfo = notification.userInfo
        {
            let isEnter = mallInfo[IS_ENTRY] as! String
            
            BaseVC.sharedInstance.DLog("isEnter \(isEnter)")
            
            //region enter notification
            if isEnter == "1"
            {
                BaseVC.sharedInstance.DLog("enter region notification");
                
                //Check for old notification by comparing mallId
                
                if let tempMallDetails = mallInfo as? Dictionary<String,AnyObject>
                {
                    if let tempMallId = tempMallDetails[kAPIMallId] as? String
                    {
                        
                        self.currentMallDetails = BaseVC.sharedInstance.getUserDefaultDataFromKey(CURRENT_MALL_DETAILS) as? Dictionary<String,AnyObject>
                        
                        BaseVC.sharedInstance.DLog("current mall details \(self.currentMallDetails)")

                        let currentMallId = self.currentMallDetails![kAPIMallId] as! String
                        if(tempMallId == currentMallId)
                        {
                           // BaseVC.sharedInstance.setUserDefaultDataFromKey(CURRENT_MALL_DETAILS, dic: self.currentMallDetails!)
                            
                            self.actionOnEnterNotification()
                        }
                    }
                }
            }
            else
            {
                BaseVC.sharedInstance.DLog("exit region notification");
                self.actionOnExitNotification()
            }
        }
    }
    
    // MARK: - Application Supporting Funcations -
    
    func initAPP()
    {
        self.initObjects()
        self.checkInternet()
        //    self.setStatusBar()
        self.registerForPushNotification()
        self.addFabric()
        self.getDeviceLocation()
        //        self.otherLogs()
     //   self.startUpdatingLocationInterval()
        //   self.configureAudio()
        
    }
    
    func initObjects()
    {
        if BaseVC.sharedInstance.isExistUserDefaultKey(CURRENT_MALL_DETAILS)
        {
            self.currentMallDetails = BaseVC.sharedInstance.getUserDefaultDataFromKey(CURRENT_MALL_DETAILS) as? Dictionary<String,AnyObject>
        }
    }
    func addFabric()
    {
        Fabric.with([Crashlytics.self])
    }
    func loadFirstViewController(viewController: String)
    {
        /////////////////////////Add Navigation Controller and set Root View Controller///////////////////////////////////////
        let object = STORY_BOARD.instantiateViewControllerWithIdentifier(viewController)
        self.navigationController = UINavigationController()
        self.navigationController!.pushViewController(object, animated: false)
        self.navigationController?.navigationBar.hidden = true
        self.loadNavigationControllerToWindow()
    }
    
    func loadNavigationControllerToWindow()
    {
        self.window!.rootViewController = self.navigationController
        self.window!.makeKeyAndVisible()
    }
    
    func setStatusBar()
    {
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
    }
    
    func endBackgroundUpdateTask()
    {
        UIApplication.sharedApplication().endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }
    
    func registerForPushNotification()
    {
        BaseVC.sharedInstance.getCurrentPhoneNotificationSetting()
        let userNotificationType : UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Sound, UIUserNotificationType.Badge]
        let settings = UIUserNotificationSettings(forTypes: userNotificationType, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        // UIApplication.sharedApplication().registerForRemoteNotifications()
        
        UIApplication.sharedApplication().cancelAllLocalNotifications()
    }
    
    // MARK: - Reachability -
    
    func checkInternet()
    {
        self.isNetworkReachable = self.isConnectedToNetwork()
        self.addRechabilityObserver()
    }
    
    func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func addRechabilityObserver()
    {
        do
        {
            self.reachability = try Reachability.reachabilityForInternetConnection()
        }
        catch
        {
            BaseVC.sharedInstance.DLog("Unable to create Reachability")
            return
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: #selector(AppDelegate.reachabilityChanged(_:)),
            name: ReachabilityChangedNotification,
            object: reachability)
        
        do
        {
            try self.reachability.startNotifier()
        }
        catch
        {
            BaseVC.sharedInstance.DLog("Unable to start notifier")
        }
    }
    
    func reachabilityChanged(note: NSNotification)
    {
        let reachability = note.object as! Reachability
        
        if reachability.isReachable()
        {
            if reachability.isReachableViaWiFi()
            {
                BaseVC.sharedInstance.DLog("Reachable via WiFi")
            }
            else
            {
                BaseVC.sharedInstance.DLog("Reachable via Cellular")
            }
            
            self.isNetworkReachable = true
        }
        else
        {
            BaseVC.sharedInstance.DLog("Not reachable")
            self.isNetworkReachable = false
        }
    }
    
    func stopRechabilityNotification()
    {
        self.reachability.stopNotifier()
        NSNotificationCenter.defaultCenter().removeObserver(self,
            name: ReachabilityChangedNotification,
            object: self.reachability)
    }
    
    // MARK: - Location Manager Related Method -
    func getDeviceLocation()
    {
        if (CLLocationManager.locationServicesEnabled())
        {
            if CLLocationManager.authorizationStatus() == .AuthorizedAlways
            {
                self.initLocationManager()
            }
            else if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse
            {
                self.initLocationManager()
            }
            else if CLLocationManager.authorizationStatus() == .Denied
            {
                self.initLocationManagerObject()
            }
            else if CLLocationManager.authorizationStatus() == .NotDetermined
            {
                self.initLocationManager()
            }
            else if CLLocationManager.authorizationStatus() == .Restricted
            {
                self.initLocationManagerObject()
            }
        }
        else
        {
            self.alertTurnOnLocationFromDevicePrivacySetting()
        }
        
        if self.locationManager == nil
        {
            BaseVC.sharedInstance.DLog("Location manager permission asked but not selected any option")
        }
    }
    
    func initLocationManager()
    {
        self.initLocationManagerObject()
      //  self.locationManager.startUpdatingLocation()
      //  self.locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func initLocationManagerObject()
    {
        // ///////////////////////Location Manager Allocation///////////////////////////////////////
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
       self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.distanceFilter = 200
        // self.locationManager.activityType = CLActivityType.OtherNavigation
        self.locationManager.requestAlwaysAuthorization()
      //  self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        // locationManager.stopUpdatingLocation()
        if (error.localizedDescription.characters.count > 0) {
            BaseVC.sharedInstance.DLog("\(error)")
            if let clErr = CLError(rawValue: error.code) {
                switch clErr {
                case .LocationUnknown:
                    BaseVC.sharedInstance.DLog("location unknown")
                case .Denied:
                    BaseVC.sharedInstance.DLog("denied")
                    BaseVC.sharedInstance.alertDontAllow()
                    // self.AllowLocationServicePopUp()
                    // default:
                    // print("unknown Core Location error")
                }
            } else {
                BaseVC.sharedInstance.DLog("other error")
            }
        }
    }
    func locationManager(manager: CLLocationManager, monitoringDidFailForRegion region: CLRegion?, withError error: NSError) {
        BaseVC.sharedInstance.DLog("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        BaseVC.sharedInstance.DLog("Enter in Region \(region)")
        if region is CLCircularRegion {
                       handleRegionEvent(region,eventType: EventType.OnEntry)
                }
    }
    
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        BaseVC.sharedInstance.DLog("Exit in Region \(region)")

        if region is CLCircularRegion {
            handleRegionEvent(region,eventType: EventType.OnExit)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // myLocations.append(locations[0])
        
        let locationArray = locations as NSArray
        let locationObj: AnyObject? = locationArray.lastObject
        
        if let _ = manager.location
        {
            self.latitude = manager.location!.coordinate.latitude
            self.longitude = manager.location!.coordinate.longitude
            
            self.locationAccuracy = locationObj!.horizontalAccuracy
            
            BaseVC.sharedInstance.DLog("didUpdateLocations (Lat,long)=(\(self.latitude),\(self.longitude))")
            
            if(self.latitude != 0.0 && self.longitude != 0.0)
            {
                self.loadNearByMallList()

//                if(!self.isFirstTimeLocationIsUpdate)
//                {
//                    self.isFirstTimeLocationIsUpdate = true
//
//                }
            }
        }
    }
    
    func checkUserAlreadyInRegion()
    {
        
        if let _ = self.arrayNearByMall
        {
            for mallDetails in self.arrayNearByMall!
            {
                //get the distance between user current latlong and mall latlong if it is within the mall radius
                // break from the loop set the mall details to currentMallDetails
                
                
               // let mallDistance = mallDetails[kAPIDistance].doubleValue
                var radius:Double = 200.0

                if let radiusTemp = mallDetails[kAPIRadius].string
                {
                    radius = Double(radiusTemp)!
                }

                radius = radius / 1000
                let mallName = mallDetails[kAPIMallName].stringValue
                
                //My location
                let myLocation = CLLocation(latitude: self.latitude, longitude: self.longitude)
                
                //Mall location
                let mallLocation = CLLocation(latitude: mallDetails[kAPILatitude].doubleValue, longitude: mallDetails["lontitude"].doubleValue)
                
                //Measuring my distance  (in km)
                let distance = myLocation.distanceFromLocation(mallLocation) / 1000
                
                //Display the result in km
                BaseVC.sharedInstance.DLog(String(format: "The distance to \(mallName) is %.06fkm", distance))
                if distance <= radius
                {
                    //check user is already inside a mall

                    if (self.currentMallDetails != nil)
                    {
                        //check user is in the same mall or different mall
                       let currentMallId =  self.currentMallDetails![kAPIMallId] as! String
                       let newMallId = mallDetails.dictionaryObject![kAPIMallId] as! String
                        
                        if currentMallId != newMallId
                        {
                            //user is in different mall
                            self.updateCurrentMall(mallDetails,isMallSwipe: true)
                        }
                    }
                    else
                    {
                        self.updateCurrentMall(mallDetails,isMallSwipe: false)
                    }
                    
                    break;
                }
            }
        }
    }
    
    func updateCurrentMall(mallDetails:JSON, isMallSwipe: Bool)
    {
        //Make exit from currentMallDetails 
        if isMallSwipe == true
        {
            let currentMallId =  self.currentMallDetails![kAPIMallId] as! String

            if let geoTification = geoDetailsfromRegionIdentifier(currentMallId)
            {
                if let enterFlag = geoTification.mallDetails[IS_ENTRY] as? String
                {
                    if enterFlag == "1"
                    {
                        let msg = "\(ALERT_THANK_YOU) \(geoTification.note). \(ALERT_HAVE_A_GREATE_DAY)"
                        BaseVC.sharedInstance.DLog("Active \(msg)")
                        geoTification.mallDetails[IS_ENTRY] = "0"
                        self.currentMallDetails = nil
                        BaseVC.sharedInstance.removeUserDefaultKey(CURRENT_MALL_DETAILS)
                        // check the topViewController and post a notification to that screen
                        self.actionOnExitNotification()
                        let alertVw = UIAlertView(title: ALERT_TITLE, message: msg, delegate: nil, cancelButtonTitle: "OK")
                        alertVw.show()
                        
                        updateGeotificationArrayForMallId(currentMallId, newDetails: geoTification)
                    }
                }
            }
        }
        
        let mallId = mallDetails[kAPIMallId].stringValue

        if let geoTification = geoDetailsfromRegionIdentifier(mallId)
        {
            BaseVC.sharedInstance.DLog("mall details \(geoTification.mallDetails)")
            var msg:String
                msg = "Welcome to \(geoTification.note)! \(ALERT_WELCOME_MSG)"
                BaseVC.sharedInstance.DLog("Active \(msg)")
                
                geoTification.mallDetails[IS_ENTRY] = "1"
                self.currentMallDetails = geoTification.mallDetails
                BaseVC.sharedInstance.setUserDefaultDataFromKey(CURRENT_MALL_DETAILS, dic: self.currentMallDetails!)
                self.actionOnEnterNotification()
                let alertVw = UIAlertView(title: ALERT_TITLE, message: msg, delegate: nil, cancelButtonTitle: "OK")
                alertVw.show()
            
                updateGeotificationArrayForMallId(mallId, newDetails: geoTification)
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        
        BaseVC.sharedInstance.DLog("\(locationStatus)");
        switch status {
        case CLAuthorizationStatus.Restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.Denied:
            locationStatus = "User denied access to location"
            // self.AllowLocationServicePopUp()
        case CLAuthorizationStatus.NotDetermined:
            locationStatus = "Status not determined"
        default:
            if status == CLAuthorizationStatus.AuthorizedAlways
            {
                // if you click on Device Settings > Privacy > Locations > Always
                
                if !locationStatus.isEqualToString(LOCATION_PERMISSION_ALERT_NOT_ASKED)
                {
                    BaseVC.sharedInstance.startLocationManager()
                    
                }
            }
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        
        BaseVC.sharedInstance.DLog("\(locationStatus)");

        if (shouldIAllow == true)
        {
            BaseVC.sharedInstance.Log("Location to Allowed")
            // Start location services
            if !locationStatus.isEqualToString("Status not determined")
            {
            }
        }
        else
        {
            if !locationStatus.isEqualToString("Status not determined")
            {
                BaseVC.sharedInstance.Log("Denied access: \(locationStatus)")
            }
        }
    }
    
    func alertTurnOnLocationFromAppSetting()
    {
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            if !self.locationStatus.isEqualToString(LOCATION_PERMISSION_ALERT_NOT_ASKED)
            {
                let alertController = UIAlertController(
                    title: ALERT_TURN_ON_LOCATION_FROM_APP,
                    message: "",
                    preferredStyle: .Alert)
                
                let cancelAction = UIAlertAction(title: ALERT_CANCEL, style: .Cancel, handler: nil)
                alertController.addAction(cancelAction)
                
                let openAction = UIAlertAction(title: ALERT_SETTINGS, style: .Default) {(action) in
                    if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
                alertController.addAction(openAction)
                self.navigationController?.topViewController!.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func alertTurnOnLocationFromDevicePrivacySetting()
    {
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            if !self.locationStatus.isEqualToString(LOCATION_PERMISSION_ALERT_NOT_ASKED)
            {
                let alertController = UIAlertController(
                    title: ALERT_TURN_ON_LOCATION_FROM_DEVICE_PRIVACY,
                    message: "",
                    preferredStyle: .Alert)
                
                let cancelAction = UIAlertAction(title: ALERT_OK, style: .Cancel, handler: nil)
                alertController.addAction(cancelAction)
                
                self.navigationController?.topViewController!.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: GeoFence Region
    func loadNearByMallList()
    {
        var param = Dictionary<String,String>()
        
        if(self.latitude != 0.0 && self.longitude != 0.0)
        {
            param[kAPILatitude] = "\(self.latitude)"
            param[kAPILongitude] = "\(self.longitude)"
            
            BaseVC.sharedInstance.printAPIURL(APINameInString.APIGetNearByMallList.rawValue, param: param)

            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIGetNearByMallList.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            if (status == 1)
                            {
                                if (IS_TESTING)
                                {
                                    UIApplication.sharedApplication().applicationIconBadgeNumber += 1
                                }
                                self.arrayNearByMall = json[kAPIData].arrayValue
                               // BaseVC.sharedInstance.setUserDefaultDataFromKey(USER_DEFAULT_NEAR_BY_MALL_LIST, dic: value as! NSDictionary)
                                
                                self.removeAllGeotification()
                                self.addMallToGeofenceRegion()
                                
//                                if(!self.isFirstTimeLocationIsUpdate)
//                                {
//                                    self.isFirstTimeLocationIsUpdate = true;
//                                    
//                                    if !IS_TESTING
//                                    {
//                                        //check that user current latlong is exist in a mall region or not
//                                        self.checkUserAlreadyInRegion()
//                                    }
//                                }
                                
                                self.checkUserAlreadyInRegion()

                            }
                        }
                        BaseVC.sharedInstance.DLog("response \(json)")
                    }
                case .Failure(let error):
                    BaseVC.sharedInstance.DLog(error)
//                    let notification = UILocalNotification()
//                    notification.alertBody = " Error : \(error.debugDescription)"
//                    notification.soundName = "Default";
//                    notification.applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
//                    UIApplication.sharedApplication().presentLocalNotificationNow(notification)
                }
            }
        }
    }
    
    func addMallToGeofenceRegion()
    {
        for mallDetails in self.arrayNearByMall!
        {
            
            // 1
             
            let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(mallDetails[kAPILatitude].doubleValue, mallDetails["lontitude"].doubleValue)
            let mallId = mallDetails[kAPIMallId].stringValue
            let note = mallDetails[kAPIMallName].stringValue
            let distance = mallDetails[kAPIDistance].doubleValue
            
            var radius:Double = 200.0
            
            //            BaseVC.sharedInstance.DLog("radius \(mallDetails[kAPIRadius])")
            //            BaseVC.sharedInstance.DLog("radius double \(mallDetails[kAPIRadius].double)")
            //            BaseVC.sharedInstance.DLog("radius string \(mallDetails[kAPIRadius].string)")
            
            
            if let radiusTemp = mallDetails[kAPIRadius].string
            {
                radius = Double(radiusTemp)!
            }
            
            let clampedRadius = (radius > self.locationManager.maximumRegionMonitoringDistance) ? self.locationManager.maximumRegionMonitoringDistance : radius
            
//            if clampedRadius < 200
//            {
//                clampedRadius = radius
//            }
            
            var mallDetailsForNotification = BaseVC.sharedInstance.removeNullFromDictionary(mallDetails.dictionaryObject!) as! Dictionary<String, AnyObject>
            
            //If user is already inside the mall set the IS_ENTRY flag to 1 else 0
            
            if let currentMallDetails = self.currentMallDetails
            {
                //compare the mallID
                let currentMallId = currentMallDetails[kAPIMallId] as? String
                if currentMallId == mallId
                {
                    mallDetailsForNotification[IS_ENTRY] = "1"
                }
                else
                {
                    mallDetailsForNotification[IS_ENTRY] = "0"
                }
            }
            else
            {
                mallDetailsForNotification[IS_ENTRY] = "0"
            }
            
            let geotification = Geotification(coordinate: coordinate, RadiusMetres: clampedRadius, PointOfInterestId:mallId, note: note, eventType: EventType.OnEntry, EventId: mallId, distanceBetCurrentAndRegionLatLong: distance, mallDetails:mallDetailsForNotification)
            addGeotification(geotification)
            // 2
            startMonitoringGeotification(geotification)
            
            saveAllGeotifications()
        }
    }
    
    
    // MARK: Loading and saving functions
    
    func loadAllGeotifications() {
        self.geotifications = []
        
        if let savedItems = NSUserDefaults.standardUserDefaults().objectForKey(kSavedItemsKey) as? NSArray
        {
            for savedItem in savedItems
            {
                
                //print("\(NSKeyedUnarchiver.unarchiveObjectWithData(savedItem as! NSData))")
                
                if let geotification = NSKeyedUnarchiver.unarchiveObjectWithData(savedItem as! NSData) as? Geotification {
                    addGeotification(geotification)
                   // startMonitoringGeotification(geotification)
                   // saveAllGeotifications()

                }
            }
        }
    }
    
    func loadAllNearByMall()
    {
        self.arrayNearByMall = []
        
        if  let value = BaseVC.sharedInstance.getUserDefaultDataFromKey1(USER_DEFAULT_NEAR_BY_MALL_LIST)
        {
            let json = JSON(value)
            if let status = json[kAPIStatus].int
            {
                if (status == 1)
                {
                    self.arrayNearByMall = json[kAPIData].arrayValue
                }
            }
        }
    }
    
    func saveAllGeotifications() {
        let items = NSMutableArray()
        for geotification in geotifications {
            let item = NSKeyedArchiver.archivedDataWithRootObject(geotification)
            items.addObject(item)
        }
        NSUserDefaults.standardUserDefaults().setObject(items, forKey: kSavedItemsKey)
        NSUserDefaults.standardUserDefaults().synchronize()
        
    }
    
    func updateGeotificationArrayForMallId(mallId: String,newDetails:Geotification)
    {
        if let _ = NSUserDefaults.standardUserDefaults().objectForKey(kSavedItemsKey)
        {
            let items = NSUserDefaults.standardUserDefaults().objectForKey(kSavedItemsKey)?.mutableCopy() as! NSMutableArray
            
            for savedItem in items
            {
                if let geotification = NSKeyedUnarchiver.unarchiveObjectWithData(savedItem as! NSData) as? Geotification
                {
                    
                    BaseVC.sharedInstance.DLog("------ geotification \(geotification.mallDetails)")
                    
                    
                    if geotification.PointOfInterestId == mallId
                    {
                        //remove the mall data and the new for it
                        items.removeObject(savedItem)
                        
                        let item = NSKeyedArchiver.archivedDataWithRootObject(newDetails)
                        items.addObject(item)
                        
                        newDetails.printDetails()

                        break;
                    }
                }
            }
            
            NSUserDefaults.standardUserDefaults().setObject(items, forKey: kSavedItemsKey)
            NSUserDefaults.standardUserDefaults().synchronize()
        }

    }
   
    
    // MARK: Functions that update the model/associated views with geotification changes
    
    func addGeotification(geotification: Geotification) {
        geotifications.append(geotification)
        BaseVC.sharedInstance.DLog("Geotifications  (\(geotifications.count))")
        geotification.printDetails()
    }
    
    func removeAllGeotification()
    {
        for geotification in self.geotifications
        {
            stopMonitoringGeotification(geotification)
            removeGeotification(geotification)
            saveAllGeotifications()
        }
    }
    func removeGeotification(geotification: Geotification) {
        //    if let indexInArray = find(geotifications, geotification) {
        //      geotifications.removeAtIndex(indexInArray)
        //    }
        
        if let indexInArray = geotifications.indexOf(geotification) {
            geotifications.removeAtIndex(indexInArray)
        }
        
        
        //N mapView.removeAnnotation(geotification)
        //N removeRadiusOverlayForGeotification(geotification)
        //N updateGeotificationsCount()
    }
    
    func handleRegionEvent(region: CLRegion, eventType: EventType) {
        // Show an alert if application is active
        if UIApplication.sharedApplication().applicationState == .Active
        {
            
            if let geoTification = geoDetailsfromRegionIdentifier(region.identifier)
            {
                BaseVC.sharedInstance.DLog("Application is active")

                BaseVC.sharedInstance.DLog("kAPIma details \(geoTification.mallDetails)")
                var msg:String
                if(eventType == .OnEntry)
                {
                    msg = "Welcome to \(geoTification.note)! \(ALERT_WELCOME_MSG)"
                    BaseVC.sharedInstance.DLog("Active \(msg)")

                    geoTification.mallDetails[IS_ENTRY] = "1"
                    self.currentMallDetails = geoTification.mallDetails
                    BaseVC.sharedInstance.setUserDefaultDataFromKey(CURRENT_MALL_DETAILS, dic: self.currentMallDetails!)
                    
                    
                    self.actionOnEnterNotification()
                    let alertVw = UIAlertView(title: ALERT_TITLE, message: msg, delegate: nil, cancelButtonTitle: "OK")
                    alertVw.show()
                    updateGeotificationArrayForMallId(region.identifier, newDetails: geoTification)


                }
                else
                {
                    if let enterFlag = geoTification.mallDetails[IS_ENTRY] as? String
                    {
                        BaseVC.sharedInstance.DLog("fg *** enterFlag \(enterFlag)")

                        if enterFlag == "1"
                        {
                            msg = "\(ALERT_THANK_YOU) \(geoTification.note). \(ALERT_HAVE_A_GREATE_DAY)"
                            BaseVC.sharedInstance.DLog("Active \(msg)")
                            geoTification.mallDetails[IS_ENTRY] = "0"
                            self.currentMallDetails = nil
                            BaseVC.sharedInstance.removeUserDefaultKey(CURRENT_MALL_DETAILS)
                            // check the topViewController and post a notification to that screen
                            self.actionOnExitNotification()
                            let alertVw = UIAlertView(title: ALERT_TITLE, message: msg, delegate: nil, cancelButtonTitle: "OK")
                            alertVw.show()
                            
                            updateGeotificationArrayForMallId(region.identifier, newDetails: geoTification)
                        }
                        else
                        {
                            BaseVC.sharedInstance.DLog("*** else enterFlag \(enterFlag)")
                        }
                    }
                }
            }
        }
        else
        {
            BaseVC.sharedInstance.DLog("Application is in bg")

            // Otherwise present a local notification
            
            if let geoTification = geoDetailsfromRegionIdentifier(region.identifier)
            {
                let notification = UILocalNotification()
                if(eventType == .OnEntry)
                {
                    notification.alertBody = "Welcome to \(geoTification.note)! \(ALERT_WELCOME_MSG)"
                    BaseVC.sharedInstance.DLog("Background \(notification.alertBody)")
                    geoTification.mallDetails[IS_ENTRY] = "1"
                    self.currentMallDetails = geoTification.mallDetails
                    BaseVC.sharedInstance.setUserDefaultDataFromKey(CURRENT_MALL_DETAILS, dic: self.currentMallDetails!)
                    notification.soundName = "Default";
                    notification.userInfo = geoTification.mallDetails
                    UIApplication.sharedApplication().presentLocalNotificationNow(notification)
                    
                    updateGeotificationArrayForMallId(region.identifier, newDetails: geoTification)

                }
                else
                {
                    if let enterFlag = geoTification.mallDetails[IS_ENTRY] as? String
                    {
                         BaseVC.sharedInstance.DLog("bg *** enterFlag \(enterFlag)")
                        if enterFlag == "1"
                        {
                            notification.alertBody = "\(ALERT_THANK_YOU) \(geoTification.note). \(ALERT_HAVE_A_GREATE_DAY)"
                            BaseVC.sharedInstance.DLog("Background \(notification.alertBody)")
                            geoTification.mallDetails[IS_ENTRY] = "0"
                            self.currentMallDetails = nil
                            BaseVC.sharedInstance.removeUserDefaultKey(CURRENT_MALL_DETAILS)
                            notification.soundName = "Default";
                            notification.userInfo = geoTification.mallDetails
                            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
                            
                            updateGeotificationArrayForMallId(region.identifier, newDetails: geoTification)

                        }
                        else
                        {
                            BaseVC.sharedInstance.DLog("*** else enterFlag \(enterFlag)")
                        }
                        
                    }
                }
            }
        }
    }
    
     func notefromRegionIdentifier(identifier: String) -> String? {
        if let savedItems = NSUserDefaults.standardUserDefaults().objectForKey(kSavedItemsKey) as? NSArray
        {
            for savedItem in savedItems {
                if let geotification = NSKeyedUnarchiver.unarchiveObjectWithData(savedItem as! NSData) as? Geotification {
                    
                    BaseVC.sharedInstance.DLog("------ geotification \(geotification.mallDetails)")
                    if geotification.PointOfInterestId == identifier {
                        return geotification.note
                    }
                }
            }
        }
        return nil
    }
    func geoDetailsfromRegionIdentifier(identifier: String) -> Geotification?
    {
        if let savedItems = NSUserDefaults.standardUserDefaults().objectForKey(kSavedItemsKey) as? NSArray
        {
            for savedItem in savedItems {
                if let geotification = NSKeyedUnarchiver.unarchiveObjectWithData(savedItem as! NSData) as? Geotification {
                    
                    BaseVC.sharedInstance.DLog("------ geotification \(geotification.mallDetails)")
                    if geotification.PointOfInterestId == identifier {
                        return geotification
                    }
                }
            }
        }
        return nil
    }

    
    func startMonitoringGeotification(geotification: Geotification) {
        
        // 1
        if !CLLocationManager.isMonitoringAvailableForClass(CLCircularRegion) {
            let alertVw = UIAlertView(title: ALERT_TITLE, message: "Geofencing is not supported on this device!", delegate: nil, cancelButtonTitle: "Ok")
            alertVw.show()

            BaseVC.sharedInstance.DLog("Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
            
            let alertVw = UIAlertView(title: ALERT_TITLE, message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.", delegate: nil, cancelButtonTitle: "Ok")
            alertVw.show()
            BaseVC.sharedInstance.DLog("Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        // 3
        let region = regionWithGeotification(geotification)
        // 4
        
        BaseVC.sharedInstance.DLog("region \(region)")
        self.locationManager.startMonitoringForRegion(region)
    }
    
    func stopMonitoringGeotification(geotification: Geotification) {
        for region in self.locationManager.monitoredRegions {
            if let circularRegion = region as? CLCircularRegion {
                if circularRegion.identifier == geotification.PointOfInterestId {
                    self.locationManager.stopMonitoringForRegion(circularRegion)
                }
            }
        }
    }
    
    func regionWithGeotification(geotification: Geotification) -> CLCircularRegion {
        // 1
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.RadiusMetres, identifier: geotification.PointOfInterestId)
        // 2
        region.notifyOnEntry = (geotification.eventType == .OnEntry)
      //  region.notifyOnExit = !region.notifyOnEntry //Do not want notification in onExit from region.
        region.notifyOnExit = true
        return region
    }
    
    // MARK: - Action on Region Enter / Exit

    func actionOnExitNotification()
    {
        // check the topViewController and post a notification to that screen
        if let topVC = self.navigationController?.topViewController
        {
            if topVC.isKindOfClass(MallStoreListVC)
            {
                NSNotificationCenter.defaultCenter().postNotificationName(NSNOTIFICATION_ON_MALL_EXIT, object: nil)
            }
            else if topVC.isKindOfClass(MallDealListVC)
            {
                NSNotificationCenter.defaultCenter().postNotificationName(NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL, object: nil)
            }
            else if topVC.isKindOfClass(StoreDetailsVC)
            {
                NSNotificationCenter.defaultCenter().postNotificationName(NSNOTIFICATION_ON_MALL_EXIT_FOR_STORE_DETAILS, object: nil)
            }
            else if topVC.isKindOfClass(DealDetailsVC)
            {
                NSNotificationCenter.defaultCenter().postNotificationName(NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL_DETAILS, object: nil)
            }
            else if topVC.isKindOfClass(ReportVC)
            {
                NSNotificationCenter.defaultCenter().postNotificationName(NSNOTIFICATION_ON_MALL_EXIT_FOR_REPORT, object: nil)
            }
        }
    }
    
    func actionOnEnterNotification()
    {
        // check the topViewController and post a notification to that screen
        
        if let topVC = self.navigationController?.topViewController
        {
            if topVC.isKindOfClass(MallStoreListVC)
            {
                NSNotificationCenter.defaultCenter().postNotificationName(NSNOTIFICATION_ON_MALL_ENTER, object: nil)
            }
            else if topVC.isKindOfClass(MallDealListVC)
            {
                NSNotificationCenter.defaultCenter().postNotificationName(NSNOTIFICATION_ON_MALL_ENTER_FOR_DEAL, object: nil)
            }
        }
    }
    

    // MARK: - Location Update Interval Time [10 min] -
    func startUpdatingLocationInterval()
    {
        // =====================Start updating location for Interval=====================
        
        if self.timerUpdateLocation.isKindOfClass(NSTimer)
        {
            self.timerUpdateLocation.invalidate()
        }
        
        self.timerUpdateLocation = NSTimer.scheduledTimerWithTimeInterval(LOCATION_UPDATE_INTERVAL_TIME, target: self, selector: #selector(AppDelegate.updateLocation(_:)), userInfo: nil, repeats: true)
    }
    
    func updateLocation(timer : NSTimer)
    {
        
    }
    
    func stopUpdateLocation()
    {
        if self.timerUpdateLocation.isKindOfClass(NSTimer)
        {
            self.timerUpdateLocation.invalidate()
        }
    }
    
    func beginNewBackgroundTask()
    {
    
    // Once called, the beginNewBackgroundTask will start a new background task, if the app is indeed in the
    // background, and will then explicitly end all the other tasks to prevent the app from being killed by the system
    
    let application = UIApplication.sharedApplication()
    self.backgroundLocationTask  = UIBackgroundTaskInvalid;
        
        if(application.respondsToSelector(#selector(UIApplication.beginBackgroundTaskWithExpirationHandler(_:))))
        {
            self.backgroundLocationTask = application.beginBackgroundTaskWithExpirationHandler({ () -> Void in
                
                NSLog("background task\(self.backgroundLocationTask) expired");
            })
        }
    }
    
    func isUserWithinTheRangeOfMall() -> Bool
    {
        return true
    }
}