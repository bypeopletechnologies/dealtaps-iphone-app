//
//  Dealtaps-Bridging-Header.h
//  Dealtaps
//
//  Created by Nimisha on 10/02/16.
//  Copyright © 2016 Nimisha. All rights reserved.
//

#ifndef DealTabs_Bridging_Header_h
#define DealTabs_Bridging_Header_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "ObjCToSwift.h"
#import "sqlite3.h"
#import <time.h>
#import "JTProgressHUD.h"
#import "iCarousel.h"


#endif /* DealTabs_Bridging_Header_h */
