//
//  StoreDetailsVC.swift
//  DealTaps
//
//  Created by Nimisha on 17/02/16.
//
//

import UIKit
import Kingfisher
import SwiftyJSON
import Alamofire
import Darwin

class StoreDetailsVC: BaseVC, iCarouselDataSource, iCarouselDelegate {
    
    @IBOutlet weak var vwExpandCategory: UIView!
    @IBOutlet weak var btnCategoryExpand: UIButton!
    @IBOutlet weak var vwCategoryList: UIView!
    @IBOutlet weak var lblStoreTitle: UILabel!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var vwTemp: UIView!
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var vwStoreTimeDetails: UIView!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var imgStoreLogo: UIImageView!
    @IBOutlet weak var lblStoreDesc: UILabel!
    @IBOutlet weak var lblClosingTime: UILabel!
    @IBOutlet weak var lblShopNumber: UILabel!
    @IBOutlet weak var lblStoreLevel: UILabel!
    @IBOutlet weak var vwStoreDetails: UIView!
   // @IBOutlet weak var vwCategory: UIView!
    @IBOutlet weak var btnExpandShadow: UIImageView!
    @IBOutlet weak var btnExpand: UIButton!
    @IBOutlet weak var vwExpansion: UIView!
    var timerForAutoScroll:NSTimer?
    let spaceBetweenCategory:CGFloat = 20.0
    let startXForCategory:CGFloat = 29.0
    var defaultNumberOfCategory:Int = 0
    var isExpanded:Bool = false
    var imgArray:[String?] = []
    var dictStoreDetails = Dictionary<String,JSON>()
    var originalFrame:CGRect?
    var originalFrameOfCategoryList:CGRect?
    var isFirstTime:Bool = false
    var arrayCategory = [JSON]()
    
    var vwTmpFrame:CGRect?
    //var vwCatFrame:CGRect?
    var vwCatListFrame:CGRect?
    
    
    //MARK: - View Life Cycle
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
//        if(!self.isFirstTime)
//        {
//            self.isFirstTime = true
//        }
//        else
//        {
//           // self.loadData()
//            DLog("data loaded temp \(self.vwTemp.frame)")
//        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.updateFont()

        self.carousel.clipsToBounds = true
        self.carousel.type = .Rotary
        self.carousel.layer.borderColor = COLOR_LINE_GRAY.CGColor
        self.carousel.layer.borderWidth = 1.0
        self.carousel.scrollEnabled = true
        
        self.timerForAutoScroll = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(StoreDetailsVC.autoScrollToNextImage), userInfo: nil, repeats: true)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(StoreDetailsVC.updateStoreDetailsOnExit(_:)), name: NSNOTIFICATION_ON_MALL_EXIT_FOR_STORE_DETAILS, object: nil)
        // Do any additional setup after loading the view.
    }

    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_EXIT_FOR_STORE_DETAILS, object: nil);
    }
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated);
        
        self.scrollVw.hidden = false

    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
    }
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated);
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Report
    @IBAction func onReportClicked(sender: UIButton)
    {
        let reportVC = STORY_BOARD.instantiateViewControllerWithIdentifier("ReportVC") as! ReportVC
        reportVC.reportType = ReportType.ReportTypeStore
        reportVC.reportId = self.dictStoreDetails[kAPIStoreId]?.string
        appDelegate.navigationController?.pushViewController(reportVC, animated: ANIMATION_FOR_PUSH_POP)
    }
    //MARK: - Call
    @IBAction func onCallClicked(sender: UIButton) {
        
        if let storePhoneNumber = self.dictStoreDetails[kAPIStorePhoneNumber]?.string
        {
            if let url = NSURL(string: "tel://\(self.convertPhoneNumberInDialFormat(storePhoneNumber))")
            {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        else
        {
            super.DAlert("Sorry!!", message: "Store phone number is not available", action: "OK", sender: self)
        }
    }
    //MARK: - Expand
    @IBAction func onExpandClicked(sender: UIButton) {
        
        self.loadStoreDescription()
    }
    
    @IBAction func onExpandCategoryClicked(sender: UIButton) {
        
        sender.selected = !sender.selected
        
        if(sender.selected)
        {
            //Expand
            self.loadNewCategoryRow(1,defaultCnt: self.defaultNumberOfCategory)

        }
        else
        {
            //Collaspan
            
            //remove extra category
            
            for index in self.defaultNumberOfCategory...self.arrayCategory.count
            {
                if let vwCat = self.vwCategoryList.viewWithTag(index)
                {
                    vwCat.removeFromSuperview()
                }
            }
            let difference = self.vwCategoryList.frame.size.height - (self.vwCatListFrame?.size.height)!
            
            self.vwCategoryList.translatesAutoresizingMaskIntoConstraints = true
            self.vwCategoryList.frame = CGRectMake(self.vwCategoryList.frame.origin.x, self.vwCategoryList.frame.origin.y, self.vwCategoryList.frame.size.width, self.vwCategoryList.frame.size.height - difference)
            self.vwCategoryList.setNeedsUpdateConstraints()
            self.vwCategoryList.updateConstraintsIfNeeded()
            self.vwCategoryList.layoutIfNeeded()
            
            
            self.vwContainer.translatesAutoresizingMaskIntoConstraints = true
            self.vwContainer.frame = CGRectMake(self.vwContainer.frame.origin.x, self.vwContainer.frame.origin.y, self.vwContainer.frame.size.width, self.vwContainer.frame.size.height - difference)
            self.vwContainer.setNeedsUpdateConstraints()
            self.vwContainer.updateConstraintsIfNeeded()
            self.vwContainer.layoutIfNeeded()
            self.vwContainer.layoutSubviews()
        }
    }
    //MARK: - Load Data
    func autoScrollToNextImage()
    {
        var nextIndex = 0
        if self.carousel.currentItemIndex < 3
        {
            nextIndex = self.carousel.currentItemIndex + 1
        }
        self.carousel.scrollToItemAtIndex(nextIndex, duration: 1.5)
    }
    func loadData()
    {
        //DLog("storeDetails \(self.dictStoreDetails)")
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            if let displayImage = self.dictStoreDetails[kAPIStoreLogo]?.string
            {
                // optionsInfo: [.Transition(ImageTransition.Fade(1))],
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.imgStoreLogo.kf_showIndicatorWhenLoading = true
                }
                let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                //super.DLog("url of display image \(URL.absoluteString)")
                
                self.imgStoreLogo.kf_setImageWithURL(URL, placeholderImage: nil,
                                                     optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                     progressBlock: { receivedSize, totalSize in
                                                        //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                    },
                                                     completionHandler: { image, error, cacheType, imageURL in
                })
            }

        }
            self.lblStoreTitle.text = self.dictStoreDetails[kAPIStoreName]?.string
        self.lblShopNumber.text = self.dictStoreDetails[kAPIShopNo]?.string
        self.lblStoreLevel.text = self.dictStoreDetails[kAPIStoreLevel]?.string
        self.lblStoreName.text = self.dictStoreDetails[kAPIStoreName]?.string
        
      //  self.lblStoreDesc.text = "Macy's, Inc. is one of the nation's premier omnichannel retailers, with fiscal 2014 sales of $28.1 billion. As of April 4, 2015, the company operates about 900 stores vbmn ,vmb ,vmcbn ,vmbn jghjg kg fgdjsh dfksd fksjdf skdjf skjdf skjdf ksdjf hsdjfh djsfh djhf djsf sdjf dsjfh sdjfh dsjf hjdh jdf k  end"

        self.lblStoreDesc.text = self.dictStoreDetails[kAPIStoreDescription]?.string
        self.loadStoreDescriptionFirstTime()
        
        if let _ = self.lblStoreName.text
        {
            self.lblStoreName.text = self.lblStoreName.text!.uppercaseString
        }
        self.imgStoreLogo.clipsToBounds = true
        
        if let startTime = self.dictStoreDetails[kAPIStartTime]?.string
        {
            self.lblClosingTime.text = self.removePaddingZeroFromDate(startTime)
        }
        
        if let closingTime = self.dictStoreDetails[kAPIEndTime]?.string
        {
            self.lblClosingTime.text = "\(self.lblClosingTime.text!)-\(self.removePaddingZeroFromDate(closingTime))"
        }
        self.loadStoreImage()
        
        if let categoryArray = self.dictStoreDetails[kAPICategory]?.array
        {
            self.arrayCategory = categoryArray
            //print("category--> \(self.arrayCategory)")
            
            if self.arrayCategory.count > 0
            {
                
               /* if IS_TESTING
                {
                    for _ in 1...3
                    {
                        //add more categories
                        for cat in self.arrayCategory
                        {
                            self.arrayCategory.append(cat)
                        }
                    }
                    DLog("Category count \(self.arrayCategory.count)")
                    self.loadStoreCategory()
                }
                else
                {
                    self.loadStoreCategory()
                }*/
                
                self.loadStoreCategory()

            }
        }
    }
    func loadStoreImage()
    {
        self.imgArray.removeAll();
        
        if let storeImage = self.dictStoreDetails[kAPIStorePhoto2]?.string
        {
            self.imgArray.append("\(STORE_IMAGE_BASE_URL)\(storeImage)")
        }
        else
        {
            self.imgArray.append("")
        }
        
        if let storeImage = self.dictStoreDetails[kAPIStoreDisplayImage]?.string
        {
            self.imgArray.append("\(STORE_IMAGE_BASE_URL)\(storeImage)")
        }
        else
        {
            self.imgArray.append("")
        }
        
        if let storeImage = self.dictStoreDetails[kAPIStorePhoto3]?.string
        {
            self.imgArray.append("\(STORE_IMAGE_BASE_URL)\(storeImage)")
        }
        else
        {
            self.imgArray.append("")
        }
        
        self.carousel.reloadData()
        self.carousel.startAnimation()
        
    }
    
    func loadStoreDescriptionFirstTime()
    {        
        
        if self.originalFrame == nil
        {
            self.originalFrame = self.lblStoreDesc.frame
            DLog("container original frame \(self.vwStoreDetails.frame)")
            DLog("lblStoreDesc original frame \(self.originalFrame)")
            
            self.lblStoreDesc.numberOfLines = 0
            self.lblStoreDesc.sizeToFit()
            
            DLog("sizeToFit lblStoreDesc original frame \(self.lblStoreDesc.frame)")

            if(self.lblStoreDesc.frame.size.height < self.originalFrame?.height)
            {
                DLog("no need to change the height")
                self.btnExpand.enabled = false
                self.lblStoreDesc.numberOfLines = 3
                
                self.lblStoreDesc.translatesAutoresizingMaskIntoConstraints = true
                self.lblStoreDesc.frame = self.originalFrame!
                
                self.lblStoreDesc.setNeedsLayout()
                self.lblStoreDesc.layoutIfNeeded()

                return
            }
            else
            {
                DLog("need to change the height")
                self.lblStoreDesc.numberOfLines = 3
                self.lblStoreDesc.frame = self.originalFrame!
                self.btnExpand.enabled = true
            }
        }
    }
    func loadStoreDescription()
    {
        if self.originalFrame == nil
        {
            self.originalFrame = self.lblStoreDesc.frame
            //DLog("container original frame \(self.vwStoreDetails.frame)")
            //DLog("lblStoreDesc original frame \(self.originalFrame)")

            self.lblStoreDesc.numberOfLines = 0
            self.lblStoreDesc.sizeToFit()
            
            if(self.lblStoreDesc.frame.size.height < self.originalFrame?.height)
            {
                DLog("no need to change the height")
                self.btnExpand.enabled = false
                self.lblStoreDesc.numberOfLines = 3
                self.lblStoreDesc.frame = self.originalFrame!
                return
            }
            else
            {
                DLog("need to change the height")
                self.lblStoreDesc.frame = self.originalFrame!
                self.btnExpand.enabled = true
            }
        }
        
        
        let originalFrame = self.lblStoreDesc.frame
        DLog("before frame \(self.lblStoreDesc.frame)")
        DLog("btnExpandFrame \(self.btnExpand.frame)")

        if(self.isExpanded)
        {
            self.isExpanded = false
            self.lblStoreDesc.numberOfLines = 3
            self.lblStoreDesc.translatesAutoresizingMaskIntoConstraints = true
            self.lblStoreDesc.frame = self.originalFrame!
        }
        else
        {
            self.isExpanded = true
            self.lblStoreDesc.numberOfLines = 0
            self.lblStoreDesc.translatesAutoresizingMaskIntoConstraints = true
            self.lblStoreDesc.sizeToFit()
        }
      
        dispatch_async(dispatch_get_main_queue()) {

            self.lblStoreDesc.setNeedsLayout()
            self.lblStoreDesc.layoutIfNeeded()
            
            let difference = self.lblStoreDesc.frame.size.height - originalFrame.height
            
            super.DLog("difference \(difference)")
            self.vwStoreDetails.translatesAutoresizingMaskIntoConstraints = true
            self.vwStoreDetails.frame = CGRectMake(self.vwStoreDetails.frame.origin.x, self.vwStoreDetails.frame.origin.y, self.vwStoreDetails.frame.size.width, self.vwStoreDetails.frame.size.height + difference)
            self.vwStoreDetails.setNeedsUpdateConstraints()
            self.vwStoreDetails.updateConstraintsIfNeeded()
            self.vwStoreDetails.layoutIfNeeded()
            
            self.vwExpansion.translatesAutoresizingMaskIntoConstraints = true
            self.vwExpansion.frame = CGRectMake(self.vwExpansion.frame.origin.x, self.vwExpansion.frame.origin.y + difference, self.vwExpansion.frame.size.width, self.vwExpansion.frame.size.height)
            self.vwExpansion.layoutIfNeeded()
            
            self.vwCategoryList.translatesAutoresizingMaskIntoConstraints = true
            self.vwCategoryList.frame = CGRectMake(self.vwCategoryList.frame.origin.x, self.vwCategoryList.frame.origin.y + difference, self.vwCategoryList.frame.size.width, self.vwCategoryList.frame.size.height)
            self.vwCategoryList.setNeedsUpdateConstraints()
            self.vwCategoryList.updateConstraintsIfNeeded()
            self.vwCategoryList.layoutIfNeeded()
            
            self.vwContainer.translatesAutoresizingMaskIntoConstraints = true
            self.vwContainer.frame = CGRectMake(self.vwContainer.frame.origin.x, self.vwContainer.frame.origin.y, self.vwContainer.frame.size.width, self.vwContainer.frame.size.height + difference)
            self.vwContainer.setNeedsUpdateConstraints()
            self.vwContainer.updateConstraintsIfNeeded()
            self.vwContainer.layoutIfNeeded()
            self.vwContainer.layoutSubviews()

        }
        
    }
    func updateFont()
    {
        // calculate new height
     /*   let currentSize = self.bounds.size
        let factor = currentSize.height / initialSize.height
        
        // calculate point size of font
        let pointSize = initialFont.pointSize * factor;*/
        
        // make same font, but new size
        let fontDescriptor = self.lblStoreDesc.font.fontDescriptor()
        DLog("scale \(fontDescriptor.pointSize)")
      
        var pointSize = CGFloat()
        if(DeviceType.IS_IPHONE_4_OR_LESS)
        {
            pointSize = 10.0
        }
        else if(DeviceType.IS_IPHONE_5)
        {
            pointSize = 11.0
        }
        else if(DeviceType.IS_IPHONE_6)
        {
            pointSize = 13.0
        }
        else
        {
            pointSize = 14.0
        }
        self.lblStoreDesc.font = UIFont(descriptor: fontDescriptor,
            size: pointSize)
    }
    //MARK: Category
    
    func loadStoreCategory()
    {
        //414 = 398
        //x = ?
        
        //638 = 80
        
        self.vwTmpFrame = self.vwTemp.frame
//        self.vwTmpFrame!.size.width = (self.view.frame.size.width * self.vwTemp.frame.size.width)/414
//        self.vwTmpFrame!.size.height = (self.view.frame.size.height * self.vwTemp.frame.size.height)/736
        DLog("frame \(self.vwTmpFrame!)")

        self.originalFrameOfCategoryList = self.vwCategoryList.frame
        
        self.vwCatListFrame = self.vwCategoryList.frame
//        self.vwCatListFrame!.size.width = self.vwTmpFrame!.size.width
//        self.vwCatListFrame!.size.height = self.vwTmpFrame!.size.height * 0.09247648903 //0.125786
        DLog("frame \(self.vwCatListFrame!)")

        var x:CGFloat = (self.startXForCategory * (self.vwCatListFrame?.size.width)!) / (self.originalFrameOfCategoryList?.size.width)!
        let space = (self.spaceBetweenCategory * self.view.frame.size.width)/414.0
        let trallingSpace = x

        DLog("x \(x) trallingSpace \(trallingSpace)")

        for cnt in 0..<self.arrayCategory.count
        {
            //Add next category width to x + trailing
           
            if (self.getSizeForNextCategory(cnt)+x+trallingSpace >= self.vwCatListFrame!.width)
            {
                self.defaultNumberOfCategory = cnt
                break
            }
            
            let vwSubCategory = createCategoryView(cnt, x: x,y:0,categoryListFrame: self.vwCatListFrame!)
           // vwSubCategory.backgroundColor = UIColor.blueColor()
            self.vwCategoryList.addSubview(vwSubCategory)
            x += (vwSubCategory.frame.size.width + space)
        }
        
        if(self.defaultNumberOfCategory == 0)
        {
            self.btnCategoryExpand.hidden = true
        }
        else
        {
            self.btnCategoryExpand.hidden = false
        }
    }
    func getSizeForNextCategory(cnt: Int) -> CGFloat
    {
        let categoryDetails = self.arrayCategory[cnt]
        let fontSize = (10.0 * self.view.frame.size.width)/414.0
        let lblCategoryName = UILabel()
        lblCategoryName.font = UIFont(name: FontForApp.FontBold.rawValue, size:fontSize)
        lblCategoryName.text = categoryDetails[kAPICategoryName].string?.uppercaseString
        lblCategoryName.sizeToFit()
        
        return lblCategoryName.frame.size.width

    }
    func loadNewCategoryRow(rCount: Int,defaultCnt: Int)
    {
        var rowCount = rCount
        var defaultDisplayCayegoryCnt = defaultCnt
        //Expand view height
        
        dispatch_async(dispatch_get_main_queue()) { 
            
            let difference = self.vwCatListFrame!.size.height
            self.vwCategoryList.translatesAutoresizingMaskIntoConstraints = true
            self.vwCategoryList.frame = CGRectMake(self.vwCategoryList.frame.origin.x, self.vwCategoryList.frame.origin.y, self.vwCategoryList.frame.size.width, self.vwCategoryList.frame.size.height+difference)
            self.vwCategoryList.setNeedsUpdateConstraints()
            self.vwCategoryList.updateConstraintsIfNeeded()
            self.vwCategoryList.layoutIfNeeded()
            
            
            self.vwContainer.translatesAutoresizingMaskIntoConstraints = true
            self.vwContainer.frame = CGRectMake(self.vwContainer.frame.origin.x, self.vwContainer.frame.origin.y, self.vwContainer.frame.size.width, self.vwContainer.frame.size.height + difference)
            self.vwContainer.setNeedsUpdateConstraints()
            self.vwContainer.updateConstraintsIfNeeded()
            self.vwContainer.layoutIfNeeded()
            self.vwContainer.layoutSubviews()

        }
        
       // var x:CGFloat = self.lblStoreName.frame.origin.x
        var x:CGFloat = (self.startXForCategory * (self.vwCatListFrame?.size.width)!) / (self.originalFrameOfCategoryList?.size.width)!
        let space = (self.spaceBetweenCategory * self.view.frame.size.width)/414.0
        let trallingSpace = x
        
        DLog("x \(x) trallingSpace \(trallingSpace)")
        
        var needToExpandMore = false
        for cnt in defaultDisplayCayegoryCnt..<self.arrayCategory.count
        {
            if (self.getSizeForNextCategory(cnt)+x+trallingSpace >= self.vwCatListFrame!.width)
            {
                needToExpandMore = true
                defaultDisplayCayegoryCnt = cnt
                break
            }
            
            let vwSubCategory = createCategoryView(cnt, x: x,y:CGFloat(rowCount)*self.vwCatListFrame!.height,categoryListFrame: self.vwCatListFrame!)
            
            vwSubCategory.tag = cnt
            self.vwCategoryList.addSubview(vwSubCategory)
            x += (vwSubCategory.frame.size.width + space)
        }
        
        if(needToExpandMore)
        {
            rowCount += 1
            loadNewCategoryRow(rowCount,defaultCnt: defaultDisplayCayegoryCnt)
        }
    }
    
    func createCategoryView(cnt:Int, x:CGFloat,y:CGFloat, categoryListFrame: CGRect) -> UIView
    {
        let categoryDetails = self.arrayCategory[cnt]
        let iconWidth:CGFloat = 26.0
        let fontSize = (10.0 * self.view.frame.size.width)/414.0

        let lblCategoryName = UILabel()
        lblCategoryName.font = UIFont(name: FontForApp.FontBold.rawValue, size:fontSize)
        lblCategoryName.text = categoryDetails[kAPICategoryName].string?.uppercaseString
        lblCategoryName.sizeToFit()
        lblCategoryName.textColor = COLOR_TEXT_GRAY
        //  lblCategoryName.backgroundColor = UIColor.yellowColor()
        lblCategoryName.frame = CGRectMake(0, categoryListFrame.height - lblCategoryName.frame.size.height, lblCategoryName.frame.size.width, lblCategoryName.frame.size.height)
        
        
        let vwSubCategory = UIView()
        vwSubCategory.frame = CGRectMake(x, y, lblCategoryName.frame.size.width, categoryListFrame.height)
        DLog("cnt \(cnt) frame \(vwSubCategory.frame)")
        //   vwSubCategory.backgroundColor = UIColor(red: CGFloat(cnt * 2 + 25)/255.0, green: 50.0/255.0, blue: 75/255.0, alpha: 1.0)
        
        vwSubCategory.addSubview(lblCategoryName)
        
        let imgCategoryIcon = UIImageView()
       // imgCategoryIcon.image = UIImage(named: self.iconName[cnt])
        imgCategoryIcon.contentMode = .ScaleAspectFit
        // imgCategoryIcon.backgroundColor = UIColor.redColor()
        let imgWidth = (iconWidth * self.view.frame.size.width) / 414.0
        imgCategoryIcon.frame = CGRectMake((vwSubCategory.frame.size.width - imgWidth)/2.0, lblCategoryName.frame.minY - imgWidth - 5, imgWidth, imgWidth)
        
        imgCategoryIcon.kf_showIndicatorWhenLoading = true

        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            if let categoryImg = categoryDetails[kAPICategoryIcon].string
            {
                let URL = NSURL(string:"\(CATEGORY_IMAGE_BASE_URL)\(categoryImg)")!
                
                //super.DLog("image URL \(URL.absoluteString)")
                
                imgCategoryIcon.kf_setImageWithURL(URL, placeholderImage: nil,
                                                   optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                   progressBlock: { receivedSize, totalSize in
                    },
                                                   completionHandler: { image, error, cacheType, imageURL in
                                                    
                                                    super.DLog("image downloaded")
                })
            }
            
        }
                vwSubCategory.addSubview(imgCategoryIcon)
        
        return vwSubCategory
    }
    //MARK: - Carousel Delegate and DataSource
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        return self.imgArray.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        var itemView: UIImageView
        //create new view if no view is available for recycling
        if (view == nil)
        {
            //don't do anything specific to the index within
            //this `if (view == nil) {...}` statement because the view will be
            //recycled and used with other index values later
            
            
            var height:CGFloat
            
           /* if(DeviceType.IS_IPHONE_4_OR_LESS)
            {
               height = (self.view.frame.size.height + 40 - self.vwTop.frame.height-16.0) * CGFloat(0.37578)
            }
            else
            {
                height = (self.view.frame.size.height - self.vwTop.frame.height-16.0) * CGFloat(0.37578)
            }*/
            
          //  let vwTempheight = self.vwTemp.frame.size.height
         //   height = (self.view.frame.size.height - (self.vwTop.frame.height * 0.0869565) - 20 - 16.0) * CGFloat(0.37578)
            height = self.carousel.frame.size.height
            DLog("height \(height)")
            DLog("scroll \(self.carousel.frame)")
            DLog("------vwTemp frame \(self.vwTemp.frame)");
            
            itemView = UIImageView(frame:CGRect(x:0, y:0, width:height, height:height))
         //   itemView.image = UIImage(named: "page.png")
            itemView.contentMode = .ScaleToFill
          //  itemView.clipsToBounds = true
            itemView.backgroundColor = UIColor.whiteColor()

            itemView.layer.shadowColor = UIColor.grayColor().CGColor
            itemView.layer.shadowOffset = CGSizeMake(0.0,0.0)
            itemView.layer.shadowOpacity = 1.0
            itemView.layer.shadowRadius = 6.0
            
            let shadowRect:CGRect = CGRectInset(itemView.bounds, 0, 4)  // inset top/bottom
            itemView.layer.shadowPath = UIBezierPath(rect: shadowRect).CGPath
            
        }
        else
        {
            //get a reference to the label in the recycled view
            itemView = view as! UIImageView;
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            if let displayImageURL = self.imgArray[index]
            {
                dispatch_async(dispatch_get_main_queue()) {
                    
                    itemView.kf_showIndicatorWhenLoading = true
                }
                let URL = NSURL(string:"\(displayImageURL)")!
                
                //super.DLog("image URL \(URL.absoluteString)")
                
                itemView.kf_setImageWithURL(URL, placeholderImage: nil,
                                            optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                            progressBlock: { receivedSize, totalSize in
                    },
                                            completionHandler: { image, error, cacheType, imageURL in
                                                
                                                super.DLog("image downloaded")
                })
            }
        }
        
        dispatch_async(dispatch_get_main_queue()) {

        itemView.alpha = 1.0

        }
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .Spacing)
        {
            return value * 1.1
        }
        return value
    }

    @IBAction func onBackClicked(sender: AnyObject)
    {
        if let _ = self.timerForAutoScroll
        {
            self.timerForAutoScroll!.invalidate()
        }
        self.popSelfViewController()
    }
    
    //MARK: - Get Store Detail
    func getStoreDetails()
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIStoreId] = self.dictStoreDetails[kAPIStoreId]?.stringValue
            self.printAPIURL(APINameInString.APIGetStoreDetails.rawValue, param: param)

         //   BaseVC.sharedInstance.showLoader()
            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIGetStoreDetails.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
              //  BaseVC.sharedInstance.hideLoader()
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            if (status == 1)
                            {
                                self.dictStoreDetails = json[kAPIData].dictionaryValue
                                self.loadData()
                            }
                            
                         //   self.loadCategories()
                        }
                        
                        super.DLog("response \(json)")
                    }
                case .Failure(let error):
                    super.DLog(error)
                    super.DAlert(ALERT_TITLE, message: error.localizedDescription, action: "OK", sender: self)
                }
            }
            
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
           // DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "Ok", sender: self)
        }
    }

    func updateStoreDetailsOnExit(notification: NSNotification)
    {
        self.scrollVw.hidden = true
        self.lblStoreTitle.text = ""
    }
}
