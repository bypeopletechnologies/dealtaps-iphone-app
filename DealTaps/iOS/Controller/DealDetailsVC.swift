//
//  DealDetailsVC.swift
//  DealTaps
//
//  Created by abc on 3/1/16.
//
//

import UIKit
import Kingfisher
import SwiftyJSON
import MessageUI


class DealDetailsVC: BaseVC,iCarouselDataSource, iCarouselDelegate,MFMessageComposeViewControllerDelegate {

    @IBOutlet weak var btnExpand: UIButton!
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var vwStoreDetail: UIView!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTemp: UIView!
    @IBOutlet weak var imgStoreLogo: UIImageView!
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblDealDesc: UILabel!
    @IBOutlet weak var lblDealCostraint: UILabel!
    @IBOutlet weak var lblDealStartDate: UILabel!
    @IBOutlet weak var lblDealClosingTime: UILabel!
    @IBOutlet weak var lblDealEndDate: UILabel!
    var originalFrame:CGRect?

    var timerForAutoScroll:NSTimer?
    var dictDealDetails = Dictionary<String,AnyObject>()
    var arrayDealImgs = [String]()
    var isFirstTime:Bool = false
    var isScanDeal:Bool = false
    var selectedDealId : String!

    //MARK: - View Life Cycle
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DealDetailsVC.updateDealDetailsOnExit(_:)), name: NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL_DETAILS, object: nil)

        
        DLog("dict \(self.dictDealDetails)")
        DLog("dict \(self.arrayDealImgs)")
        DLog("selectedDealId \(selectedDealId)")

        self.btnReport.exclusiveTouch = true
        self.btnShare.exclusiveTouch = true
        self.btnFav.exclusiveTouch = true
        
        self.checkForFav()
        
        self.carousel.clipsToBounds = true
        self.carousel.type = .Rotary
        self.carousel.layer.borderColor = COLOR_LINE_GRAY.CGColor
        self.carousel.layer.borderWidth = 1.0
        self.carousel.scrollEnabled = true

        self.timerForAutoScroll = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(DealDetailsVC.autoScrollToNextImage), userInfo: nil, repeats: true)


        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        DLog("vwStoreDetail frame \(self.vwStoreDetail.frame)")
        DLog("vwTemp frame \(self.vwTemp.frame)")
        DLog(" scrollVw frame \(self.scrollVw.frame)")
        
        self.loadDealData()
    }
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
       
        if(!self.isFirstTime)
        {
            self.isFirstTime = true
          //  self.loadDealData()
        }
        else
        {
            DLog("vwStoreDetail frame \(self.vwStoreDetail.frame)")
            DLog("vwTemp frame \(self.vwTemp.frame)")
            DLog(" scrollVw frame \(self.scrollVw.frame)")
            DLog(" lblDealDesc frame \(self.lblDealDesc.frame)")
           // self.loadDealData()
        }
    }
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL_DETAILS, object: nil);
    }
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated);
        self.scrollVw.hidden = false
        
    }
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated);
    }
    
    override func viewDidDisappear(animated: Bool) {
        if let _ = self.timerForAutoScroll
        {
            self.timerForAutoScroll!.invalidate()
        }
        super.viewDidDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewWillAppear(animated: Bool) {
//        
//    }

    //MARK: - Action
   
    func setFavImage()
    {
        if(self.btnFav.selected)
        {
            self.imgFav.image = UIImage(named: "heart_green_icon.png")
        }
        else
        {
            self.imgFav.image = UIImage(named: "like_icon.png")
        }
    }
    @IBAction func onFavClicked(sender: UIButton)
    {
        sender.selected = !sender.selected
        
        if(sender.selected)
        {
            //Add to Fav
            
            if(self.isScanDeal)
            {
                self.dictDealDetails[kAPIIsScanDeal] = "true"
              //  self.addToFavDeal(self.dictDealDetails, dealId: "\(self.dictDealDetails[kAPIScanDealId] as! String)",dealType:DealType.DealTypeScan)
                self.addToFavDeal(self.dictDealDetails, dealId: "\(self.selectedDealId)",dealType:DealType.DealTypeScan)

            }
            else
            {
                self.dictDealDetails[kAPIIsScanDeal] = "false"
              //  self.addToFavDeal(self.dictDealDetails, dealId: "\(self.dictDealDetails[kAPISimpleDealId] as! String)",dealType:DealType.DealTypeSimple)
                self.addToFavDeal(self.dictDealDetails, dealId: "\(self.selectedDealId)",dealType:DealType.DealTypeSimple)
            }
        }
        else
        {

            //Remove From Fav
//            if(self.isScanDeal)
//            {
//                self.logicalRemoveFromFav(("\((self.dictDealDetails[kAPIScanDealId]!.intValue))"))
//            }
//            else
//            {
//                self.logicalRemoveFromFav(("\((self.dictDealDetails[kAPISimpleDealId]!.intValue))"))
//            }
            
            self.logicalRemoveFromFav(("\((self.selectedDealId))"))

        }
        self.setFavImage()
    }
    @IBAction func onBackClicked(sender: UIButton) {
        
        if let _ = self.timerForAutoScroll
        {
            self.timerForAutoScroll!.invalidate()
        }
        self.popSelfViewController()
    }
    @IBAction func onReportClicked(sender: UIButton)
    {
        let reportVC = STORY_BOARD.instantiateViewControllerWithIdentifier("ReportVC") as! ReportVC
        reportVC.reportType = ReportType.ReportTypeDeal
        
        if(self.isScanDeal)
        {
            reportVC.dealType = DealType.DealTypeSimple
          //  reportVC.reportId = "\((self.dictDealDetails[kAPIScanDealId]?.intValue)!)"
            reportVC.reportId = "\(self.selectedDealId)"
        }
        else
        {
            reportVC.dealType = DealType.DealTypeScan
          //  reportVC.reportId = "\((self.dictDealDetails[kAPIScanDealId]?.intValue)!)"
            reportVC.reportId = "\(self.selectedDealId)"
        }
        appDelegate.navigationController?.pushViewController(reportVC, animated: ANIMATION_FOR_PUSH_POP)
    }
    
    @IBAction func onShareClicked(sender: UIButton)
    {
        let messageComposer = MFMessageComposeViewController()
        messageComposer.messageComposeDelegate = self  //  Make sure to set this property to self, so that the controller can be dismissed!
       // messageComposer.recipients = textMessageRecipients
      //  messageComposer.body = "DealTaps - Awesome deal....Check it out!"
        
        let msgText = "Hey, come check out \(self.dictDealDetails[kAPIStoreName]!) at the \(appDelegate.currentMallDetails![kAPIMallName] as! String)!\n\(self.lblStoreName.text!) - \(self.lblDealCostraint.text!). Deal ends \(self.lblDealEndDate.text!). Save time and money with Dealtaps! \n https://itunes.apple.com/us/app/dealtaps/id1107888685?ls=1&mt=8"
        
        DLog("message text \(msgText)")

        
        if (MFMessageComposeViewController.canSendText())
        {
            messageComposer.body = msgText

            //Remove attached images code
//            for cnt in 0  ..< self.arrayDealImgs.count
//            {
//                let imgVw:UIImageView  = self.carousel.itemViewAtIndex(cnt) as! UIImageView
//                if let img = imgVw.image
//                {
//                    DLog("image is available")
//                    let imageData1 = UIImageJPEGRepresentation(img, 1.0)
//                    messageComposer.addAttachmentData(imageData1!, typeIdentifier: "image/png", filename: "deal\(cnt+1).png")
//                }
//            }
            
            presentViewController(messageComposer, animated: true, completion: nil)
        }
    }

    
    //MARK: - Carousel Delegate and DataSource
    func autoScrollToNextImage()
    {
        var nextIndex = 0
        if self.carousel.currentItemIndex < self.arrayDealImgs.count
        {
            nextIndex = self.carousel.currentItemIndex + 1
        }
        self.carousel.scrollToItemAtIndex(nextIndex, duration: 1.5)
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        return self.arrayDealImgs.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        var itemView: UIImageView
        //create new view if no view is available for recycling
        if (view == nil)
        {
            //don't do anything specific to the index within
            //this `if (view == nil) {...}` statement because the view will be
            //recycled and used with other index values later
            
            // 240 X 270
            // 398 X 270
            
            let height:CGFloat = (self.view.frame.size.height - self.vwTop.frame.height-16.0) * CGFloat(0.424528)
            let width = (self.view.frame.size.width - 16.0) * 240.0 / 398.0
                        
            itemView = UIImageView(frame:CGRect(x:0, y:0, width:width, height:height))
            itemView.contentMode = .ScaleAspectFill
          //  itemView.clipsToBounds = true
            itemView.backgroundColor = UIColor.whiteColor()
            
            itemView.layer.shadowColor = UIColor.grayColor().CGColor
            itemView.layer.shadowOffset = CGSizeMake(0.0,0.0)
            itemView.layer.shadowOpacity = 1.0
            itemView.layer.shadowRadius = 6.0
            
            let shadowRect:CGRect = CGRectInset(itemView.bounds, 0, 4)  // inset top/bottom
            itemView.layer.shadowPath = UIBezierPath(rect: shadowRect).CGPath
            
        }
        else
        {
            //get a reference to the label in the recycled view
            itemView = view as! UIImageView;
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        
        dispatch_async(dispatch_get_main_queue()) {

        itemView.kf_showIndicatorWhenLoading = true
        }
        var URL:NSURL
        if(self.isScanDeal)
        {
             URL = NSURL(string:"\(SCAN_DEAL_IMAGE_BASE_URL)\(self.arrayDealImgs[index])")!
        }
        else
        {
            URL = NSURL(string:"\(DEAL_IMAGE_BASE_URL)\(self.arrayDealImgs[index])")!
        }

        //super.DLog("image URL \(URL.absoluteString)")
        //super.DLog("scale factor \(UIScreen.mainScreen().scale)")
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {

        itemView.kf_setImageWithURL(URL, placeholderImage: nil,
            optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
            progressBlock: { receivedSize, totalSize in
            },
            completionHandler: { image, error, cacheType, imageURL in
                
                super.DLog("image downloaded")
        })
        }
        itemView.alpha = 1.0
        
        
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .Spacing)
        {
            return value * 1.1
        }
        return value
    }

    //MARK: - Load data
    func loadDealData()
    {
        self.updateFont()

        self.dictDealDetails = self.removeNullFromDictionary(self.dictDealDetails) as! Dictionary<String, AnyObject>
        
        DLog("deal Details -> \(self.dictDealDetails)")
        
        self.lblStoreName.text = self.dictDealDetails[kAPIDealBannerTitle] as? String
        
        if let _ = self.lblStoreName.text
        {
            self.lblStoreName.text = self.lblStoreName.text!.uppercaseString
        }
        self.imgStoreLogo.clipsToBounds = true
        
        if let closingTime = self.dictDealDetails[kAPIEndTime] as? String
        {
            self.lblDealClosingTime.text = self.removePaddingZeroFromDate(closingTime)
        }
        
        if let startDate = self.dictDealDetails[kAPIDealStartDate] as? String
        {
            self.lblDealStartDate.text = self.convertDateFromOneFormatToOtherFormat(startDate, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
        }
        
        if let endDate = self.dictDealDetails[kAPIDealEndDate] as? String
        {
            self.lblDealEndDate.text = self.convertDateFromOneFormatToOtherFormat(endDate, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
        }
        
        if self.dictDealDetails[kAPIDealSaleTypeId]?.intValue == 3 
        {
            self.lblDealCostraint.text = self.dictDealDetails[kAPIDealConstraint] as? String
            
        }
        else if self.dictDealDetails[kAPIDealSaleTypeId]?.intValue == 1 || self.dictDealDetails[kAPIDealSaleTypeId]?.intValue == 2
        {
            self.lblDealCostraint.text = "\(self.dictDealDetails[kAPIDealConstraint]!.intValue) \(self.dictDealDetails[kAPIDealSaleType] as! String)"
        }
        else if self.dictDealDetails[kAPIDealSaleTypeId]?.intValue == 4
        {
            self.lblDealCostraint.text = "\(self.dictDealDetails[kAPIDealSaleType] as! String)"
        }
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            if let displayImage = self.dictDealDetails[kAPIStoreLogo] as? String
            {
                // optionsInfo: [.Transition(ImageTransition.Fade(1))],
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.imgStoreLogo.kf_showIndicatorWhenLoading = true
                }
                let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                super.DLog("@@@@@ url of display image \(URL.absoluteString)")
                
                self.imgStoreLogo.kf_setImageWithURL(URL, placeholderImage: nil,
                                                     optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                     progressBlock: { receivedSize, totalSize in
                                                        //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                    },
                                                     completionHandler: { image, error, cacheType, imageURL in
                })
            }
        }
      
        
        if let dealDesc = self.dictDealDetails[kAPIDealDesc] as? String
        {
            
//            self.lblDealDesc.text = "Offer valid for one item at regular price only. Limit one per customer per day. df dhfjk dsjhf kdshjf kjsdf djfh sjf ksjdfh sjhf ksjfh kjhf ksjfh kjsfh kjhf kdjhf jdhf jhf sh fhf df f hf sjcnbv xncbv xncbv mxcnb vxcnbv nvb xcnb vxncbv nb vxnb vxnvb euyriwue rweyrowery dhfsdjfhsd fsdjfh ksjdf jsdf jsdhf sjhf jdsf j fsjhjdfj skcbv cnxv bcn vbcnv cn vnc vncv bcnvb ewweiueiu r hhsdf sdjfsdj fhsdj fdj fsdf vbnxcv nbxcnv bxncbv ehfhdfkjhd fdkfh d hfdjfh kjcb end"
            
             self.lblDealDesc.text = dealDesc

            if self.originalFrame == nil
            {
                self.originalFrame = self.lblDealDesc.frame
                DLog("originalFrame \(self.originalFrame)")
                
                self.lblDealDesc.numberOfLines = 0
                self.lblDealDesc.sizeToFit()
                
                
                if (self.lblDealDesc.frame.size.height < self.originalFrame!.height)
                {
                    DLog("no need to change the height")
                    self.btnExpand.hidden = true
//                    self.lblDealDesc.numberOfLines = 5
//                    
//                    self.lblDealDesc.translatesAutoresizingMaskIntoConstraints = true
//                    self.lblDealDesc.frame = self.originalFrame!
//                    
//                    self.lblDealDesc.setNeedsLayout()
//                    self.lblDealDesc.layoutIfNeeded()
                    
                    return
                }
                else
                {
                    DLog("need to change the height")
                    self.lblDealDesc.numberOfLines = 6
                    self.lblDealDesc.frame = self.originalFrame!
                    self.btnExpand.hidden = false
                }
            }
        }
    }
    
    func updateFont()
    {
        // calculate new height
        /*   let currentSize = self.bounds.size
        let factor = currentSize.height / initialSize.height
        
        // calculate point size of font
        let pointSize = initialFont.pointSize * factor;*/
        
        // make same font, but new size
        let fontDescriptor = self.lblDealDesc.font.fontDescriptor()
        DLog("scale \(fontDescriptor.pointSize)")
        
        var pointSize = CGFloat()
        if(DeviceType.IS_IPHONE_4_OR_LESS)
        {
            pointSize = 10.0
        }
        else if(DeviceType.IS_IPHONE_5)
        {
            pointSize = 12.0
        }
        else if(DeviceType.IS_IPHONE_6)
        {
            pointSize = 13.0
        }
       
        self.lblDealDesc.font = UIFont(descriptor: fontDescriptor,
            size: pointSize)
    }
    func checkForFav()
    {
        
        if (self.isDealInFav("\(self.selectedDealId)"))
        {
            self.btnFav.selected = true
        }
        else
        {
            self.btnFav.selected = false
        }
        self.setFavImage()
        
    }
    //MARK: - MFMessageComposeViewControllerDelegate
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        
        DLog("message send")
        switch(result)
        {
        case MessageComposeResultFailed:
            DLog("error to send message")
        case MessageComposeResultSent:
            DLog("Message is sent successfully")
            self.updateShareCount()
        case MessageComposeResultCancelled:
            DLog("Message sending cancelled by user")
        default:
            DLog("Unexpected error")
        }
        
        dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }

    func updateShareCount()
    {
        if(self.isScanDeal)
        {
          //  self.onShareClicked("\(self.dictDealDetails[kAPIScanDealId] as! String)", dealType:  DealType.DealTypeScan)
            self.onShareClicked("\(self.selectedDealId)", dealType:  DealType.DealTypeScan)
        }
        else
        {
            //self.onShareClicked("\(self.dictDealDetails[kAPISimpleDealId] as! String)", dealType: DealType.DealTypeSimple)
            self.onShareClicked("\(self.selectedDealId)", dealType: DealType.DealTypeSimple)
        }
    }
    
    @IBAction func onExpandClicked(sender: UIButton) {
        
        sender.selected = !sender.selected
        
        var difference:CGFloat = CGFloat()
        
        if(sender.selected)
        {
            self.lblDealDesc.numberOfLines = 0
            self.lblDealDesc.translatesAutoresizingMaskIntoConstraints = true
            self.lblDealDesc.sizeToFit()
            difference = self.lblDealDesc.frame.size.height - self.originalFrame!.height
        }
        else
        {
            difference = -(self.lblDealDesc.frame.size.height - self.originalFrame!.height)
            self.lblDealDesc.numberOfLines = 6
            self.lblDealDesc.translatesAutoresizingMaskIntoConstraints = true
            self.lblDealDesc.frame = self.originalFrame!
        }
        
          self.lblDealDesc.setNeedsLayout()
          self.lblDealDesc.layoutIfNeeded()
        
        //            if (originalFrame.height < self.lblDealDesc.frame.size.height)
        //            {
        //                self.lblDealDesc.setNeedsLayout()
        //                self.lblDealDesc.layoutIfNeeded()
        //            }
        //            else
        //            {
        //                self.lblDealDesc.frame = originalFrame
        //                self.lblDealDesc.setNeedsLayout()
        //                self.lblDealDesc.layoutIfNeeded()
        //            }
        
        
         DLog("difference \(difference)")
         self.vwStoreDetail.translatesAutoresizingMaskIntoConstraints = true
         self.vwStoreDetail.frame = CGRectMake(self.vwStoreDetail.frame.origin.x, self.vwStoreDetail.frame.origin.y, self.vwStoreDetail.frame.size.width, self.vwStoreDetail.frame.size.height + difference)
         self.vwStoreDetail.setNeedsUpdateConstraints()
         self.vwStoreDetail.updateConstraintsIfNeeded()
         self.vwStoreDetail.layoutIfNeeded()
         
         self.vwContainer.translatesAutoresizingMaskIntoConstraints = true
         self.vwContainer.frame = CGRectMake(self.vwContainer.frame.origin.x, self.vwContainer.frame.origin.y, self.vwContainer.frame.size.width, self.vwContainer.frame.size.height + difference)
         self.vwContainer.setNeedsUpdateConstraints()
         self.vwContainer.updateConstraintsIfNeeded()
         self.vwContainer.layoutIfNeeded()
         self.vwContainer.layoutSubviews()
 
    }
    func updateDealDetailsOnExit(notification: NSNotification)
    {
        self.scrollVw.hidden = true
    }
}
