//
//  MallDealListVC.swift
//  DealTaps
//
//  Created by Nimisha on 27/02/16.
//
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class MallDealListVC: BaseVC,UITableViewDataSource,UITableViewDelegate,DWBubbleMenuViewDelegate {
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var imgLoader: UIImageView!
    @IBOutlet weak var noMallAvailableImageView: UIImageView!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var lblMallName: UILabel!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var vwCategoryContainer: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet var vwCategory: UIView!
    var arrayCategoryList:Array<JSON>?
    var arrayDealList:Array<JSON>?
    var selectedCategory:Int? = 0

    var menu:DWBubbleMenuButton!
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_ENTER_FOR_DEAL, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL, object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MallDealListVC.updateDealListOnEnter(_:)), name: NSNOTIFICATION_ON_MALL_ENTER_FOR_DEAL, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MallDealListVC.updateDealListOnExit(_:)), name: NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL, object: nil)

        //Create bottom navigation menu
        self.createBottomMenu()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let selectedMenu:UIButton = self.menu.viewWithTag(2) as! UIButton
        self.menu.setSelectedMenu(selectedMenu)

        
        if IS_TESTING
        {
           // self.lblMallName.text = "Premier Fashion Mall"

            if let selectedCategory = self.selectedCategory
            {
                if let categoryArray = self.arrayCategoryList
                {
                    let categoryDetails = categoryArray[selectedCategory]
                    self.getScanDealFromMall(TEST_MALL_ID, categoryId: "\(categoryDetails[kAPICategoryId].stringValue)")
                }
                else
                {
                    self.getScanDealFromMall(TEST_MALL_ID, categoryId: nil)
                }
            }
            else
            {
                self.getScanDealFromMall(TEST_MALL_ID, categoryId: nil)
            }
            
            self.getMallCategoryList(TEST_MALL_ID)
        }
        else
        {
            if let mallDetails = appDelegate.currentMallDetails
            {
                self.lblMallName.text = mallDetails[kAPIMallName] as? String
                
                if let selectedCategory = self.selectedCategory
                {
                    if let categoryArray = self.arrayCategoryList
                    {
                        let categoryDetails = categoryArray[selectedCategory]
                        self.getScanDealFromMall(mallDetails[kAPIMallId] as! String, categoryId: "\(categoryDetails[kAPICategoryId].stringValue)")
                    }
                    else
                    {
                        self.getScanDealFromMall(mallDetails[kAPIMallId] as! String, categoryId: nil)
                    }
                }
                else
                {
                    self.getScanDealFromMall(mallDetails[kAPIMallId] as! String, categoryId: nil)
                }
                
                self.getMallCategoryList(mallDetails[kAPIMallId] as! String)
                self.btnFilter.hidden = false
            }
            else
            {
                //There is no near by mall
                self.lblMallName.text = ""
                self.btnFilter.hidden = true
                super.DAlert(ALERT_TITLE, message: "Please enter a mall to view deals", action: "OK", sender: self)
            }

        }
        
        self.startAllTimer()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_ENTER_FOR_DEAL, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_EXIT_FOR_DEAL, object: nil)
    }
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
    }
    
    func showHideNoLogo(arrayCount : Int)
    {
        if (arrayCount > 0)
        {
            self.noMallAvailableImageView.hidden = true
            self.imgLoader.hidden = true
        }
        else
        {
            self.noMallAvailableImageView.hidden = false
            self.imgLoader.hidden = false
        }
    }

    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == self.tblVw
        {
            if let _ = self.arrayDealList
            {
                self.showHideNoLogo((self.arrayDealList?.count)!)
                tableView.scrollEnabled = true
                return (self.arrayDealList?.count)!
            }
            else
            {
                self.showHideNoLogo(0)
            }
        }
        else if tableView == self.tblCategory
        {
            if let _ = self.arrayCategoryList
            {
                tableView.scrollEnabled = true
                return (self.arrayCategoryList?.count)!
            }
        }
        
        tableView.scrollEnabled = false
        return 0;
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView == self.tblVw
        {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("MallDealCell", forIndexPath:indexPath ) as! MallDealCell
            cell.isScanDealCell = false
            let storeDeails: Dictionary<String, JSON> = self.arrayDealList![indexPath.row].dictionaryValue
            cell.lblStoreName.text = storeDeails[kAPIDealBannerTitle]?.string
            
            if let _ = cell.lblStoreName.text
            {
                cell.lblStoreName.text = cell.lblStoreName.text!.uppercaseString
            }
            cell.imgStoreLogo.clipsToBounds = true
            
            if let closingTime = storeDeails[kAPIEndTime]?.string
            {
                cell.lblDealClosingTime.text = self.removePaddingZeroFromDate(closingTime)
            }
            
            if let startDate = storeDeails[kAPIDealStartDate]?.string
            {
                cell.lblDealStartDate.text = self.convertDateFromOneFormatToOtherFormat(startDate, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
            }
            
            if storeDeails[kAPIDealSaleTypeId]?.intValue == 3
            {
                cell.lblDealTitle.text = storeDeails[kAPIDealConstraint]?.stringValue
            }
            else if storeDeails[kAPIDealSaleTypeId]?.intValue == 1 || storeDeails[kAPIDealSaleTypeId]?.intValue == 2
            {
                cell.lblDealTitle.text = "\((storeDeails[kAPIDealConstraint]?.stringValue)! as String) \((storeDeails[kAPIDealSaleType]?.stringValue)! as String)"
            }
            else if storeDeails[kAPIDealSaleTypeId]?.intValue == 4
            {
                cell.lblDealTitle.text = "\((storeDeails[kAPIDealSaleType]?.stringValue)! as String)"
            }
            
            
            if let endDate = storeDeails[kAPIDealEndDate]?.string
            {
                cell.lblDealEndDate.text = self.convertDateFromOneFormatToOtherFormat(endDate, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
            }
            
            cell.imgStoreLogo.kf_showIndicatorWhenLoading = true

            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {

                if let displayImage = storeDeails[kAPIStoreLogo]?.string
                {
                    // optionsInfo: [.Transition(ImageTransition.Fade(1))],
                    dispatch_async(dispatch_get_main_queue()) {

                  //  cell.imgStoreLogo.kf_showIndicatorWhenLoading = true
                    }
                    let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                    super.DLog("url of display image \(URL.absoluteString)")
                    
                    cell.imgStoreLogo.kf_setImageWithURL(URL, placeholderImage: nil,
                                                         optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                         progressBlock: { receivedSize, totalSize in
                                                            //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                        },
                                                         completionHandler: { image, error, cacheType, imageURL in
                                                           // print("\(indexPath.row + 1): Finished")
                                                            
                                                            
                    })
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                cell.selectionStyle = .None
              //  cell.updateConstraintsIfNeeded()
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
            }

            
            
            
            cell.arrayDealImgs.removeAll()
            
            //calculate the number of images for deal
            if let dealImg1 = storeDeails[kAPIDealImage1]?.string
            {
                cell.arrayDealImgs.append(dealImg1)
            }
            if let dealImg2 = storeDeails[kAPIDealImage2]?.string
            {
                cell.arrayDealImgs.append(dealImg2)
            }
            if let dealImg3 = storeDeails[kAPIDealImage3]?.string
            {
                cell.arrayDealImgs.append(dealImg3)
            }
            if let dealImg4 = storeDeails[kAPIDealImage4]?.string
            {
                cell.arrayDealImgs.append(dealImg4)
            }
            if let dealImg5 = storeDeails[kAPIDealImage5]?.string
            {
                cell.arrayDealImgs.append(dealImg5)
            }
            
            cell.numberOfDealImages = (cell.arrayDealImgs.count < 3) ? 3 : cell.arrayDealImgs.count
            
            cell.selectionStyle = .None
            
            cell.carousel.reloadData()
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath:indexPath ) as! CategoryCell
            let categoryDetails: Dictionary<String, JSON> = self.arrayCategoryList![indexPath.row].dictionaryValue
            
            if let categoryName = categoryDetails[kAPICategoryName]
            {
                cell.lblCategoryName.text = categoryName.stringValue.uppercaseString
            }
            
            cell.lblCategoryName.textColor = COLOR_TEXT_CELL_GRAY
            
//            if indexPath.row == 0
//            {
//                cell.imgCategory.image = UIImage(named: (categoryDetails[kAPICategoryIcon]?.string)!)
//            }
//            else
//            {
                if let displayImage = categoryDetails[kAPICategoryIcon]?.string
                {
                    dispatch_async(dispatch_get_main_queue()) {

                    cell.imgCategory.kf_showIndicatorWhenLoading = true
                    }
                    let URL = NSURL(string:"\(CATEGORY_IMAGE_BASE_URL)\(displayImage)")!
                    DLog("url of display image \(URL.absoluteString)")
                    
                    cell.imgCategory.kf_setImageWithURL(URL, placeholderImage: nil,
                        optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                        progressBlock: { receivedSize, totalSize in
                            //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                        },
                        completionHandler: { image, error, cacheType, imageURL in
                           // print("\(indexPath.row + 1): Finished")
                            
                            if error == nil
                            {
                                
                                if self.selectedCategory == indexPath.row
                                {
                                    if let _ = cell.imgCategory.image
                                    {
                                        cell.imgCategory.image = cell.imgCategory.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                                        cell.imgCategory.tintColor = COLOR_TEXT_GREEN
                                        cell.lblCategoryName.textColor = COLOR_TEXT_GREEN
                                    }
                                }
                                
                            }

                    })
                }
            
           // }
            
            
            cell.selectionStyle = .None
            return cell
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == self.tblVw
        {
            DLog("selected cell \(self.arrayDealList![indexPath.row])")
            
            //Stop timer of all visible cell
            self.stopAllTimer()
            
            //Get the selected cell
            let selectedCell = self.tblVw.cellForRowAtIndexPath(indexPath) as! MallDealCell
            
            //Call dealClicked API to increment deal visit count
            
            self.onDealClicked("\(self.arrayDealList![indexPath.row][kAPISimpleDealId].stringValue)", dealType: DealType.DealTypeSimple)
            
            let dealDetailVC = STORY_BOARD.instantiateViewControllerWithIdentifier("DealDetailsVC") as! DealDetailsVC
            dealDetailVC.dictDealDetails = self.arrayDealList![indexPath.row].dictionaryObject!
            dealDetailVC.arrayDealImgs = selectedCell.arrayDealImgs
            dealDetailVC.isScanDeal = false
            dealDetailVC.selectedDealId = "\(self.arrayDealList![indexPath.row][kAPISimpleDealId].stringValue)"
            appDelegate.navigationController?.pushViewController(dealDetailVC, animated: ANIMATION_FOR_PUSH_POP)
        }
        else
        {
            if let _ = self.tblCategory.cellForRowAtIndexPath(indexPath)
            {
                let cell = self.tblCategory.cellForRowAtIndexPath(indexPath) as! CategoryCell
                
                if let _ = cell.imgCategory.image
                {
                    cell.imgCategory.image = cell.imgCategory.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                    cell.imgCategory.tintColor = COLOR_TEXT_GREEN
                }
                cell.lblCategoryName.textColor = COLOR_TEXT_GREEN
                self.selectedCategory = indexPath.row
                
            }
        }
    }
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == self.tblCategory
        {
            if let _  = self.tblCategory.cellForRowAtIndexPath(indexPath)
            {
                let cell = self.tblCategory.cellForRowAtIndexPath(indexPath) as! CategoryCell
                
                if let _ = cell.imgCategory.image
                {
                    cell.imgCategory.image = cell.imgCategory.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                }
                //   cell.imgCategory.tintColor = COLOR_TEXT_CELL_GRAY
                cell.lblCategoryName.textColor = COLOR_TEXT_CELL_GRAY
            }
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if tableView == self.tblVw
        {
            //  return self.tblVw.frame.width * 0.3913043478
            return self.tblVw.frame.height * 0.4938271605
        }
        else
        {
            return self.tblCategory.frame.height * 0.1423728814 //0.1328903654    //0.2484
        }
    }
    
    //MARK: Start and Stop Timer
    func stopAllTimer()
    {
        let visibleCells = self.tblVw.visibleCells
        for cell in visibleCells
        {
            let dealCell = cell as! MallDealCell
            if(dealCell.timerForAutoScroll != nil)
            {
                dealCell.timerForAutoScroll?.invalidate()
                dealCell.timerForAutoScroll = nil
            }
        }
    }
    func startAllTimer()
    {
        let visibleCells = self.tblVw.visibleCells
        for cell in visibleCells
        {
            let dealCell = cell as! MallDealCell
            dealCell.startTimer()
        }
    }
    
    //MARK: - Get Deal List
    func getScanDealFromMall(mallId: String, categoryId: String?)
    {
        
        /*
        {
        "dealImage1" : "qrcode_advertise_1_1.png",
        "sale_type_id" : "2",
        "dealImage3" : null,
        "saleType" : "Dollars off",
        "dealEndDate" : "2016-02-27",
        "dealImage5" : null,
        "storeClosingTime" : "07:00 PM",
        "dealDesc" : "ttesst",
        "store_id" : "3",
        "dealImage2" : "qrcode_advertise_1_2.png",
        "storeName" : "ARMANI",
        "storeLogo" : "store_3.png",
        "dealImage4" : "qrcode_advertise_1_4.png",
        "dealConstraint" : "12",
        "dealStartDate" : "2016-02-24",
        "qr_advertise_id" : "1"
        }
        */
        
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIMallId] = mallId
            param[kAPIReportDealType] = "1"

            if let categoryId = categoryId
            {
                if categoryId != "0"
                {
                    param[kAPIMallCategoryId] = categoryId
                }
            }
            
            self.printAPIURL(APINameInString.APIGetDealListOfMall.rawValue, param: param)
            
           // BaseVC.sharedInstance.hideLoader()
           // BaseVC.sharedInstance.showLoader()
            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIGetDealListOfMall.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
             //   BaseVC.sharedInstance.hideLoader()
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            dispatch_async(dispatch_get_main_queue()) {
                                if (status == 1)
                                {
                                    self.arrayDealList = json[kAPIData].arrayValue
                                    self.tblVw.reloadData()
                                }
                                else
                                {
                                    self.arrayDealList = nil
                                    self.tblVw.reloadData()
                                    super.DAlert(ALERT_TITLE, message: json[kAPIMessage].stringValue, action: "OK", sender: self)
                                }
                            }
                        }
                        
                        super.DLog("response \(json)")
                    }
                case .Failure(let error):
                    super.DLog(error)
                    super.DAlert(ALERT_TITLE, message: error.localizedDescription, action: "OK", sender: self)
                }
            }
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }
    //MARK: - Bottom Navigation Menu
    
    func createBottomMenu()
    {
        // Create up menu button
        let homeLabel2 =  self.createHomeButtonView()
        
        self.menu = DWBubbleMenuButton(frame: CGRectMake(self.view.frame.size.width - homeLabel2.frame.size.width - 10.0,self.view.frame.size.height - homeLabel2.frame.size.height - 10.0,
            homeLabel2.frame.size.width,homeLabel2.frame.size.height),expansionDirection: .DirectionLeft)
        self.menu.homeButtonView = homeLabel2
        self.menu.delegate = self
        self.menu.backgroundColor = UIColor.clearColor()
        
        let selectedMenu:UIButton = self.menu.viewWithTag(2) as! UIButton
        self.menu.setSelectedMenu(selectedMenu)
        self.view.addSubview(self.menu)
    }
    
    func createHomeButtonView() -> UIImageView {
        
        let bgImgVw = UIImageView(frame: CGRectMake(0.0, 0.0, ScreenSize.SCREEN_WIDTH * 0.1835, ScreenSize.SCREEN_WIDTH * 0.1835))
        DLog("frame \(bgImgVw.frame)")
        bgImgVw.image = UIImage(named: "tag_icon.png")
        bgImgVw.clipsToBounds = true;
        
        return bgImgVw
    }
    
    //MARK: Menu delegate method
    func bubbleMenuButtonDidSelect(expandableView: DWBubbleMenuButton, selectedMenuItem: UIButton) {
        
        DLog("sender.tag \(selectedMenuItem.tag)")
        
        if(selectedMenuItem.tag == 0)
        {
            dispatch_async(dispatch_get_main_queue(), { 
                
                self.initQRCodeScanner()
            })

        }
        else if(selectedMenuItem.tag == 1)
        {
            let mallDealVC = STORY_BOARD.instantiateViewControllerWithIdentifier("MallStoreListVC") as! MallStoreListVC
            super.pushToViewControllerIfNotExistWithClassName(mallDealVC, identifier: "MallStoreListVC", animated: ANIMATION_FOR_PUSH_POP)
        }
        else if(selectedMenuItem.tag == 2)
        {
            //Default Menu Selection - Deal
        }
        else if(selectedMenuItem.tag == 3)
        {
            let favDealVC = STORY_BOARD.instantiateViewControllerWithIdentifier("FavDealListVC") as! FavDealListVC
            super.pushToViewControllerIfNotExistWithClassName(favDealVC, identifier: "FavDealListVC", animated: ANIMATION_FOR_PUSH_POP)
        }
    }
    
    func bubbleMenuButtonDidCollapse(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonWillCollapse(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonDidExpand(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonWillExpand(expandableView: DWBubbleMenuButton) {
        
    }
    
    //MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        if let _ = self.vwCategory.superview
        {
            self.menu.alpha = 1.0
        }
        else
        {
            self.menu.alpha = 0.5
        }
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.menu.alpha = 1.0
    }
//    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        self.menu.alpha = 1.0
//    }
  
    //MARK: - Update List From Notification
    func updateDealListOnEnter(notification: NSNotification)
    {
        //Check for mall details availabel or not
        if let mallDetails = appDelegate.currentMallDetails
        {
            self.lblMallName.text = mallDetails[kAPIMallName] as? String
            self.getScanDealFromMall(mallDetails[kAPIMallId] as! String, categoryId: nil)
        }
    }
    func updateDealListOnExit(notification: NSNotification)
    {
        self.arrayDealList = nil
        self.tblVw.reloadData()
    }
    @IBAction func onFilterClicked(sender: UIButton) {
                
        if self.vwCategory.superview != nil
        {
            self.vwCategory.removeFromSuperview()
        }
        self.vwCategory.frame = self.view.frame
        
        //        self.imgBlur.image = self.updateBlur()
        //        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Light)
        //        let blurView = UIVisualEffectView(effect: darkBlur)
        //        blurView.frame = self.imgBlur.bounds
        //        self.imgBlur.hidden = false
        //        self.imgBlur.addSubview(blurView)
        
        self.view.addSubview(self.vwCategory)
        self.tblCategory.reloadData()
        
        if let selectedCat = self.selectedCategory
        {
            if let _ = self.arrayCategoryList
            {
                tableView(self.tblCategory, didSelectRowAtIndexPath: NSIndexPath(forRow: selectedCat, inSection: 0))
                self.tblCategory.selectRowAtIndexPath(NSIndexPath(forRow: selectedCat, inSection: 0), animated: false, scrollPosition: .None)
            }
        }
    }
    
    @IBAction func onSelectCategory(sender: UIButton) {
        
        if self.vwCategory.superview != nil
        {
            self.vwCategory.removeFromSuperview()
        }
        
        if let selectedIndexPath = self.tblCategory.indexPathForSelectedRow
        {
            DLog("selected category \(self.arrayCategoryList![selectedIndexPath.row])")
            
            let categoryDetails = self.arrayCategoryList![selectedIndexPath.row]
            
            if IS_TESTING
            {
                //All Store
                if categoryDetails[kAPICategoryId] == "-101"
                {
                    self.getScanDealFromMall(TEST_MALL_ID, categoryId: nil)
                }
                else
                {
                    self.getScanDealFromMall(TEST_MALL_ID, categoryId: categoryDetails[kAPICategoryId].stringValue)
                }
            }
            else
            {
                //Check for mall details available or not
                if let dealDetails = appDelegate.currentMallDetails
                {
                    if categoryDetails[kAPICategoryId] == "-101"
                    {
                        self.getScanDealFromMall(dealDetails[kAPIMallId] as! String, categoryId: nil)
                    }
                    else
                    {
                        self.getScanDealFromMall(dealDetails[kAPIMallId] as! String, categoryId: categoryDetails[kAPICategoryId].stringValue)
                    }
                }
            }
        }
    }
    func addAllStoreCategoryToList()
    {
        self.tblCategory.reloadData()
        
        if let selectedCat = self.selectedCategory
        {
            if let _ = self.arrayCategoryList
            {
                tableView(self.tblCategory, didSelectRowAtIndexPath: NSIndexPath(forRow: selectedCat, inSection: 0))
                self.tblCategory.selectRowAtIndexPath(NSIndexPath(forRow: selectedCat, inSection: 0), animated: false, scrollPosition: .None)
            }
        }
    }
    
    //MARK: - Mall Category
    func getMallCategoryList(mallId: String)
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIMallId] = mallId
            param[kAPICategoryAsDefault] = DefaultCategoryAs.HotDeals.rawValue

            self.printAPIURL(APINameInString.APIGetAllCategoryListForMall.rawValue, param: param)

            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIGetAllCategoryListForMall.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            super.DLog("response \(json)")

                            if (status == 1)
                            {
                                if let _ = json[kAPIData][kAPICategory].array
                                {
                                    self.arrayCategoryList = json[kAPIData][kAPICategory].arrayValue
                                    
                                    if let defalutCategoryList = json[kAPIData][kAPICategoryAsDefault].dictionary
                                    {
                                        let allStoreCategory = JSON(defalutCategoryList)
                                        
                                        if let _ = self.arrayCategoryList
                                        {
                                            self.arrayCategoryList?.insert(allStoreCategory, atIndex: 0)
                                        }
                                        else
                                        {
                                            self.arrayCategoryList = Array<JSON>()
                                            self.arrayCategoryList?.insert(allStoreCategory, atIndex: 0)
                                        }
                                    }
                                }
                                else if let defalutCategoryList = json[kAPIData][kAPICategoryAsDefault].dictionary
                                {
                                    let allStoreCategory = JSON(defalutCategoryList)
                                    self.arrayCategoryList = Array<JSON>()
                                    self.arrayCategoryList?.insert(allStoreCategory, atIndex: 0)
                                    
                                }
                                self.DLog("all category for mall stores \(self.arrayCategoryList!)")
                                self.addAllStoreCategoryToList()
                            }

                        }
                        
                    }
                case .Failure(let error):
                    super.DLog(error)
                    super.DAlert(ALERT_TITLE, message: error.localizedDescription, action: "OK", sender: self)
                }
            }
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }

}
