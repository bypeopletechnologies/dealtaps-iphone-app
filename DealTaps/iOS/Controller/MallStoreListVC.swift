//
//  ViewController.swift
//  Dealtaps
//
//  Created by Nimisha on 10/02/16.
//  Copyright © 2016 Nimisha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import AVFoundation
import AlamofireImage

class MallStoreListVC: BaseVC,UITableViewDataSource,UITableViewDelegate,DWBubbleMenuViewDelegate {

    @IBOutlet weak var imgLoader: UIImageView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var vwCategoryContainer: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet var vwCategory: UIView!
    @IBOutlet weak var lblMallName: UILabel!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var noMallAvailableImageView: UIImageView!
    
    var arrayStoreList:Array<JSON>?
    var arrayCategoryList:Array<JSON>?
    var menu:DWBubbleMenuButton!
    var selectedCategory:Int? = 0
    var alertForNoMall = UIAlertController?()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_ENTER, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_EXIT, object: nil)

        //Add notification observer for mall enter and exit
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MallStoreListVC.updateMallListOnEnter(_:)), name: NSNOTIFICATION_ON_MALL_ENTER, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MallStoreListVC.updateMallListOnExit(_:)), name: NSNOTIFICATION_ON_MALL_EXIT, object: nil)

        self.rotateLayerInfinite(self.imgLoader.layer)
        //Create the bottom navigation menu
        self.createBottomMenu()
    }

    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let selectedMenu:UIButton = self.menu.viewWithTag(1) as! UIButton
        self.menu.setSelectedMenu(selectedMenu)

        
        if IS_TESTING
        {
            //self.lblMallName.text = "Premier Fashion Mall"
            if let selectedCategory = self.selectedCategory
            {
                if let categoryArray = self.arrayCategoryList
                {
                    let categoryDetails = categoryArray[selectedCategory]
                    self.getStoreListFromMall(TEST_MALL_ID, categoryId: "\(categoryDetails[kAPICategoryId].stringValue)")
                }
                else
                {
                    self.getStoreListFromMall(TEST_MALL_ID, categoryId: nil)
                }
            }
            else
            {
                self.getStoreListFromMall(TEST_MALL_ID, categoryId: nil)
            }
            
            self.getMallCategoryList(TEST_MALL_ID)
        }
        else
        {
            //Check for mall details available or not
            if let mallDetails = appDelegate.currentMallDetails
            {
                self.lblMallName.text = mallDetails[kAPIMallName] as? String
                
                if let selectedCategory = self.selectedCategory
                {
                    if let categoryArray = self.arrayCategoryList
                    {
                        let categoryDetails = categoryArray[selectedCategory]
                        self.getStoreListFromMall(mallDetails[kAPIMallId] as! String, categoryId: "\(categoryDetails[kAPICategoryId].stringValue)")
                    }
                    else
                    {
                        self.getStoreListFromMall(mallDetails[kAPIMallId] as! String, categoryId: nil)
                    }
                }
                else
                {
                    self.getStoreListFromMall(mallDetails[kAPIMallId] as! String, categoryId: nil)
                }
                self.getMallCategoryList(mallDetails[kAPIMallId] as! String)
                self.btnFilter.hidden = false
            }
            else
            {
                //There is no near by mall
                self.lblMallName.text = ""
                self.btnFilter.hidden = true
               // super.DAlert(ALERT_TITLE, message: ALERT_ENTER_A_MALL_TO_VIEW_STORE, action: "OK", sender: self)
                
                if objc_getClass("UIAlertController") != nil
                {
                    self.alertForNoMall = UIAlertController(title: ALERT_TITLE, message: ALERT_ENTER_A_MALL_TO_VIEW_STORE, preferredStyle: UIAlertControllerStyle.Alert)
                    self.alertForNoMall!.addAction(UIAlertAction(title: ALERT_ACTION_OK, style: UIAlertActionStyle.Default, handler:nil))
                    self.presentViewController(self.alertForNoMall!, animated: true, completion: nil)
                }
                else {
                    let alert = UIAlertView(title: ALERT_TITLE, message: ALERT_ENTER_A_MALL_TO_VIEW_STORE, delegate: self, cancelButtonTitle:ALERT_ACTION_OK)
                    alert.show()
                }
            }
        }
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_ENTER, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_EXIT, object: nil)        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblVw
        {
            if let _ = self.arrayStoreList
            {
                self.showHideNoLogo((self.arrayStoreList?.count)!)
                tableView.scrollEnabled = true
                return (self.arrayStoreList?.count)!
            }
            else
            {
                self.showHideNoLogo(0)
            }
        }
        else if tableView == self.tblCategory
        {
            if let _ = self.arrayCategoryList
            {
               // self.showHideNoLogo((self.arrayCategoryList?.count)!)
                tableView.scrollEnabled = true
                return (self.arrayCategoryList?.count)!
            }
        }
        
        tableView.scrollEnabled = false
        return 0;
    }
    
    func showHideNoLogo(arrayCount : Int)
    {
        if (arrayCount > 0)
        {
            self.noMallAvailableImageView.hidden = true
            self.imgLoader.hidden = true
        }
        else
        {
            self.noMallAvailableImageView.hidden = false
            self.imgLoader.hidden = false
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView == self.tblVw
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("StoreLogoCell", forIndexPath:indexPath ) as! StoreLogoCell
            let storeDeails: Dictionary<String, JSON> = self.arrayStoreList![indexPath.row].dictionaryValue
            cell.lblShopNumber.text = storeDeails[kAPIShopNo]?.string
            cell.lblFloorNumber.text = storeDeails[kAPIStoreLevel]?.string
            cell.lblStoreName.text = storeDeails[kAPIStoreName]?.string
            
            if let _ = cell.lblStoreName.text
            {
                cell.lblStoreName.text = cell.lblStoreName.text!.uppercaseString
            }
            cell.imgMainPage.clipsToBounds = true
            cell.imgStoreLogo.clipsToBounds = true
            
            if let closingTime = storeDeails[kAPIEndTime]?.string
            {
                cell.lblClosingTime.text = self.removePaddingZeroFromDate(closingTime)
            }
            
            cell.imgMainPage.kf_showIndicatorWhenLoading = true

            if USED_IMAGES == USED_KINGFISHER
            {
                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                dispatch_async(dispatch_get_global_queue(priority, 0)) {
                    
                    if let displayImage = storeDeails[kAPIStoreMainPhoto]?.string
                    {
                       
                        let URL = NSURL(string:"\(STORE_IMAGE_BASE_URL)\(displayImage)")!
                        super.DLog("url of display image \(URL.absoluteString)")
                        
                        //                optionsInfo: [.Transition(ImageTransition.Fade(1))],
                        
                        cell.imgMainPage.kf_setImageWithURL(URL, placeholderImage: nil,
                                                            optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                            progressBlock: { receivedSize, totalSize in
                                                                //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                            },
                                                            completionHandler: { image, error, cacheType, imageURL in
                                                               // print("\(indexPath.row + 1): Finished")
                        })
                        
                        
                    }
                    
                    if let displayImage = storeDeails[kAPIStoreLogo]?.string
                    {
                        // optionsInfo: [.Transition(ImageTransition.Fade(1))],
                        dispatch_async(dispatch_get_main_queue()) {
                            cell.imgStoreLogo.kf_showIndicatorWhenLoading = true
                        }
                        let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                        super.DLog("url of display image \(URL.absoluteString)")
                        
                        cell.imgStoreLogo.kf_setImageWithURL(URL, placeholderImage: nil,
                                                             optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                             progressBlock: { receivedSize, totalSize in
                                                                //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                            },
                                                             completionHandler: { image, error, cacheType, imageURL in
                                                               // print("\(indexPath.row + 1): Finished")
                        })
                    }
                    
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    cell.selectionStyle = .None
                  //  cell.updateConstraintsIfNeeded()
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                }
                
                
            }
            else if USED_IMAGES == USED_STATIC_IMAGE
            {
                cell.imgMainPage.image = UIImage(named: "store_photo_3_1461576450.png")
                cell.imgStoreLogo.image = UIImage(named: "brand_name.png")
            }
            else if USED_IMAGES == USED_ALOMFIRE_IMAGES
            {
                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                dispatch_async(dispatch_get_global_queue(priority, 0)) {
                    
                    if let displayImage = storeDeails[kAPIStoreLogo]?.string
                    {
                        let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                        
                        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                            size: cell.imgStoreLogo.frame.size,
                            radius: 0.0
                        )
                        cell.imgStoreLogo.af_setImageWithURL(URL, placeholderImage: nil, filter: filter, imageTransition: .None, completion: { (response) -> Void in
                            
                            if let error = response.result.error as? AnyObject
                            {
                                self.DLog(error) //# NSError
                            }
                            else
                            {
                                self.DLog(response.result.value!) //# UIImage
                            }
                        })
                    }
                    
                    
                    if let displayImage = storeDeails[kAPIStoreMainPhoto]?.string
                    {
                        let URL = NSURL(string:"\(STORE_IMAGE_BASE_URL)\(displayImage)")!
                        
                        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                            size: cell.imgMainPage.frame.size,
                            radius: 0.0
                        )
                        cell.imgMainPage.af_setImageWithURL(URL, placeholderImage: nil, filter: filter, imageTransition: .None, completion: { (response) -> Void in
                            
                            if let error = response.result.error as? AnyObject
                            {
                                self.DLog(error) //# NSError
                            }
                            else
                            {
                                self.DLog(response.result.value!) //# UIImage
                            }
                        })
                    }
                    
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    cell.selectionStyle = .None
                    cell.updateConstraintsIfNeeded()
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                }
            }
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath:indexPath ) as! CategoryCell
            let categoryDetails: Dictionary<String, JSON> = self.arrayCategoryList![indexPath.row].dictionaryValue
            
            if let categoryName = categoryDetails[kAPICategoryName]
            {
                cell.lblCategoryName.text = categoryName.stringValue.uppercaseString
            }
            
            cell.lblCategoryName.textColor = COLOR_TEXT_CELL_GRAY
            
            cell.imgCategory.kf_showIndicatorWhenLoading = true

            
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
                if let displayImage = categoryDetails[kAPICategoryIcon]
                {

                    let URL = NSURL(string:"\(CATEGORY_IMAGE_BASE_URL)\(displayImage)")!
                    super.DLog("url of display image \(URL.absoluteString)")
                    
                    cell.imgCategory.kf_setImageWithURL(URL, placeholderImage: nil,
                                                        optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                        progressBlock: { receivedSize, totalSize in
                                                            //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                        },
                                                        completionHandler: { image, error, cacheType, imageURL in
                                                           // print("\(indexPath.row + 1): Finished")
                                                            
                                                            if error == nil
                                                            {
                                                                
                                                                if self.selectedCategory == indexPath.row
                                                                {
                                                                    if let _ = cell.imgCategory.image
                                                                    {
                                                                        cell.imgCategory.image = cell.imgCategory.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                                                                        cell.imgCategory.tintColor = COLOR_TEXT_GREEN
                                                                        cell.lblCategoryName.textColor = COLOR_TEXT_GREEN
                                                                    }
                                                                }

                                                            }
                    })
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                cell.selectionStyle = .None
              //  cell.updateConstraintsIfNeeded()
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
            }
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == self.tblVw
        {
            DLog("selected cell \(self.arrayStoreList![indexPath.row])")
            
            let storeDetailVC = STORY_BOARD.instantiateViewControllerWithIdentifier("StoreDetailsVC") as! StoreDetailsVC
            storeDetailVC.dictStoreDetails = self.arrayStoreList![indexPath.row].dictionary!
            appDelegate.navigationController?.pushViewController(storeDetailVC, animated: ANIMATION_FOR_PUSH_POP)
        }
        else
        {
            if let _ = self.tblCategory.cellForRowAtIndexPath(indexPath)
            {
                let cell = self.tblCategory.cellForRowAtIndexPath(indexPath) as! CategoryCell
                
                DLog("1. selected cat \(cell.lblCategoryName.text)")
                if let _ = cell.imgCategory.image
                {
                    cell.imgCategory.image = cell.imgCategory.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                    cell.imgCategory.tintColor = COLOR_TEXT_GREEN
                }
                cell.lblCategoryName.textColor = COLOR_TEXT_GREEN
                self.selectedCategory = indexPath.row
                
            }
        }
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == self.tblCategory
        {
            if let _ = self.tblCategory.cellForRowAtIndexPath(indexPath)
            {
                let cell = self.tblCategory.cellForRowAtIndexPath(indexPath) as! CategoryCell
                
                DLog("deselected cat \(cell.lblCategoryName.text)")
                if let _ = cell.imgCategory.image
                {
                    cell.imgCategory.image = cell.imgCategory.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                }
                //   cell.imgCategory.tintColor = COLOR_TEXT_CELL_GRAY
                cell.lblCategoryName.textColor = COLOR_TEXT_CELL_GRAY
            }
            
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if tableView == self.tblVw
        {
            //  return self.tblVw.frame.width * 0.3913043478
            return self.tblVw.frame.height * 0.25    //0.2484
        }
        else
        {
            return self.tblCategory.frame.height * 0.1423728814 //0.1328903654    //0.2484
        }
        
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == self.tblVw
        {
            //  return self.tblVw.frame.width * 0.3913043478
            return self.tblVw.frame.height * 0.25    //0.2484
        }
        else
        {
            return self.tblCategory.frame.height * 0.1423728814 //0.1328903654    //0.2484
        }
    }
    
    //MARK: - Load Store List

    func callSampleAPI()
    {
//        Alamofire.request(.GET, "http://httpbin.org/get").validate().responseJSON { response in
//            switch response.result {
//            case .Success:
//                if let value = response.result.value {
//                    let json = JSON(value)
//                    
//                    print("JSON: \(json)")
//                }
//            case .Failure(let error):
//                print(error)
//            }
//        }
    }
    
    func getStoreListFromMall(mallId: String, categoryId: String?)
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIMallId] = mallId
            
            if let categoryId = categoryId
            {
                if categoryId != "0"
                {
                    param[kAPIMallCategoryId] = categoryId
                }
            }
            
            //   BaseVC.sharedInstance.hideLoader()
            //   BaseVC.sharedInstance.showLoader()
            
            self.printAPIURL(APINameInString.APIGetStoreList.rawValue, param: param)
            
            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIGetStoreList.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                //  BaseVC.sharedInstance.hideLoader()
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            dispatch_async(dispatch_get_main_queue()) {
                                if (status == 1)
                                {
                                    self.arrayStoreList = json[kAPIData].arrayValue
                                    self.tblVw.reloadData()
                                }
                                else
                                {
                                    self.arrayStoreList = nil
                                    self.tblVw.reloadData()
                                    super.DAlert(ALERT_TITLE, message: json[kAPIMessage].stringValue, action: "OK", sender: self)
                                }
                            }
                        }
                        
                        super.DLog("response \(json)")
                    }
                case .Failure(let error):
                    super.DLog(error)
                    super.DAlert(ALERT_TITLE, message: error.localizedDescription, action: "OK", sender: self)
                }
            }
            
        }
        else
        {
           Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }
    //MARK: - Bottom Navigation Menu

    func createBottomMenu()
    {
        // Create up menu button
        let homeLabel2 =  self.createHomeButtonView()
        
        self.menu = DWBubbleMenuButton(frame: CGRectMake(self.view.frame.size.width - homeLabel2.frame.size.width - 10.0,self.view.frame.size.height - homeLabel2.frame.size.height - 10.0,
            homeLabel2.frame.size.width,homeLabel2.frame.size.height),expansionDirection: .DirectionLeft)
        self.menu.homeButtonView = homeLabel2
        self.menu.delegate = self
        self.menu.backgroundColor = UIColor.clearColor()
        
        let selectedMenu:UIButton = self.menu.viewWithTag(1) as! UIButton
        self.menu.setSelectedMenu(selectedMenu)

        self.view.addSubview(self.menu)
    }
    
    func createHomeButtonView() -> UIImageView {
        
        let bgImgVw = UIImageView(frame: CGRectMake(0.0, 0.0, ScreenSize.SCREEN_WIDTH * 0.1835, ScreenSize.SCREEN_WIDTH * 0.1835))
        DLog("frame \(bgImgVw.frame)")
        bgImgVw.image = UIImage(named: "home_icon.png")
        bgImgVw.clipsToBounds = true;
        
        return bgImgVw
    }

    //MARK: Menu delegate method
    func bubbleMenuButtonDidSelect(expandableView: DWBubbleMenuButton, selectedMenuItem: UIButton) {
        
        DLog("sender.tag \(selectedMenuItem.tag)")
        
        if(selectedMenuItem.tag == 0)
        {
            if(IS_TESTING)
            {
                // "1269558200" "1133490715"
                self.loadScanResultScreen("1299107090")
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), {

                self.initQRCodeScanner()
                })
            }
            
        }
        else if(selectedMenuItem.tag == 1)
        {
            DLog("----2----");
        }
        else if(selectedMenuItem.tag == 2)
        {
            let mallDealVC = STORY_BOARD.instantiateViewControllerWithIdentifier("MallDealListVC") as! MallDealListVC
            super.pushToViewControllerIfNotExistWithClassName(mallDealVC, identifier: "MallDealListVC", animated: ANIMATION_FOR_PUSH_POP)
        }
        else if(selectedMenuItem.tag == 3)
        {
            let favDealVC = STORY_BOARD.instantiateViewControllerWithIdentifier("FavDealListVC") as! FavDealListVC
            super.pushToViewControllerIfNotExistWithClassName(favDealVC, identifier: "FavDealListVC", animated: ANIMATION_FOR_PUSH_POP)
        }
    }
    
    func bubbleMenuButtonDidCollapse(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonWillCollapse(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonDidExpand(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonWillExpand(expandableView: DWBubbleMenuButton) {
        
    }
    //MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        
        if let _ = self.vwCategory.superview
        {
            self.menu.alpha = 1.0
        }
        else
        {
            self.menu.alpha = 0.5
        }
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.menu.alpha = 1.0
    }
    //MARK: - Update List From Notification
    func updateMallListOnEnter(notification: NSNotification)
    {
        //Check for mall details availabel or not
        if let mallDetails = appDelegate.currentMallDetails
        {
            if let alert = self.alertForNoMall
            {
                alert.dismissViewControllerAnimated(true, completion: { () -> Void in
                    
                    self.alertForNoMall = nil
                })
            }
            self.lblMallName.text = mallDetails[kAPIMallName] as? String
            self.getStoreListFromMall(mallDetails[kAPIMallId] as! String, categoryId: nil)
            self.getMallCategoryList(mallDetails[kAPIMallId] as! String)
        }
    }
    func updateMallListOnExit(notification: NSNotification)
    {
        self.lblMallName.text = ""
        self.arrayStoreList = nil
        self.tblVw.reloadData()
    }
    //MARK: - Mall Category
    func getMallCategoryList(mallId: String)
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIMallId] = mallId
            param[kAPICategoryAsDefault] = DefaultCategoryAs.AllStores.rawValue
            
            self.printAPIURL(APINameInString.APIGetAllCategoryListForMall.rawValue, param: param)
            
            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIGetAllCategoryListForMall.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            super.DLog("response \(json)")
                            
                            dispatch_async(dispatch_get_main_queue()) {
                                if (status == 1)
                                {
                                    if let _ = json[kAPIData][kAPICategory].array
                                    {
                                        self.arrayCategoryList = json[kAPIData][kAPICategory].arrayValue
                                        
                                        if let defalutCategoryList = json[kAPIData][kAPICategoryAsDefault].dictionary
                                        {
                                            let allStoreCategory = JSON(defalutCategoryList)
                                            
                                            if let _ = self.arrayCategoryList
                                            {
                                                self.arrayCategoryList?.insert(allStoreCategory, atIndex: 0)
                                            }
                                            else
                                            {
                                                self.arrayCategoryList = Array<JSON>()
                                                self.arrayCategoryList?.insert(allStoreCategory, atIndex: 0)
                                            }
                                        }
                                    }
                                    else if let defalutCategoryList = json[kAPIData][kAPICategoryAsDefault].dictionary
                                    {
                                        let allStoreCategory = JSON(defalutCategoryList)
                                        self.arrayCategoryList = Array<JSON>()
                                        self.arrayCategoryList?.insert(allStoreCategory, atIndex: 0)
                                        
                                    }
                                    self.DLog("all category for mall stores \(self.arrayCategoryList!)")
                                    self.addAllStoreCategoryToList()
                                }
                                
                                
                            }
                        }
                        
                    }
                case .Failure(let error):
                    super.DLog(error)
                    super.DAlert(ALERT_TITLE, message: error.localizedDescription, action: "OK", sender: self)
                }
            }
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }
    @IBAction func onSelectCategory(sender: UIButton) {
        
        if self.vwCategory.superview != nil
        {
            self.vwCategory.removeFromSuperview()
        }
        
        if let selectedIndexPath = self.tblCategory.indexPathForSelectedRow
        {
            DLog("selected category \(self.arrayCategoryList![selectedIndexPath.row])")
            
            let categoryDetails = self.arrayCategoryList![selectedIndexPath.row]
            
            if IS_TESTING
            {
                //All Store
                if categoryDetails[kAPICategoryId] == "-101"
                {
                    self.getStoreListFromMall(TEST_MALL_ID, categoryId: nil)
                }
                else
                {
                    self.getStoreListFromMall(TEST_MALL_ID, categoryId: categoryDetails[kAPICategoryId].stringValue)
                }
            }
            else
            {
                //Check for mall details available or not
                if let mallDetails = appDelegate.currentMallDetails
                {
                    if categoryDetails[kAPICategoryId] == "-101"
                    {
                        self.getStoreListFromMall(mallDetails[kAPIMallId] as! String, categoryId: nil)
                    }
                    else
                    {
                        self.getStoreListFromMall(mallDetails[kAPIMallId] as! String, categoryId: categoryDetails[kAPICategoryId].stringValue)
                    }
                }
            }
        }
    }
    
    func addAllStoreCategoryToList()
    {
        self.tblCategory.reloadData()
        
        if let selectedCat = self.selectedCategory
        {
            if let _ = self.arrayCategoryList
            {
                tableView(self.tblCategory, didSelectRowAtIndexPath: NSIndexPath(forRow: selectedCat, inSection: 0))
                self.tblCategory.selectRowAtIndexPath(NSIndexPath(forRow: selectedCat, inSection: 0), animated: false, scrollPosition: .None)
            }
        }
    }
    
    @IBAction func onFilterClicked(sender: UIButton) {
        
        if self.vwCategory.superview != nil
        {
            self.vwCategory.removeFromSuperview()
        }
        self.vwCategory.frame = self.view.frame
        
//        self.imgBlur.image = self.updateBlur()
//        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Light)
//        let blurView = UIVisualEffectView(effect: darkBlur)
//        blurView.frame = self.imgBlur.bounds
//        self.imgBlur.hidden = false
//        self.imgBlur.addSubview(blurView)
        
        self.view.addSubview(self.vwCategory)
        self.tblCategory.reloadData()
        
        if let selectedCat = self.selectedCategory
        {
            if let _ = self.arrayCategoryList
            {
                tableView(self.tblCategory, didSelectRowAtIndexPath: NSIndexPath(forRow: selectedCat, inSection: 0))
                self.tblCategory.selectRowAtIndexPath(NSIndexPath(forRow: selectedCat, inSection: 0), animated: false, scrollPosition: .None)
            }
        }
    }
    
    func updateBlur() -> UIImage
    {
        // 2
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, true, 1)
        // 3
        self.view.drawViewHierarchyInRect(self.view.bounds, afterScreenUpdates: true)
        // 4
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshot
    }
    
    func rotateLayerInfinite(layer: CALayer)
    {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation");
        rotation.fromValue = 0.0
        rotation.toValue = 2 * M_PI
        rotation.duration = 1.0 // Speed
        rotation.repeatCount = Float.infinity // Repeat forever. Can be a finite number.
        layer.removeAllAnimations()
        layer.addAnimation(rotation, forKey:"Spin")
    }
}


