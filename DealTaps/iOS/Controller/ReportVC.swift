//
//  ReportVC.swift
//  DealTaps
//
//  Created by abc on 3/5/16.
//
//

import UIKit
import SwiftyJSON
import Alamofire

class ReportVC: BaseVC,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet var vwReportReasonList: UIView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtReportDesc: UITextView!
    @IBOutlet weak var lblReportTitle: UILabel!
    
    var arrayReasonList = [String]()
    var reportType: ReportType = ReportType.ReportTypeStore
    var dealType: DealType = DealType.DealTypeScan
    var reportId:String?
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ReportVC.updateReportOnExit(_:)), name: NSNOTIFICATION_ON_MALL_EXIT_FOR_REPORT, object: nil)

        self.btnSend.enabled = false
        self.loadReasonList()
        
    }
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSNOTIFICATION_ON_MALL_EXIT_FOR_REPORT, object: nil);
    }
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated);
        self.vwContainer.hidden = false
        
    }
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Action
    @IBAction func onBackClicked(sender: AnyObject)
    {
        super.onPopSelfClicked(sender as! UIButton)
    }

    @IBAction func onSendClicked(sender: AnyObject)
    {
        
        if self.txtReportDesc.isFirstResponder()
        {
            self.txtReportDesc.resignFirstResponder()
        }
        
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIUserUUID] = self.getUUID()
            param[kAPIReportType] = "\(self.reportType.rawValue)"
            param[kAPIReportTitle] = self.lblReportTitle.text
            
            if let reportId = self.reportId
            {
                param[kAPIReportId] = "\(reportId)"
            }
            if self.reportType == ReportType.ReportTypeDeal
            {
                // deal_type :- 1 - simple deal , 2 - QR code Deal
                param[kAPIReportDealType] = "\(self.dealType.rawValue)"
            }
            
            if let reportDesc = self.txtReportDesc.text
            {
                param[kAPIReportDesc] = reportDesc
            }
            
            self.printAPIURL(APINameInString.APISendReportDetails.rawValue, param: param)

            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APISendReportDetails.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
             //   self.onBackClicked(sender)
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            dispatch_async(dispatch_get_main_queue(), { 
                                
                                if (status == 1)
                                {
                                    
                                    let alert = UIAlertController(title: ALERT_TITLE, message: "Thanks for submitting your report", preferredStyle: UIAlertControllerStyle.Alert)
                                    
                                    let OkAction = UIAlertAction(title: ALERT_OK,style: .Cancel){(action) in
                                        self.onBackClicked(sender)
                                    }
                                    alert.addAction(OkAction)
                                    self.presentViewController(alert, animated: true, completion: nil)
                                    
                                    
                                }
 
                            })
                        }
                        
                        super.DLog("response \(json)")
                    }
                case .Failure(let error):
                    super.DLog(error)
                   // super.DAlert(ALERT_TITLE, message: error.description, action: "Ok", sender: self)
                }
            }
            
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }
    
    @IBAction func onRemoveReportListClicked(sender: UIButton)
    {
        if(self.vwReportReasonList.superview != nil)
        {
            self.vwReportReasonList.removeFromSuperview()
        }
        
        if let selectedIndexPath = self.tblVw.indexPathForSelectedRow
        {
            self.lblReportTitle.text = self.arrayReasonList[selectedIndexPath.row].uppercaseString
            self.lblReportTitle.textColor = COLOR_TEXT_GREEN
            self.btnSend.enabled = true
        }
    }
    @IBAction func onSelectReportSubjectClicked(sender: AnyObject)
    {
        if(self.vwReportReasonList.superview != nil)
        {
            self.vwReportReasonList.removeFromSuperview()
        }
        
        self.vwReportReasonList.frame = self.view.frame
        self.view.addSubview(self.vwReportReasonList)
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrayReasonList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ReportCell", forIndexPath: indexPath) as! ReportCell
        cell.lblReason.text = self.arrayReasonList[indexPath.row]
        cell.imgCircle.image = UIImage(named: "bullet_not_selected")
        cell.lblReason.textColor = COLOR_TEXT_CELL_GRAY
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ReportCell
        cell.imgCircle.image = UIImage(named: "bullet_selected")
        cell.lblReason.textColor = COLOR_TEXT_GREEN
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ReportCell
        cell.imgCircle.image = UIImage(named: "bullet_not_selected")
        cell.lblReason.textColor = COLOR_TEXT_CELL_GRAY
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return self.tblVw.frame.size.height * 0.1388888889
    }
    //MARK: - Load Reason List
    func loadReasonList()
    {
        self.arrayReasonList.append("Store is misrepresenting deals")
        self.arrayReasonList.append("Store’s deal is no longer valid")
        self.arrayReasonList.append("Store posted inappropriate content")
        self.arrayReasonList.append("Can’t locate store in shopping complex")
        self.arrayReasonList.append("QR code is not scanning")
        self.arrayReasonList.append("App is crashing, help!")
        self.arrayReasonList.append("Can’t share deals with friends")
        self.arrayReasonList.append("Problem with the app")
    }
    func updateReportOnExit(notification: NSNotification)
    {
        self.vwContainer.hidden = true
    }
}
