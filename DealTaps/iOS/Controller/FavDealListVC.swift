//
//  MallDealListVC.swift
//  DealTaps
//
//  Created by Nimisha on 27/02/16.
//
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class FavDealListVC: BaseVC,UITableViewDataSource,UITableViewDelegate,DWBubbleMenuViewDelegate {
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var lblMallName: UILabel!
    @IBOutlet weak var tblVw: UITableView!
    var menu:DWBubbleMenuButton!
    
    var dictFavDeal:NSMutableDictionary?
    var arraySelectedDeal = NSMutableArray()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createBottomMenu()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let selectedMenu:UIButton = self.menu.viewWithTag(3) as! UIButton
        self.menu.setSelectedMenu(selectedMenu)
     
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.dictFavDeal = self.getValidFavList()
        self.tblVw.reloadData()
        self.startAllTimer()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let _ = self.dictFavDeal
        {
            if self.dictFavDeal?.allKeys.count > 0
            {
                tableView.scrollEnabled = true
            }
            else
            {
                tableView.scrollEnabled = false
            }
            return (self.dictFavDeal?.allKeys.count)!
        }
        tableView.scrollEnabled = false
        return 0;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("FavDealCell", forIndexPath:indexPath ) as! FavDealCell
        
        if let dealId = self.dictFavDeal?.allKeys[indexPath.row]
        {
            DLog("deal \(self.dictFavDeal?.objectForKey(dealId))")
            
            if let storeDeails = self.dictFavDeal?.objectForKey(dealId)
            {
                if let isScanDeal = storeDeails[kAPIIsScanDeal] as? String
                {
                    if isScanDeal == "true"
                    {
                        cell.isScanDeal = true
                        cell.dealId = storeDeails[kAPIScanDealId] as! String
                    }
                    else
                    {
                        cell.isScanDeal = false
                        if let dealId = storeDeails[kAPIScanDealId] as? String
                        {
                            cell.dealId = dealId
                        }
                        else if let dealId = storeDeails[kAPISimpleDealId] as? String
                        {
                            cell.dealId = dealId
                        }
                    }
                }
               // cell.btnFav.tag = Int(cell.dealId)!
                cell.btnFav.tag = indexPath.row
                cell.tag = cell.btnFav.tag
                cell.lblStoreName.text = storeDeails[kAPIDealBannerTitle] as? String
                
                if let _ = cell.lblStoreName.text
                {
                    cell.lblStoreName.text = cell.lblStoreName.text!.uppercaseString
                }
                cell.imgStoreLogo.clipsToBounds = true
                
                if let closingTime = storeDeails[kAPIEndTime] as? String
                {
                    cell.lblDealClosingTime.text = self.removePaddingZeroFromDate(closingTime)
                }
                
                if let startDate = storeDeails[kAPIDealStartDate] as? String
                {
                    cell.lblDealStartDate.text = self.convertDateFromOneFormatToOtherFormat(startDate, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
                }
                if storeDeails[kAPIDealSaleTypeId]!!.intValue == 3
                {
                    cell.lblDealTitle.text = storeDeails[kAPIDealConstraint] as? String
                }
                else if storeDeails[kAPIDealSaleTypeId]!!.intValue == 1 || storeDeails[kAPIDealSaleTypeId]!!.intValue == 2
                {
                    cell.lblDealTitle.text = "\(storeDeails[kAPIDealConstraint]!!.intValue) \(storeDeails[kAPIDealSaleType] as! String)"
                }
                else if  storeDeails[kAPIDealSaleTypeId]!!.intValue == 4
                {
                    cell.lblDealTitle.text = "\(storeDeails[kAPIDealSaleType] as! String)"
                }
                
                if let endDate = storeDeails[kAPIDealEndDate] as? String
                {
                    cell.lblDealEndDate.text = self.convertDateFromOneFormatToOtherFormat(endDate, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
                }
                
                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                dispatch_async(dispatch_get_global_queue(priority, 0)) {
                    if let displayImage = storeDeails[kAPIStoreLogo] as? String
                    {
                        // optionsInfo: [.Transition(ImageTransition.Fade(1))],
                        dispatch_async(dispatch_get_main_queue()) {
                            
                            cell.imgStoreLogo.kf_showIndicatorWhenLoading = true
                        }
                        let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                        super.DLog("url of display image \(URL.absoluteString)")
                        
                        
                        cell.imgStoreLogo.kf_setImageWithURL(URL, placeholderImage: nil,
                                                             optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                             progressBlock: { receivedSize, totalSize in
                                                                //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                            },
                                                             completionHandler: { image, error, cacheType, imageURL in
                                                                //print("\(indexPath.row + 1): Finished")
                        })
                    }
                }
                
                cell.arrayDealImgs.removeAll()

                //calculate the number of images for deal
                if let dealImg1 = storeDeails[kAPIDealImage1] as? String
                {
                    if (!dealImg1.isEmpty)
                    {
                        cell.arrayDealImgs.append(dealImg1)
                    }
                }
                if let dealImg2 = storeDeails[kAPIDealImage2] as? String
                {
                    if (!dealImg2.isEmpty)
                    {
                        cell.arrayDealImgs.append(dealImg2)
                    }
                }
                if let dealImg3 = storeDeails[kAPIDealImage3] as? String
                {
                    if (!dealImg3.isEmpty)
                    {
                        cell.arrayDealImgs.append(dealImg3)
                    }
                }
                if let dealImg4 = storeDeails[kAPIDealImage4] as? String
                {
                    if (!dealImg4.isEmpty)
                    {
                        cell.arrayDealImgs.append(dealImg4)
                    }
                }
                if let dealImg5 = storeDeails[kAPIDealImage5] as? String
                {
                    if (!dealImg5.isEmpty)
                    {
                        cell.arrayDealImgs.append(dealImg5)
                    }
                }
                
            }
            
            cell.numberOfDealImages = (cell.arrayDealImgs.count < 3) ? 3 : cell.arrayDealImgs.count
            cell.selectionStyle = .None
            cell.carousel.reloadData()
        }
        
        cell.btnFav.addTarget(self, action: #selector(FavDealListVC.onSelectDealForDelete(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        //Check for scan and regular deal
        //DLog("selected cell \(self.arrayDealList![indexPath.row])")
        
        //Stop timer of all visible cell
       // self.stopAllTimer()
        
        if let dealId = self.dictFavDeal?.allKeys[indexPath.row]
        {
            DLog("deal \(self.dictFavDeal?.objectForKey(dealId))")
            
            if let dealDetails = self.dictFavDeal?.objectForKey(dealId) as? NSDictionary
            {
                //Get the selected cell
                let selectedCell = self.tblVw.cellForRowAtIndexPath(indexPath) as! FavDealCell
                let dealDetailVC = STORY_BOARD.instantiateViewControllerWithIdentifier("DealDetailsVC") as! DealDetailsVC
                dealDetailVC.dictDealDetails = dealDetails as! Dictionary<String, AnyObject>
                dealDetailVC.arrayDealImgs = selectedCell.arrayDealImgs
                dealDetailVC.isScanDeal = selectedCell.isScanDeal
                dealDetailVC.selectedDealId = "\(selectedCell.dealId)"
                appDelegate.navigationController?.pushViewController(dealDetailVC, animated: ANIMATION_FOR_PUSH_POP)
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return self.tblVw.frame.height * 0.4938271605
    }
    
    //MARK: - Start and Stop Timer
    func stopAllTimer()
    {
        let visibleCells = self.tblVw.visibleCells
        for cell in visibleCells
        {
            let dealCell = cell as! FavDealCell
            if(dealCell.timerForAutoScroll != nil)
            {
                dealCell.timerForAutoScroll?.invalidate()
                dealCell.timerForAutoScroll = nil
            }
        }
    }
    func startAllTimer()
    {
        let visibleCells = self.tblVw.visibleCells
        for cell in visibleCells
        {
            let dealCell = cell as! FavDealCell
            dealCell.startTimer()
        }
    }
    
    //MARK: - Bottom Navigation Menu
    
    func createBottomMenu()
    {
        // Create up menu button
        let homeLabel2 =  self.createHomeButtonView()
        
        self.menu = DWBubbleMenuButton(frame: CGRectMake(self.view.frame.size.width - homeLabel2.frame.size.width - 10.0,self.view.frame.size.height - homeLabel2.frame.size.height - 10.0,
            homeLabel2.frame.size.width,homeLabel2.frame.size.height),expansionDirection: .DirectionLeft)
        self.menu.homeButtonView = homeLabel2
        self.menu.delegate = self
        self.menu.backgroundColor = UIColor.clearColor()
        
        let selectedMenu:UIButton = self.menu.viewWithTag(3) as! UIButton
        self.menu.setSelectedMenu(selectedMenu)
        self.view.addSubview(self.menu)
    }
    
    func createHomeButtonView() -> UIImageView {
        
        let bgImgVw = UIImageView(frame: CGRectMake(0.0, 0.0, ScreenSize.SCREEN_WIDTH * 0.1835, ScreenSize.SCREEN_WIDTH * 0.1835))
        DLog("frame \(bgImgVw.frame)")
        bgImgVw.image = UIImage(named: "heart_icon.png")
        bgImgVw.clipsToBounds = true;
        
        return bgImgVw
    }
    
    //MARK: Menu delegate method
    func bubbleMenuButtonDidSelect(expandableView: DWBubbleMenuButton, selectedMenuItem: UIButton) {
        
        DLog("sender.tag \(selectedMenuItem.tag)")
        
        if(selectedMenuItem.tag == 0)
        {
            dispatch_async(dispatch_get_main_queue(), {

                self.initQRCodeScanner()
            })

        }
        else if(selectedMenuItem.tag == 1)
        {
            DLog("----2----");
            let mallDealVC = STORY_BOARD.instantiateViewControllerWithIdentifier("MallStoreListVC") as! MallStoreListVC
            super.pushToViewControllerIfNotExistWithClassName(mallDealVC, identifier: "MallStoreListVC", animated: ANIMATION_FOR_PUSH_POP)
        }
        else if(selectedMenuItem.tag == 2)
        {
            DLog("----3----");
            let mallDealVC = STORY_BOARD.instantiateViewControllerWithIdentifier("MallDealListVC") as! MallDealListVC
            super.pushToViewControllerIfNotExistWithClassName(mallDealVC, identifier: "MallDealListVC", animated: ANIMATION_FOR_PUSH_POP)
        }
        else if(selectedMenuItem.tag == 3)
        {
            DLog("----4----");
        }
    }
    
    func bubbleMenuButtonDidCollapse(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonWillCollapse(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonDidExpand(expandableView: DWBubbleMenuButton) {
        
    }
    func bubbleMenuButtonWillExpand(expandableView: DWBubbleMenuButton) {
        
    }
    
    //MARK: - Delete Deal From Favourite
    @IBAction func onFavDeleteClicked(sender: UIButton)
    {
        
        DLog("selected cell \(self.arraySelectedDeal)")

        
        if let selectedDealForDelete  = self.tblVw.indexPathsForSelectedRows
        {


            for indexPath in selectedDealForDelete
            {
                let cellForDeal = self.tblVw.cellForRowAtIndexPath(indexPath) as! FavDealCell
               // self.removeFromFav(cellForDeal.dealId)
                self.logicalRemoveFromFav(cellForDeal.dealId)
            }
            
            self.dictFavDeal = self.getValidFavList()

            self.tblVw.beginUpdates()
            self.tblVw.deleteRowsAtIndexPaths(selectedDealForDelete, withRowAnimation: .Automatic)
            self.tblVw.endUpdates()
            
            self.tblVw.reloadData()

        }
        
        self.arraySelectedDeal.removeAllObjects()
    }
    func onSelectDealForDelete(sender: UIButton)
    {
        DLog("selected cell tag \(sender.tag)")
        
        sender.selected = !sender.selected
        if(sender.selected)
        {
            self.arraySelectedDeal.addObject(sender.tag)
            let rowToSelect:NSIndexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
            let cellForDeal = self.tblVw.cellForRowAtIndexPath(rowToSelect) as! FavDealCell
            cellForDeal.imgFav.image = UIImage(named:"heart_pink_icon.png")
            self.tblVw.selectRowAtIndexPath(rowToSelect, animated: false, scrollPosition:UITableViewScrollPosition.None)
            
        }
        else
        {
            self.arraySelectedDeal.removeObject(sender.tag)
            let rowToSelect:NSIndexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
            let cellForDeal = self.tblVw.cellForRowAtIndexPath(rowToSelect) as! FavDealCell
            cellForDeal.imgFav.image = UIImage(named:"heart_green_icon.png")
            self.tblVw.deselectRowAtIndexPath(rowToSelect, animated: false)
        }
    }
    
    //MARK:- UIScrollViewDelegate
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        
        self.menu.alpha = 0.5
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.menu.alpha = 1.0
    }
}
