//
//  StoreDealListVC.swift
//  DealTaps
//
//  Created by abc on 3/12/16.
//
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class StoreDealListVC: BaseVC,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblMallName: UILabel!
    @IBOutlet weak var tblVw: UITableView!
    var arrayDealList:Array<AnyObject>?
    var arrayQRDealList:Array<AnyObject>?
    var qrCodeId:String = String()
    var tempCnt = 0
    var originalFrame:CGRect?
    var isDescExpand:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        self.loadAllDealInStore()
        
    }
    override func viewDidAppear(animated: Bool) {
        
        
        super.viewDidAppear(animated)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            if let _ = self.arrayQRDealList
            {
                return (self.arrayQRDealList?.count)!
            }
            else
            {
                return 0
            }
        }
        else
        {
            if let _ = self.arrayDealList
            {
                return (self.arrayDealList?.count)!
            }
            else
            {
                return 0
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("ScanDealCell", forIndexPath:indexPath ) as! ScanDealCell
            let dictScanDealDetails = self.arrayQRDealList![indexPath.row] as! Dictionary<String,AnyObject>
            
           if let storeDeails = self.removeNullFromDictionary(dictScanDealDetails as NSDictionary) as? Dictionary<String, AnyObject>
            {
                if let storeName = storeDeails[kAPIDealBannerTitle]! as? String
                {
                    cell.lblStoreName.text = storeName.uppercaseString
                }
                
                if let closingTime = storeDeails[kAPIEndTime]
                {
                    cell.lblScanDealClosingTime.text = self.removePaddingZeroFromDate("\(closingTime)")
                }
                
                if let startDate = storeDeails[kAPIDealStartDate]
                {
                    cell.lblScanDealStartDate.text = self.convertDateFromOneFormatToOtherFormat(startDate as! String, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
                }
                if let endDate = storeDeails[kAPIDealEndDate]
                {
                    cell.lblScanDealEndDate.text = self.convertDateFromOneFormatToOtherFormat(endDate as! String, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
                }
                if let dealDesc = storeDeails[kAPIDealDesc]
                {
                    self.updateFont(cell.lblScanDealDesc)
                    cell.lblScanDealDesc.text = "\(dealDesc)"
                   // cell.lblScanDealDesc.text = "Macy's, Inc. is one of the nation's premier omnichannel retailers, with fiscal 2014 sales of $28.1 billion. As of April 4, 2015, the company operates about 900 stores Macy's, Inc. is one of the nation's premier omnichannel retailers, with fiscal 2014 sales of $28.1 billion. As of April 4, 2015, the company operates about 900 stores hjgh jj"
                }
                if storeDeails[kAPIDealSaleTypeId]?.intValue == 3
                {
                    cell.lblDealConstraint.text = "\(storeDeails[kAPIDealConstraint]!)"
                }
                else if storeDeails[kAPIDealSaleTypeId]?.intValue == 1 || storeDeails[kAPIDealSaleTypeId]?.intValue == 2
                {
                    cell.lblDealConstraint.text = "\((storeDeails[kAPIDealConstraint]!)) \(storeDeails[kAPIDealSaleType]!)"
                }
                else if storeDeails[kAPIDealSaleTypeId]?.intValue == 4
                {
                    cell.lblDealConstraint.text = "\(storeDeails[kAPIDealSaleType]!)"
                }
                
                cell.imgStoreLogo.kf_showIndicatorWhenLoading = true

                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                dispatch_async(dispatch_get_global_queue(priority, 0))
                {

                    if let displayImage = storeDeails[kAPIStoreLogo]
                    {
                        // optionsInfo: [.Transition(ImageTransition.Fade(1))],

                        let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                        //super.DLog("url of display image \(URL.absoluteString)")
                        
                        cell.imgStoreLogo.kf_setImageWithURL(URL, placeholderImage: nil,
                                                             optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                             progressBlock: { receivedSize, totalSize in
                                                                //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                            },
                                                             completionHandler: { image, error, cacheType, imageURL in
                                                                //print("\(indexPath.row + 1): Finished")
                        })
                    }
                }
                
                dispatch_async(dispatch_get_main_queue())
                {
                    cell.selectionStyle = .None
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                }
                
                cell.arrayDealImgs.removeAll()
                //calculate the number of images for deal
                if let dealImg1 = storeDeails[kAPIDealImage1] as? String
                {
                    if !(dealImg1.isEmpty)
                    {
                        cell.arrayDealImgs.append(dealImg1)
                    }
                }
                if let dealImg2 = storeDeails[kAPIDealImage2] as? String
                {
                    if !dealImg2.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg2)
                    }
                }
                if let dealImg3 = storeDeails[kAPIDealImage3]  as? String
                {
                    if !dealImg3.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg3)
                    }
                }
                if let dealImg4 = storeDeails[kAPIDealImage4]  as? String
                {
                    if !dealImg4.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg4)
                    }
                }
                if let dealImg5 = storeDeails[kAPIDealImage5]  as? String
                {
                    if !dealImg5.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg5)
                    }
                }
                
                cell.btnReport.addTarget(self, action: #selector(StoreDealListVC.onScanDealReportClicked(_:)), forControlEvents: .TouchUpInside)
                cell.btnFav.addTarget(self, action: #selector(StoreDealListVC.onScanDealFavClicked(_:)), forControlEvents: .TouchUpInside)

                //self.checkTheNeedOfExpandDes(cell)
                //self.expandDesc(cell)
                //cell.btnExpand.addTarget(self, action: #selector(StoreDealListVC.onExpandClicked(_:)), forControlEvents: .TouchUpInside)
                cell.numberOfDealImages = (cell.arrayDealImgs.count < 3) ? 3 : cell.arrayDealImgs.count
                
               // dispatch_async(dispatch_get_main_queue())
                //{
                cell.carousel.reloadData()
                //}

            }
            
            cell.imgStoreLogo.clipsToBounds = true
//            cell.btnExpand.addTarget(self, action: #selector(StoreDealListVC.onExpandClicked(_:)), forControlEvents: .TouchUpInside)
//
//            self.updateFont(cell.lblScanDealDesc)
//
//            cell.lblScanDealDesc.text = "Macy's, Inc. is one of the nation's premier omnichannel retailers, with fiscal 2014 sales of $28.1 billion. As of April 4, 2015, the company operates about 900 stores Macy's, Inc. is one of the nation's premier omnichannel retailers, with fiscal 2014 sales of $28.1 billion. As of April 4, 2015, the company operates about 900 stores hjgh jj"
//
//            self.checkTheNeedOfExpandDes(cell)
//            self.expandDesc(cell)

            return cell

        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("MallDealCell", forIndexPath:indexPath ) as! MallDealCell
            cell.isScanDealCell = false
            let dictScanDealDetails = self.arrayDealList![indexPath.row] as! Dictionary<String,AnyObject>
            if let storeDeails = self.removeNullFromDictionary(dictScanDealDetails as NSDictionary) as? Dictionary<String, AnyObject>
            {
                if let storeName = storeDeails[kAPIDealBannerTitle]! as? String
                {
                    cell.lblStoreName.text = storeName.uppercaseString
                }
                
                if let closingTime = storeDeails[kAPIEndTime]
                {
                    cell.lblDealClosingTime.text = self.removePaddingZeroFromDate("\(closingTime)")
                }
                
                if let startDate = storeDeails[kAPIDealStartDate]
                {
                    cell.lblDealStartDate.text = self.convertDateFromOneFormatToOtherFormat(startDate as! String, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
                }
                if let endDate = storeDeails[kAPIDealEndDate]
                {
                    cell.lblDealEndDate.text = self.convertDateFromOneFormatToOtherFormat(endDate as! String, oldDateFormat: "yyyy-MM-dd", newDateFormat: "MMM dd")
                }

                if storeDeails[kAPIDealSaleTypeId]?.intValue == 3
                {
                    cell.lblDealTitle.text = "\(storeDeails[kAPIDealConstraint]!)"
                }
                else if storeDeails[kAPIDealSaleTypeId]?.intValue == 1 || storeDeails[kAPIDealSaleTypeId]?.intValue == 2
                {
                    cell.lblDealTitle.text = "\((storeDeails[kAPIDealConstraint]!)) \(storeDeails[kAPIDealSaleType]!)"
                }
                else if storeDeails[kAPIDealSaleTypeId]?.intValue == 4
                {
                    cell.lblDealTitle.text = "\(storeDeails[kAPIDealSaleType]!)"
                }
                
                cell.imgStoreLogo.kf_showIndicatorWhenLoading = true

                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                dispatch_async(dispatch_get_global_queue(priority, 0))
                {
                    if let displayImage = storeDeails[kAPIStoreLogo]
                    {
                        // optionsInfo: [.Transition(ImageTransition.Fade(1))],
        
                        let URL = NSURL(string:"\(STORE_LOGO_BASE_URL)\(displayImage)")!
                        //super.//DLog("url of display image \(URL.absoluteString)")
                        cell.imgStoreLogo.clipsToBounds = true
                        cell.imgStoreLogo.kf_setImageWithURL(URL, placeholderImage: nil,
                                                             optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                                             progressBlock: { receivedSize, totalSize in
                                                                //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                            },
                                                             completionHandler: { image, error, cacheType, imageURL in
                                                                //print("\(indexPath.row + 1): Finished")
                        })
                    }
                }
                
                cell.arrayDealImgs.removeAll()

                //calculate the number of images for deal
                if let dealImg1 = storeDeails[kAPIDealImage1] as? String
                {
                    if !(dealImg1.isEmpty)
                    {
                        cell.arrayDealImgs.append(dealImg1)
                    }
                }
                if let dealImg2 = storeDeails[kAPIDealImage2] as? String
                {
                    if !dealImg2.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg2)
                    }
                }
                if let dealImg3 = storeDeails[kAPIDealImage3]  as? String
                {
                    if !dealImg3.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg3)
                    }
                }
                if let dealImg4 = storeDeails[kAPIDealImage4]  as? String
                {
                    if !dealImg4.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg4)
                    }
                }
                if let dealImg5 = storeDeails[kAPIDealImage5]  as? String
                {
                    if !dealImg5.isEmpty
                    {
                        cell.arrayDealImgs.append(dealImg5)
                    }
                }
                
                cell.numberOfDealImages = (cell.arrayDealImgs.count < 3) ? 3 : cell.arrayDealImgs.count
                
                cell.carousel.reloadData()
            }
            
            dispatch_async(dispatch_get_main_queue())
            {
                cell.selectionStyle = .None
                cell.updateConstraintsIfNeeded()
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
            }
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 1
        {
            //Stop timer of all visible cell
          //  self.stopAllTimer()
            
            //Get the selected cell
            
            if indexPath.section != 0
            {
                let selectedCell = self.tblVw.cellForRowAtIndexPath(indexPath) as! MallDealCell
                
                let dealDetailVC = STORY_BOARD.instantiateViewControllerWithIdentifier("DealDetailsVC") as! DealDetailsVC
                dealDetailVC.dictDealDetails = self.arrayDealList![indexPath.row] as! Dictionary<String, AnyObject>
                dealDetailVC.arrayDealImgs = selectedCell.arrayDealImgs
                dealDetailVC.isScanDeal = false
                dealDetailVC.selectedDealId = "\(dealDetailVC.dictDealDetails[kAPIScanDealId] as! String)"
                appDelegate.navigationController?.pushViewController(dealDetailVC, animated: ANIMATION_FOR_PUSH_POP)
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        ////DLog("section \(indexPath.section)")
        
        if indexPath.section == 0
        {
            
            if self.isDescExpand
            {
                DLog("Cell height \(self.tblVw.frame.height * 1.0559006211)")
                return self.tblVw.frame.height * 0.9953416149
            }
            else
            {
                DLog("Cell height \(self.tblVw.frame.height * 0.9953416149)")
                return self.tblVw.frame.height * 0.9953416149
            }
        }
        else
        {
            return self.tblVw.frame.height * 0.4968944099
        }
    }
   
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
//        if indexPath.section == 0
//        {
//            return self.tblVw.frame.height * 0.9953416149
//        }
//        else
//        {
//            return self.tblVw.frame.height * 0.4968944099
//        }
//    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 2
    }
    
    //MARK: Start and Stop Timer
    func stopAllTimer()
    {
        let visibleCells = self.tblVw.visibleCells
        for cell in visibleCells
        {
            if cell.isKindOfClass(MallDealCell)
            {
                let dealCell = cell as! MallDealCell
                if(dealCell.timerForAutoScroll != nil)
                {
                    dealCell.timerForAutoScroll?.invalidate()
                    dealCell.timerForAutoScroll = nil
                }
            }
            else
            {
                let dealCell = cell as! ScanDealCell
                if(dealCell.timerForAutoScroll != nil)
                {
                    dealCell.timerForAutoScroll?.invalidate()
                    dealCell.timerForAutoScroll = nil
                }
            }
        }
    }
    func startAllTimer()
    {
        let visibleCells = self.tblVw.visibleCells
        for cell in visibleCells
        {
            if cell.isKindOfClass(MallDealCell)
            {
                let dealCell = cell as! MallDealCell
                dealCell.startTimer()
            }
            else
            {
                let dealCell = cell as! ScanDealCell
                dealCell.startTimer()
            }
        }
    }
    
    @IBAction func onHomeClicked(sender: UIButton)
    {
        let mallDealVC = STORY_BOARD.instantiateViewControllerWithIdentifier("MallStoreListVC") as! MallStoreListVC
        super.pushToViewControllerIfNotExistWithClassName(mallDealVC, identifier: "MallStoreListVC", animated: ANIMATION_FOR_PUSH_POP)
    }
    func loadAllDealInStore()
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIQrCOde] = self.qrCodeId
            
            self.printAPIURL(APINameInString.APIGetScanDealListByQrCode.rawValue, param: param)

            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIGetScanDealListByQrCode.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        if let status = json[kAPIStatus].int
                        {
                            super.DLog("response \(json)")

                            dispatch_async(dispatch_get_main_queue())
                            {
                                if (status == 1)
                                {
                                    let allDealArray = json[kAPIData].arrayObject
                                    
                                    if allDealArray?.count > 0
                                    {
                                        self.lblMallName.text = "\(allDealArray![0][kAPIStoreName] as! String)"
                                    }
                                    
                                    //Apply filter and differentiate the scan deal and regular deal
                                    self.arrayQRDealList = allDealArray?.filter({ (deal: AnyObject) -> Bool in
                                        let dealDetails = deal as! Dictionary<String, AnyObject>
                                        return dealDetails[kAPIIsScanDeal]!.boolValue
                                    })
                                    
                                    //Apply filter and differentiate the scan deal and regular deal
                                    self.arrayDealList = allDealArray?.filter({ (deal: AnyObject) -> Bool in
                                        let dealDetails = deal as! Dictionary<String, AnyObject>
                                        return !dealDetails[kAPIIsScanDeal]!.boolValue
                                    })
                                    
                                    //super.//DLog("scan deal \(self.arrayQRDealList)")
                                    //super.//DLog("regular deal \(self.arrayDealList)")
                                    
                                    dispatch_async(dispatch_get_main_queue())
                                    {
                                    self.tblVw.reloadData()
                                    self.checkForFav()
                                    }
                                }
                                else
                                {
                                    self.lblMallName.text = ""
                                    super.DAlert(ALERT_TITLE, message:"\(json[kAPIMessage].stringValue)", action: "OK", sender: self)
                                }
                            }
                           
                        }
                        
                    }
                case .Failure(let error):
                    self.lblMallName.text = ""
                    super.DAlert(ALERT_TITLE, message: error.localizedDescription, action: "OK", sender: self)
                }
            }
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }
    
    @IBAction func onScanOtherQrClicked(sender: UIButton)
    {
        dispatch_async(dispatch_get_main_queue(), {

        self.stopAllTimer()
        self.initQRCodeScanner()
        })
    }
    
    
    func checkTheNeedOfExpandDes(cellScanDeal: ScanDealCell?) -> Bool
    {
       // let cellScanDeal = self.tblVw.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? ScanDealCell
        
        if cellScanDeal != nil
        {
            if self.originalFrame == nil
            {
                self.originalFrame = cellScanDeal?.lblScanDealDesc.frame
                DLog("container original frame \(cellScanDeal?.lblScanDealDesc.frame)")
                
                cellScanDeal?.lblScanDealDesc.numberOfLines = 0
                cellScanDeal?.lblScanDealDesc.sizeToFit()
                
                if(cellScanDeal?.lblScanDealDesc.frame.size.height < self.originalFrame?.height)
                {
                    //DLog("no need to change the height")
                    cellScanDeal?.btnExpand.enabled = false
                    cellScanDeal?.lblScanDealDesc.numberOfLines = 3
                    cellScanDeal?.lblScanDealDesc.frame = self.originalFrame!
                    return false
                }
                else
                {
                    //DLog("need to change the height")
                    cellScanDeal?.lblScanDealDesc.frame = self.originalFrame!
                    cellScanDeal?.btnExpand.enabled = true
                    return true
                }
            }
        }
        
        return false

    }
    
    func expandDesc(cellScanDeal1: ScanDealCell?)
    {
        if let cellScanDeal = cellScanDeal1
        {
            var difference:CGFloat = CGFloat()
            
            if(self.isDescExpand)
            {
                cellScanDeal.lblScanDealDesc.numberOfLines = 0
                cellScanDeal.lblScanDealDesc.translatesAutoresizingMaskIntoConstraints = true
                cellScanDeal.lblScanDealDesc.sizeToFit()
                difference = cellScanDeal.lblScanDealDesc.frame.size.height - self.originalFrame!.height
            }
            else
            {
                difference = -(cellScanDeal.lblScanDealDesc.frame.size.height - self.originalFrame!.height)
                cellScanDeal.lblScanDealDesc.numberOfLines = 3
                cellScanDeal.lblScanDealDesc.translatesAutoresizingMaskIntoConstraints = true
                cellScanDeal.lblScanDealDesc.frame = self.originalFrame!
            }
            
            cellScanDeal.lblScanDealDesc.setNeedsLayout()
            cellScanDeal.lblScanDealDesc.layoutIfNeeded()
            
            DLog("difference \(difference)")
            cellScanDeal.vwStoreDetail.translatesAutoresizingMaskIntoConstraints = true
            cellScanDeal.vwStoreDetail.frame = CGRectMake(cellScanDeal.vwStoreDetail.frame.origin.x, cellScanDeal.vwStoreDetail.frame.origin.y, cellScanDeal.vwStoreDetail.frame.size.width, cellScanDeal.vwStoreDetail.frame.size.height + difference)
            cellScanDeal.vwStoreDetail.setNeedsUpdateConstraints()
            cellScanDeal.vwStoreDetail.updateConstraintsIfNeeded()
            cellScanDeal.vwStoreDetail.layoutIfNeeded()
            
//            cellScanDeal.vwContainer.translatesAutoresizingMaskIntoConstraints = true
//            cellScanDeal.vwContainer.frame = CGRectMake(cellScanDeal.vwContainer.frame.origin.x, cellScanDeal.vwContainer.frame.origin.y, cellScanDeal.vwContainer.frame.size.width, cellScanDeal.vwContainer.frame.size.height + difference)
//            cellScanDeal.vwContainer.setNeedsUpdateConstraints()
//            cellScanDeal.vwContainer.updateConstraintsIfNeeded()
//            cellScanDeal.vwContainer.layoutIfNeeded()
//            cellScanDeal.vwContainer.layoutSubviews()
            
            
//            cellScanDeal.contentView.translatesAutoresizingMaskIntoConstraints = true
//            cellScanDeal.contentView.frame = CGRectMake(cellScanDeal.contentView.frame.origin.x, cellScanDeal.contentView.frame.origin.y, cellScanDeal.contentView.frame.size.width, cellScanDeal.contentView.frame.size.height + difference)
//            cellScanDeal.contentView.setNeedsUpdateConstraints()
//            cellScanDeal.contentView.updateConstraintsIfNeeded()
//            cellScanDeal.contentView.layoutIfNeeded()
//            cellScanDeal.contentView.layoutSubviews()

            
            DLog("cell height \(cellScanDeal.frame.size.height)")
            DLog("content view \(cellScanDeal.contentView.frame)")
            DLog("Container \(cellScanDeal.vwContainer.frame.size.height)")
        }

    }
    func onExpandClicked(sender: UIButton)
    {
        self.isDescExpand = !self.isDescExpand

        self.tblVw.beginUpdates()
        self.tblVw.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.None)
        self.tblVw.endUpdates()
    }
    
    func updateFont(lblDealDesc: UILabel)
    {
        // make same font, but new size
        let fontDescriptor = lblDealDesc.font.fontDescriptor()
        //DLog("scale \(fontDescriptor.pointSize)")
        
        var pointSize = CGFloat()
        if(DeviceType.IS_IPHONE_4_OR_LESS)
        {
            pointSize = 10.0
        }
        else if(DeviceType.IS_IPHONE_5)
        {
            pointSize = 11.0
        }
        else if(DeviceType.IS_IPHONE_6)
        {
            pointSize = 13.0
        }
        else
        {
            pointSize = 14.0
        }
        lblDealDesc.font = UIFont(descriptor: fontDescriptor,
            size: pointSize)
    }

    func onScanDealReportClicked(button: UIButton)
    {
        let reportVC = STORY_BOARD.instantiateViewControllerWithIdentifier("ReportVC") as! ReportVC
        reportVC.reportType = ReportType.ReportTypeDeal
        reportVC.dealType = DealType.DealTypeScan
        reportVC.reportId = "\((self.arrayQRDealList![0][kAPIScanDealId]?!.intValue)!)"
        appDelegate.navigationController?.pushViewController(reportVC, animated: ANIMATION_FOR_PUSH_POP)

    }
    
    func onScanDealFavClicked(sender: UIButton)
    {
        sender.selected = !sender.selected
        
        if(sender.selected)
        {
            //Add to Fav
            self.addToFavDeal(self.arrayQRDealList![0] as! Dictionary<String, AnyObject>, dealId: "\(self.arrayQRDealList![0][kAPIScanDealId] as! String)",dealType:DealType.DealTypeScan)
        }
        else
        {
            //Remove From Fav
           // self.removeFromFav(("\((self.arrayQRDealList![0][kAPIScanDealId]?!.intValue)!)"))
            self.logicalRemoveFromFav(("\((self.arrayQRDealList![0][kAPIScanDealId]?!.intValue)!)"))            
        }
    }
    func checkForFav()
    {
        if let scanDeal = self.arrayQRDealList
        {
            if scanDeal.count > 0
            {
                if let dealDetails = scanDeal[0] as? Dictionary<String,AnyObject>
                {
                    if(self.isDealInFav("\(dealDetails[kAPIScanDealId]!)"))
                    {
                        if let cell = self.tblVw.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
                        {
                            if cell.isKindOfClass(ScanDealCell)
                            {
                                let scanCell = cell as! ScanDealCell
                                scanCell.btnFav.selected = true
                            }
                        }
                    }
                    else 
                    {
                        if let cell = self.tblVw.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
                        {
                            if cell.isKindOfClass(ScanDealCell)
                            {
                                let scanCell = cell as! ScanDealCell
                                scanCell.btnFav.selected = false
                            }
                        }
                    }
                }
            }
        }
    }
}
