//
//  APIConnection.swift
//  EthOS
//
//  Created by Sujal Bandhara on 03/12/2015.
//  Copyright (c) 2015 byPeople Technologies Pvt Limited. All rights reserved.
//

import UIKit
let TIME_OUT_TIME = 60.0  // in seconds

protocol APIConnectionDelegate {
    
    func connectionFailedForAction(action: Int, andWithResponse result: NSDictionary!, method : String)
    
    func connectionDidFinishedForAction(action: Int, andWithResponse result: NSDictionary!, method : String)
    
    func connectionDidFinishedErrorResponceForAction(action: Int, andWithResponse result: NSDictionary!, method : String)

    func connectionDidUpdateAPIProgress(action: Int,bytesWritten: Int64, totalBytesWritten: Int64 ,totalBytesExpectedToWrite: Int64)

}

class APIConnection: NSObject {
    
    var delegate: APIConnectionDelegate! =  nil
    var param : NSDictionary?
    
    func getAccessToken()->NSString
    {
        var accessToken: String = ""

        let dic: NSDictionary = BaseVC.sharedInstance.getUserDefaultDictionaryFromKey(USER_DEFAULT_AUTHENTICATION_DETAIL)
        
        if(dic.isKindOfClass(NSDictionary))
        {
            if dic.valueForKey(kAPIAccessToken) != nil
            {
                accessToken = dic.valueForKey(kAPIAccessToken) as! String
            }
        }
       
        return accessToken
    }

    func CoreHTTPAuthorizationHeaderWithXAuthToken(param : NSDictionary , token : String) -> NSDictionary
    {
       // let username: AnyObject? = param[kAPIUserName]
       // let password: AnyObject? = param[kAPIPassword]
        var headers: NSDictionary?

        if param.count > 0 // && param[kAPIPassword] != nil && param[kAPIUserName] != nil
        {
            //send x-auth token and authorization header
           // let str = "\(username!):\(password!)"
           // let base64Encoded = BaseVC.sharedInstance.encodeStringToBase64(str)
            
             headers = [
                "Content-Type":"application/json, charset=utf-8",
                "Authorization": "Bearer \(self.getAccessToken())"
                /*"x-auth": token,*/
                /*"authorization": "Basic \(base64Encoded)"*/
            ]
        }
        else
        {
            //send x-auth token
             headers = ["x-auth": token]
        }
        
        
            
        return headers!
    }
    func CoreHTTPAuthorizationHeaderWithXAuthToken() -> NSDictionary
    {
        var headers: NSDictionary?
        headers = [
            "Content-Type":"application/json, charset=utf-8",
            "Authorization": "Bearer \(self.getAccessToken())"]
        
        return headers!

    }
    func SocialCoreHTTPAuthorizationHeaderWithXAuthToken(param : NSDictionary , token : String) -> NSDictionary
    {
        let socialType: AnyObject? = param[kAPISocialType]
        let socialId: AnyObject? = param[kAPISocialId]
        let socialToken: AnyObject? = param[kAPISocialToken]

        var headers: NSDictionary?
        
        if param.count > 0 && param[kAPISocialType] != nil && param[kAPISocialId] != nil && param[kAPISocialToken] != nil
        {
            //send x-auth token and authorization header
            let str = "\(socialType!)_\(socialId!):\(socialToken!)"
            let base64Encoded = BaseVC.sharedInstance.encodeStringToBase64(str)
            
            headers = [
                "x-auth": token,
                "authorization": "Basic \(base64Encoded)"
            ]
        }
        else
        {
            //send x-auth token
            headers = ["x-auth": token]
        }
        return headers!
    }

    func addQueryStringToUrl(url : String, param : NSDictionary) -> String
    {
        var queryString : String = url
        
        if param.count > 0
        {
            for (key, value) in param {
                
                BaseVC.sharedInstance.DLog("\(key) = \(value)")

                if queryString.rangeOfString("?") == nil
                {
                    queryString = queryString.stringByAppendingString("?\(key)=\(value)")
                }
                else
                {
                   queryString = queryString.stringByAppendingString("&\(key)=\(value)")

                }
            }
            BaseVC.sharedInstance.DLog("\(queryString)")
            return queryString
            
        }
        return queryString

    }
    func GET(action: Int,withAPIName apiName: String, andMessage message: String, param:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        
        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(param, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: self.addQueryStringToUrl( BASE_URL +  apiName ,param: param))!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "GET"
        request.allHTTPHeaderFields = headers as? [String : String] 
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
          
            let httpResponse = response as? NSHTTPURLResponse

            /*if (error != nil) {
                print(error)
            } else {
                print(httpResponse)
            }*/
            
            dispatch_async(dispatch_get_main_queue()) {
                 self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.GET.rawValue)
            }

        })
        
        dataTask.resume()
        return self
    }
    func GET_WITH_HEADER(action: Int,withAPIName apiName: String, andMessage message: String, param:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        
        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(param, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "GET"
        request.allHTTPHeaderFields = headers as? [String : String]
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            /*if (error != nil) {
            print(error)
            } else {
            print(httpResponse)
            }*/
            
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.GET.rawValue)
            }
            
        })
        
        dataTask.resume()
        return self
    }

    func PUT(action: Int,withAPIName apiName: String, andMessage message: String, param:[String:AnyObject], withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(param, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval:TIME_OUT_TIME)
        request.HTTPMethod = "PUT"
        request.allHTTPHeaderFields = headers as? [String : String] 
       /* request.HTTPBody = self.convertDicToMutableData(param)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")*/
        request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions())
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        BaseVC.sharedInstance.DLog(request.allHTTPHeaderFields!)

        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            let httpResponse = response as? NSHTTPURLResponse

            /*if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? NSHTTPURLResponse
                print(httpResponse)
            }*/
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.PUT.rawValue)
            }
        })
        
        dataTask.resume()
        
        return self
    }
    
    func POST(action: Int, withAPIName apiName: String, withMessage message: String, withParam param: [String:AnyObject], withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "POST"
        request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param , options: NSJSONWritingOptions())
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            let httpResponse = response as? NSHTTPURLResponse

           /* if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? NSHTTPURLResponse
                print(httpResponse)
            }*/
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.POST.rawValue)
            }

        })
        
        dataTask.resume()
        return self
    }

    func GET_WITH_HEADER(action: Int,withAPIName apiName: String, andMessage message: String, param:NSDictionary,paramHeader:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String, ContentType : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        
        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(paramHeader, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "GET"
        request.allHTTPHeaderFields = headers as? [String : String]
        
        if ContentType == CONTENT_TYPE_ENCODED
        {
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            if param.count > 0
            {
                request.HTTPBody = self.convertDicToMutableData(param)
            }
        }
        else
        {
            //json
            if param[kAPIData] != nil
            {
                request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param[kAPIData]! , options: NSJSONWritingOptions())
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
                
            }
            else
            {
                request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param , options: NSJSONWritingOptions())
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
            }
        }
        
        BaseVC.sharedInstance.DLog(request.allHTTPHeaderFields!)
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            /*if (error != nil) {
            print(error)
            } else {
            
            print(httpResponse)
            }*/
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.GET.rawValue)
            }
            
        })
        dataTask.resume()
        return self
    }
    func PATCH_WITH_HEADER(action: Int,withAPIName apiName: String, andMessage message: String, param:NSDictionary,paramHeader:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String, ContentType : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        
        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(paramHeader, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "PATCH"
        request.allHTTPHeaderFields = headers as? [String : String]
        
        if ContentType == CONTENT_TYPE_ENCODED
        {
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            if param.count > 0
            {
                request.HTTPBody = self.convertDicToMutableData(param)
            }
        }
        else
        {
            //json
            if param[kAPIData] != nil
            {
                request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param[kAPIData]! , options: NSJSONWritingOptions())
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
                
            }
            else
            {
                request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param , options: NSJSONWritingOptions())
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
            }
        }
        
        BaseVC.sharedInstance.DLog(request.allHTTPHeaderFields!)
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            /*if (error != nil) {
            print(error)
            } else {
            
            print(httpResponse)
            }*/
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.PATCH.rawValue)
            }
            
        })
        dataTask.resume()
        return self
    }
    func POST_WITH_HEADER(action: Int,withAPIName apiName: String, andMessage message: String, param:NSDictionary,paramHeader:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String, ContentType : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }

        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(paramHeader, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = headers as? [String : String]
        
        if ContentType == CONTENT_TYPE_ENCODED
        {
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            if param.count > 0
            {
                request.HTTPBody = self.convertDicToMutableData(param)
            }
        }                    
        else
        {
            //json
            if param[kAPIData] != nil
            {
                request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param[kAPIData]! , options: NSJSONWritingOptions())
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                

            }
            else
            {
                request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param , options: NSJSONWritingOptions())
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")

            }
        }
  
        BaseVC.sharedInstance.DLog(request.allHTTPHeaderFields!)

        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
           
            let httpResponse = response as? NSHTTPURLResponse

            /*if (error != nil) {
                print(error)
            } else {

                print(httpResponse)
            }*/
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.POST.rawValue)
            }

        })
        dataTask.resume()
        return self
    }
    func POST_WITH_SOCIAL_HEADER(action: Int,withAPIName apiName: String, andMessage message: String, param:NSDictionary,paramHeader:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String, ContentType : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.hideLoader()

            BaseVC.sharedInstance.showLoader()
        }
        
        let headers: NSDictionary  = SocialCoreHTTPAuthorizationHeaderWithXAuthToken(paramHeader, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = headers as? [String : String]
        
        if ContentType == CONTENT_TYPE_ENCODED
        {
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            if param.count > 0
            {
                request.HTTPBody = self.convertDicToMutableData(param)
            }
        }
        else
        {
            //json
            if param[kAPIData] != nil
            {
                request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param[kAPIData]! , options: NSJSONWritingOptions())
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
                
            }
        }
        
        BaseVC.sharedInstance.DLog(request.allHTTPHeaderFields!)
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            /*if (error != nil) {
            print(error)
            } else {
            
            print(httpResponse)
            }*/
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.POST.rawValue)
            }
            
        })
        dataTask.resume()
        return self
    }

    func DELETE(action: Int,withAPIName apiName: String, andMessage message: String, param:NSDictionary,paramHeader:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool , token : String) -> AnyObject
    {
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        
        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(paramHeader, token: token)
        
        let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: TIME_OUT_TIME)
        request.HTTPMethod = "DELETE"
        request.allHTTPHeaderFields = headers as? [String : String]
        BaseVC.sharedInstance.DLog(request.allHTTPHeaderFields!)
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            let httpResponse = response as? NSHTTPURLResponse

           /* if (error != nil) {
                print(error)
            } else {
                print(httpResponse)
            }*/
            dispatch_async(dispatch_get_main_queue()) {
                self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method :Method.DELETE.rawValue )
            }

        })
        
        dataTask.resume()
        return self
    }

    func UPLOAD_PROFILE_PIC(action: Int, withAPIName apiName: String, withMessage message: String, withParam param:NSDictionary, withProgresshudShow isProgresshudShow: CBool,  isShowNoInternetView: CBool, token : String) -> AnyObject
    {
        
        if isProgresshudShow == true
        {
            BaseVC.sharedInstance.showLoader()
        }
        let headers: NSDictionary  = CoreHTTPAuthorizationHeaderWithXAuthToken(param, token: token)
       
        if BaseVC.sharedInstance.isProfilePicExist()
        {
            let image: UIImage = UIImage(contentsOfFile: BaseVC.sharedInstance.getProfilePicPath())!

            let base64String = BaseVC.sharedInstance.convertImageToBase64(image)

            let param = ["ImageData" : "\(base64String)"]

            //let postData = NSMutableData(data: "ImageData=\(base64String)".dataUsingEncoding(NSUTF8StringEncoding)!)
            
            BaseVC.sharedInstance.DLog("ImageData=\(base64String)")
            
            let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL +  apiName)!,
                cachePolicy: .UseProtocolCachePolicy,
                timeoutInterval: TIME_OUT_TIME)
            request.HTTPMethod = "PUT"
            request.allHTTPHeaderFields = headers as? [String : String]
            request.HTTPBody =  try? NSJSONSerialization.dataWithJSONObject(param , options: NSJSONWritingOptions())
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")

            BaseVC.sharedInstance.DLog(request.allHTTPHeaderFields!)

            let session = NSURLSession.sharedSession()
            let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                let httpResponse = response as? NSHTTPURLResponse

               /* if (error != nil) {
                    print(error)
                } else {
                    print(httpResponse)
                }*/
                dispatch_async(dispatch_get_main_queue()) {
                    self.coreResponseHandling(request, response: httpResponse, json: data, error: error, action: action, method : Method.PUT.rawValue)
                }
            })
            
            dataTask.resume()

        }
        
        return self
    }
    //MARK: - Respose Handling -
    func coreResponseHandling(request: NSURLRequest,response: NSHTTPURLResponse?,json: NSData!,error: NSError?,action: Int,method : String)
    {
        //BaseVC.sharedInstance.DLog("Stop loading \(action)")
        BaseVC.sharedInstance.hideLoader()
        
        if(error != nil)
        {
            BaseVC.sharedInstance.DLog("\(error!.localizedDescription) res:\(response?.statusCode)")
            
            if(response == nil)
            {
                if let delegate = self.delegate
                {
                    delegate.connectionFailedForAction(action, andWithResponse: nil, method : method)
                }
            }
            else
            {
                if let delegate = self.delegate
                {
                    delegate.connectionFailedForAction(action, andWithResponse: nil, method : method)
                }
                
            }
        }
        else
        {
            BaseVC.sharedInstance.DLog("req:\(request) \n res:\(response?.statusCode) \n \(error) ")

            //200 - OK
            //201 - Created
            //204 - No Content
            if(response?.statusCode == 200 || response?.statusCode == 201)
            {
                var dic : NSDictionary = NSDictionary()
                var string : String = String()
                
                if (json  != nil)
                {
                    //String
                    do {
                        let jsonResult = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.AllowFragments) as? String
                        // use anyObj here
                        if (jsonResult != nil && jsonResult?.characters.count > 0)
                        {
                            BaseVC.sharedInstance.DLog(jsonResult!)
                            
                            if ( jsonResult!.isKindOfClass(NSString))
                            {
                                string = jsonResult!
                                
                                if (action == APIName.AuthenticationTokens.rawValue) || (action == APIName.socialauthenticationtokens.rawValue)
                                {
                                    let myDict:NSDictionary = [kAPIAuthToken : string]
                                    dic = myDict
                                    
                                }
                                BaseVC.sharedInstance.DLog(string)
                            }
                        }
                        
                    } catch
                    {
                        print(error)
                       BaseVC.sharedInstance.DAlert(ALERT_TITLE_404, message: ALERT_404_FOUND, action: ALERT_OK, sender: (appDelegate.navigationController?.topViewController)!)//\(jsonParseError.localizedDescription)

                    }
                    
                    //NSDictinary
                    do {
                        let jsonResult = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary
                        // use anyObj here
                        if (( jsonResult?.isKindOfClass(NSDictionary)) != nil)
                        {
                            //BaseVC.sharedInstance.DLog(jsonResult)
                            
                            dic = jsonResult!
                        }
                        
                    } catch {
                        print(error)
                       BaseVC.sharedInstance.DAlert(ALERT_TITLE_404, message: ALERT_404_FOUND, action: ALERT_OK, sender: (appDelegate.navigationController?.topViewController)!)//\(jsonParseError.localizedDescription)

                    }
                    
                    //Array
                    do {
                        let jsonResult = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.AllowFragments) as? NSArray
                        // use anyObj here
                        if (( jsonResult?.isKindOfClass(NSArray)) != nil)
                        {
                            BaseVC.sharedInstance.DLog(jsonResult!)
                            
                            let dicMutable : NSMutableDictionary = NSMutableDictionary()
                            dicMutable.setObject(jsonResult!, forKey: kAPIData)
                            dic = dicMutable as NSDictionary
                        }
                        
                        
                    } catch {
                        print(error)
                       BaseVC.sharedInstance.DAlert(ALERT_TITLE_404, message: ALERT_404_FOUND, action: ALERT_OK, sender: (appDelegate.navigationController?.topViewController)!)//\(jsonParseError.localizedDescription)

                    }
                    
                    
                    if let delegate = self.delegate
                    {
                        delegate.connectionDidFinishedForAction(action, andWithResponse: dic,method : method)
                    }
                }
            }
            else if(response?.statusCode == 204)
            {
                if let delegate = self.delegate
                {
                    var dic : NSDictionary = NSDictionary()
                    
                    if action == APIName.GpsPointsOfInterest.rawValue
                    {
                        if (self.param?.isKindOfClass(NSDictionary) != nil)
                        {
                            dic = self.param!
                        }
                    }
                    
                    delegate.connectionDidFinishedForAction(action, andWithResponse: dic,method : method)
                }
            }
            else
            {
                var dic : NSDictionary = NSDictionary()
                
                if (json  != nil)
                {
                    do {
                        let jsonResult = try NSJSONSerialization.JSONObjectWithData(json, options: []) as! [NSDictionary:AnyObject]
                        
                        if jsonResult.count > 0
                        {
                            dic = jsonResult as NSDictionary
                            BaseVC.sharedInstance.DLog(dic)
                            //AccessDenied
                            if(response?.statusCode == 400 && dic[kAPIErrorCode] as? String == "AccessDenied")//GenericError
                            {
                                if appDelegate.navigationController != nil
                                {
                                    //Check for if kAPIAuthToken is available then session is running otherwise session time out.
                                    let dicLoginData: NSMutableDictionary = BaseVC.sharedInstance.getUserDefaultDataFromKey(USER_DEFAULT_LOGIN_USER_DATA).mutableCopy() as! NSMutableDictionary
                                    
                                    if(dicLoginData.isKindOfClass(NSDictionary))
                                    {
                                        if dicLoginData.valueForKey(kAPIAuthToken) != nil
                                        {
                                            let alert = UIAlertView()
                                            alert.title = ALERT_TITLE
                                            alert.message = ALERT_SESSION_TIME_OUT
                                            alert.addButtonWithTitle(ALERT_OK)
                                            alert.show()
                                            

                                            BaseVC.sharedInstance.clearDataHistoryOfLoginUser()
                                            BaseVC.sharedInstance.popToRootViewController()


                                            //BaseVC.sharedInstance.popForcefullyLoginScreenWhenSessionTimeOutWithClassName(WelComeStep1aLoginVC(), identifier:"WelComeStep1aLoginVC" , animated: true, animationType: AnimationType.Default)
                                            
                                         

                                        }
                                    }
                                }
                            }
                            else
                            {
                                if let delegate = self.delegate
                                {
                                    delegate.connectionDidFinishedErrorResponceForAction(action, andWithResponse: dic,method: method )
                                }
                            }
                        }
                        
                        // use anyObj here
                    } catch {
                        print(error)
                        
                       BaseVC.sharedInstance.DAlert(ALERT_TITLE_404, message: ALERT_404_FOUND, action: ALERT_OK, sender: (appDelegate.navigationController?.topViewController)!)//\(jsonParseError.localizedDescription)
                        
                        
                    }
                    
                    
                }
                else
                {
                    //static
                    if let delegate = self.delegate
                    {
                        delegate.connectionDidFinishedForAction(action, andWithResponse: dic,method : method)
                    }
                }
            }
        }
    }
    

    
    //MARK: - Convert Dictinary To Data -
    //        request.HTTPBody = self.convertDicToMutableData(param)
    //request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    func convertDicToMutableData(param:NSDictionary) -> NSMutableData
    {
        var postData : NSMutableData?
        
        if param.count > 0
        {
            for (key, value) in param
            {
               // BaseVC.sharedInstance.DLog("\(key) : \(value)")

                if postData == nil
                {
                    postData = NSMutableData(data: "\(key)=\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                else
                {
                    postData!.appendData("&\(key)=\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
            }
            return postData!
            
        }
        return postData!
    }

}
