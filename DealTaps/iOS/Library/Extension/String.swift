//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

extension String {
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).componentsSeparatedByString(".").last!
    }
    
    func substring(from: Int) -> String {
        return self.substringFromIndex(self.startIndex.advancedBy(from))
    }
    
    var length: Int {
        return self.characters.count
    }
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
    
    func trimSpace() -> String
    {
       return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
    
        let nsSt = self as NSString
        return nsSt.stringByAppendingPathComponent(path)
    }
    func isEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$",
            options: [.CaseInsensitive])
        
        return regex.firstMatchInString(self, options:[],
            range: NSMakeRange(0, utf16.count)) != nil
    }
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
        mutating func deleteCharactersInRange(range: NSRange) {
            let mutableSelf = NSMutableString(string: self)
            mutableSelf.deleteCharactersInRange(range)
            self = mutableSelf as String
        }
    
    subscript (r: Range<Int>) -> String {
        
        /*
        "abcde"[0] === "a"
        "abcde"[0...2] === "abc"
        "abcde"[2..<4] === "cd"
        */
        let start = startIndex.advancedBy(r.startIndex)
        let end = start.advancedBy(r.endIndex - r.startIndex)
       // return self[Range(start: start, end: end)]
        return self[start..<end]
    }
}