//
//  Geotification.swift
//  EthOS
//
//  Created by Sujal Bandhara on 03/12/2015.
//  Copyright (c) 2015 byPeople Technologies Pvt Limited. All rights reserved.
//


import UIKit
import MapKit
import CoreLocation
import SwiftyJSON

let kGeotificationLatitudeKey = "latitude"
let kGeotificationLongitudeKey = "longitude"
let kGeotificationRadiusKey = "RadiusMetres"
let kGeotificationIdentifierKey = "PointOfInterestId"
let kGeotificationNoteKey = "note"
let kGeotificationEventTypeKey = "eventType"
let kGeotificationdistanceBetCurrentAndRegionLatLong = "distanceBetCurrentAndRegionLatLong"
let kGeotificationMallDetails = "mallDetails"

class Geotification: NSObject, NSCoding {
  var coordinate: CLLocationCoordinate2D
  var RadiusMetres: CLLocationDistance
  var PointOfInterestId: String
  var note: String
  var eventType: EventType
  var EventId: String
  var distanceBetCurrentAndRegionLatLong : Double = 0
  var mallDetails : Dictionary<String,AnyObject>
    
  var title: String {
    if note.isEmpty {
      return "No Note"
    }
    return note
  }

  var subtitle: String {
    let eventTypeString = eventType == .OnEntry ? "On Entry" : "On Exit"
    return "Radius: \(RadiusMetres)m - \(eventTypeString)"
  }

    init(coordinate: CLLocationCoordinate2D, RadiusMetres: CLLocationDistance, PointOfInterestId: String, note: String, eventType: EventType,EventId : String ,distanceBetCurrentAndRegionLatLong: Double , mallDetails : Dictionary<String,AnyObject>) {
    self.coordinate = coordinate
    self.RadiusMetres = RadiusMetres
    self.PointOfInterestId = PointOfInterestId
    self.note = note
    self.eventType = eventType
    self.EventId = EventId
    self.distanceBetCurrentAndRegionLatLong = distanceBetCurrentAndRegionLatLong
    self.mallDetails = mallDetails
  }

    func printDetails()
    {/*
        print("---------------------------------")
        print("MallId \(self.EventId)")
        print("MallName \(self.note)")
        print("Location \(self.coordinate.latitude),\(self.coordinate.longitude)")
        print("Radius \(self.RadiusMetres)")
        print("distance \(self.distanceBetCurrentAndRegionLatLong)")
        print("isEnterOrExit \(self.mallDetails)")
        print("---------------------------------")
 */
    }
  // MARK: NSCoding

  required init?(coder decoder: NSCoder) {
    let latitude = decoder.decodeDoubleForKey(kGeotificationLatitudeKey)
    let longitude = decoder.decodeDoubleForKey(kGeotificationLongitudeKey)
    coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    RadiusMetres = decoder.decodeDoubleForKey(kGeotificationRadiusKey)
    distanceBetCurrentAndRegionLatLong = decoder.decodeDoubleForKey(kGeotificationdistanceBetCurrentAndRegionLatLong)
    PointOfInterestId = decoder.decodeObjectForKey(kGeotificationIdentifierKey) as! String
    note = decoder.decodeObjectForKey(kGeotificationNoteKey) as! String
    EventId = decoder.decodeObjectForKey(kAPIEventIdString) as! String
    eventType = EventType(rawValue: decoder.decodeIntegerForKey(kGeotificationEventTypeKey))!
    mallDetails = decoder.decodeObjectForKey(kGeotificationMallDetails) as! Dictionary<String,AnyObject>
    
  }

  func encodeWithCoder(coder: NSCoder) {
    coder.encodeDouble(coordinate.latitude, forKey: kGeotificationLatitudeKey)
    coder.encodeDouble(coordinate.longitude, forKey: kGeotificationLongitudeKey)
    coder.encodeDouble(RadiusMetres, forKey: kGeotificationRadiusKey)
    coder.encodeObject(PointOfInterestId, forKey: kGeotificationIdentifierKey)
    coder.encodeObject(note, forKey: kGeotificationNoteKey)
    coder.encodeObject(EventId, forKey: kAPIEventIdString)
    coder.encodeInt(Int32(eventType.rawValue), forKey: kGeotificationEventTypeKey)
    coder.encodeDouble(distanceBetCurrentAndRegionLatLong, forKey: kGeotificationdistanceBetCurrentAndRegionLatLong)
    coder.encodeObject(mallDetails, forKey: kGeotificationMallDetails)

  }
}
