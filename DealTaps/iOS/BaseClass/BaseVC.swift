//
//  BaseVC.swift
//  EthOS
//
//  Created by Sujal Bandhara on 03/12/2015.
//  Copyright (c) 2015 byPeople Technologies Pvt Limited. All rights reserved.
//


/*
IMPORTANT :
LOCATION UPDATE WORK FLOW:-
Location update should work like below:

Location update means send location to server

Location is updated following case

1. Foreground/Background : 200m

2. Terminate : 500m/1k

3. Location update time Interval = 10 min [and we have also send location update at every 10 minutes if application is in foreground as well as background]

Note : All location are send to server with using "MemberTrackingData" API and also save in Sqlite Database. We have to save location into SQLite because of in no internet connection case we have to send location again which are fail to update in to server.

If we got fail in location update then send it again in next time api call.
--------------------------------------------------------------------------------



*/

import UIKit
import CoreLocation
import IQKeyboardManager
import SwiftyJSON
import AudioToolbox
import AVFoundation
import Alamofire

let ANIMATION_SPEED = 0.3

class BaseVC: UIViewController,AVCaptureMetadataOutputObjectsDelegate {
    
    static let sharedInstance = BaseVC()
    
    var qrCodeFrameView:UIView?
    var vwTmp:UIView?
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var scanResultDict : NSDictionary!

    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]

    private var returnKeyHandler : IQKeyboardReturnKeyHandler!
    
    /*
    *    In Location method, after start/stop location we need to push to viewcontroller
    */
    var strPushToViewController : String = String()
    
    //MARK: - View Life Cycle -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        appDelegate.navigationController?.interactivePopGestureRecognizer!.enabled = false
        
        // For IQKeyboardManager Next Button click event enable
        returnKeyHandler = IQKeyboardReturnKeyHandler(viewController: self)
        returnKeyHandler.lastTextFieldReturnKeyType = UIReturnKeyType.Done
        //returnKeyHandler.toolbarManageBehaviour = IQAutoToolbarManageBehaviour.ByPosition
        
        // Do any additional setup after loading the view.
    }
  
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - DLog -
    
    func DLog(message: AnyObject = "",file: String = #file, line: UInt = #line , function: String = #function)
    {
     //   return
        /*  #if DEBUG : In comment then display log
        #if DEBUG : Not comment then stop log
        */
        // #if DEBUG
        //print("fuction:\(function) line:\(line) file:\(file) \n=================================================================================================\n \(message) ")
        
        
        //self.logToFile("fuction:\(function) line:\(line) file:\(file) time:\(NSDate())\n=================================================================================================\n \(message) ")
        // #endif
    }
    func Log(message: AnyObject = "",file: String = #file, line: UInt = #line , function: String = #function)
    {
       // return
        /*  #if DEBUG : In comment then display log
        #if DEBUG : Not comment then stop log
        */
       // #if DEBUG
           // print("\(message) ")
       // self.logToFile("\(message) ")
       // #endif
    }
    
    func DAlert(title: String, message: String, action: String, sender: UIViewController){
        
        if objc_getClass("UIAlertController") != nil
        {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: action, style: UIAlertActionStyle.Default, handler:nil))
            sender.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertView(title: title, message: message, delegate: sender, cancelButtonTitle:action)
            alert.show()
        }
    }
    //MARK: - Loader Hide/Show  -
    func showLoader()
    {
        //BaseVC.sharedInstance.DLog("Method called")
        let imgListArray :NSMutableArray = []
        
        //use for loop
        for position in 1...3
        {
            
            let strImageName : String = "\(position).png"
            let image  = UIImage(named:strImageName)
            imgListArray .addObject(image!)
        }
        
        let imageView = UIImageView(frame: CGRectMake(0, 0, 75, 25));
        
        imageView.animationImages = NSArray(array: imgListArray) as? [UIImage]
        imageView.animationDuration = 1.0
        imageView.startAnimating()
        
        JTProgressHUD.showWithView(imageView)
    }
    
    func hideLoader()
    {
        JTProgressHUD.hide()
    }
    
    //MARK: - Start Location Manager -
    /*
    Location update should work like below:
    
    Location update means send location to server
    
    Location is updated following case
    
    1. Foreground/Background : 200m
    
    2. Terminate : 500m/1k
    
    3. Location update time Interval = 10 min [and we have also send location update at every 10 minutes if application is in foreground as well as background]
    
    Note : All location are send to server with using "MemberTrackingData" API and also save in Sqlite Database. We have to save location into SQLite because of in no internet connection case we have to send location again which are fail to update in to server.
    
    If we got fail in location update then send it again in next time api call.
    */
    func startLocationManager()
    {
        if appDelegate.locationStatus.isEqualToString(LOCATION_PERMISSION_ALERT_NOT_ASKED)
        {
            self.setUserDefaultStringFromKey(USER_DEFAULT_LOCATION_PERMISSION_ALERT, value: "1")
            
            if (CLLocationManager.locationServicesEnabled())
            {
                if CLLocationManager.authorizationStatus() == .AuthorizedAlways
                {
                    if appDelegate.navigationController != nil
                    {
                       /*  let topViewController: UIViewController = appDelegate.navigationController!.topViewController!
                        
                       if (topViewController as? LoginVC != nil) || (topViewController as? WelComeStep4LocationVC != nil) || (topViewController as? LandingPageVC != nil) || (topViewController as? SocialProfileFormVC != nil)
                        {
                            self.locationManagerSetSuccessfully()
                        }
                        else
                        {
                        }*/
                    }
                }
            }
            
            appDelegate.initLocationManager()
        }
            //Location service on [go to Settings > Privacy > Location Services > On]
        else if (CLLocationManager.locationServicesEnabled())
        {
            if CLLocationManager.authorizationStatus() == .Denied
            {
                //app location service off  [go to Settings > Privacy > EthOS > Alway]
                appDelegate.alertTurnOnLocationFromAppSetting()
            }
            else
            {
                //app location service on
                if appDelegate.locationManager != nil
                {
                   // appDelegate.locationManager.startUpdatingLocation()
                    appDelegate.locationManager.startMonitoringSignificantLocationChanges()
                    
                    //start location update interval [10 min]
                    appDelegate.startUpdatingLocationInterval()
                    
                    //                    //start geo fencings
                    //                    appDelegate.addGeotification()
                    //
                    //                    //get GPS POI list from Server
                    //                    appDelegate.getGpsPointsOfInterestFromAPI()
                    
                }
                
                self.setUserDefaultBOOLObjectFromKey(kAPIAgreeToGpsTracking, object: true)
                
                if appDelegate.navigationController != nil
                {
                 /*   let topViewController: UIViewController = appDelegate.navigationController!.topViewController!
                    
                    if (topViewController as? LoginVC != nil) || (topViewController as? WelComeStep4LocationVC != nil) || (topViewController as? LandingPageVC != nil) || (topViewController as? SocialProfileFormVC != nil)
                    {
                        self.locationManagerSetSuccessfully()
                    }*/
                }
            }
        }
        else
        {
            //Location service off [go to Settings > Privacy > Location Services > On]
            appDelegate.alertTurnOnLocationFromDevicePrivacySetting()
        }
        
    }
    func stopLocationManager()
    {
        //isAgreeGPSTracking == false -> User's GPS Track Flag while signup time(Location Update Don't allow)
        
        if appDelegate.locationStatus.isEqualToString(LOCATION_PERMISSION_ALERT_NOT_ASKED)
        {
            self.setUserDefaultStringFromKey(USER_DEFAULT_LOCATION_PERMISSION_ALERT, value: "1")
            
            appDelegate.initLocationManager()
            
        }
        else
        {
            self.alertDontAllow()
        }
    }
    /*
    *   After
    */
    func locationManagerSetSuccessfully()
    {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            if BaseVC.sharedInstance.strPushToViewController == "WelComeStep5ThanksVC"
            {
               // self.pushToViewControllerIfNotExistWithClassName(WelComeStep5ThanksVC(), identifier: "WelComeStep5ThanksVC", animated: false,animationType : AnimationType.Default)
            }
            else
            {
               // self.pushToViewControllerIfNotExistWithClassName(HomeMainVC(), identifier: "HomeMainVC", animated: true,animationType : AnimationType.Default)
            }
        })
        
        //        if BaseVC.sharedInstance.strPushToViewController == "WelComeStep5ThanksVC"
        //        {
        //            self.pushToViewControllerIfNotExistWithClassName(WelComeStep5ThanksVC(), identifier: "WelComeStep5ThanksVC", animated: false,animationType : AnimationType.Default)
        //        }
        //        else
        //        {
        //            self.pushToViewControllerIfNotExistWithClassName(HomeMainVC(), identifier: "HomeMainVC", animated: true,animationType : AnimationType.Default)
        //        }
        
    }
    
    func alertDontAccess()
    {
        let alertController = UIAlertController(
            title: ALERT_TITLE,
            message: ALERT_INFORM_NOT_GPS_TRACKING,
            preferredStyle: .Alert)
        
        
        let OkAction = UIAlertAction(title: ALERT_OK, style: .Default) { (action) in
            if appDelegate.locationManager != nil
            {
                appDelegate.locationManager.stopUpdatingLocation()
                appDelegate.locationManager.stopMonitoringSignificantLocationChanges()
            }
            
            self.setUserDefaultBOOLObjectFromKey(kAPIAgreeToGpsTracking, object: false)
            self.locationManagerSetSuccessfully()
            
        }
        alertController.addAction(OkAction)
        
        appDelegate.navigationController?.topViewController?.presentViewController(alertController, animated: true, completion: nil)
        
    }
    func alertAccess()
    {
        self.setUserDefaultBOOLObjectFromKey(kAPIAgreeToGpsTracking, object: true)
        
        self.startLocationManager()
        
    }
    
    func alertDontAllow()
    {
        let alertController = UIAlertController(
            title: ALERT_TITLE,
            message: ALERT_REQUIRE_GPS_TRACKING,
            preferredStyle: .Alert)
        
        
        let DontAccessAction = UIAlertAction(title: ALERT_DONT_ACCESS, style: .Default) { (action) in
            self.alertDontAccess()
        }
        alertController.addAction(DontAccessAction)
        
        let AccessAction = UIAlertAction(title: ALERT_ACCESS, style: .Default) { (action) in
            self.alertAccess()
        }
        alertController.addAction(AccessAction)
        
        appDelegate.navigationController?.topViewController?.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func alertUnderImplementation()
    {
        if objc_getClass("UIAlertController") != nil
        {
            let alert = UIAlertController(title: ALERT_TITLE, message: "Function is under implementation", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:nil))
            appDelegate.navigationController?.topViewController?.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertView(title: ALERT_TITLE, message: "Function is under implementation", delegate: nil, cancelButtonTitle:"OK")
            alert.show()
        }
    }
    //MARK: - Start Push Notification -
    
    
    func startPushNotification()
    {
        var value : String = String()
        
        value = BaseVC.sharedInstance.getUserDefaultStringFromKey(USER_DEFAULT_PUSH_NOTIFICATION_PERMISSION_ALERT)
        
        if !value.isEmpty
        {
            self.pushNotificationSetCompletedSuccesfully()
        }
        else
        {
            self.setUserDefaultStringFromKey(USER_DEFAULT_PUSH_NOTIFICATION_PERMISSION_ALERT, value: "1")
            
            let userNotificationType : UIUserNotificationType = [UIUserNotificationType.Alert , UIUserNotificationType.Sound , UIUserNotificationType.Badge]
            let settings = UIUserNotificationSettings(forTypes: userNotificationType, categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
            UIApplication.sharedApplication().registerForRemoteNotifications()
        }
    }
    func getCurrentPhoneNotificationSetting()
    {
        var value : String = String()
        
        value = BaseVC.sharedInstance.getUserDefaultStringFromKey(USER_DEFAULT_PUSH_NOTIFICATION_PERMISSION_ALERT)
        
        if !value.isEmpty
        {
            var isAllowPushNotifiaction : Bool = false
            
            if UIApplication.sharedApplication().isRegisteredForRemoteNotifications() {
                print("registered for remote notifications")
                
                let types = UIApplication.sharedApplication().currentUserNotificationSettings()?.types
                
                if let type = types
                {
                    if type == .None
                    {
                        isAllowPushNotifiaction = false
                    }
                    else
                    {
                        isAllowPushNotifiaction = true
                    }
                }
            }
            
            if isAllowPushNotifiaction
            {
                //Allow Push Notification
                BaseVC.sharedInstance.setUserDefaultBOOLObjectFromKey(kAPIAgreeToPushNotifications, object: true)
            }
            else
            {
                //Don't Allow Push Notification
                BaseVC.sharedInstance.setUserDefaultBOOLObjectFromKey(kAPIAgreeToPushNotifications, object: false)
            }
        }
        else
        {
            BaseVC.sharedInstance.DLog("Not started push notification")
        }
        
    }
    func pushNotificationDontAllow()
    {
        let alertController = UIAlertController(
            title: ALERT_TITLE,
            message: ALERT_INFORM_NOT_PUSH_NOTIFICATION,
            preferredStyle: .Alert)
        
        
        let OkAction = UIAlertAction(title: ALERT_OK, style: .Default) { (action) in
            
            BaseVC.sharedInstance.setUserDefaultBOOLObjectFromKey(kAPIAgreeToPushNotifications, object: false)
            
            self.pushNotificationSetCompletedSuccesfully()
        }
        alertController.addAction(OkAction)
        
        appDelegate.navigationController?.topViewController?.presentViewController(alertController, animated: true, completion: nil)
        
    }
    func pushNotificationAllow()
    {
        self.pushNotificationSetCompletedSuccesfully()
    }
    func pushNotificationSetCompletedSuccesfully()
    {
        var value : String = String()
        
        value = BaseVC.sharedInstance.getUserDefaultStringFromKey(USER_DEFAULT_ACCOUNT_SETUP_FROM_LOGIN)
        
        if !value.isEmpty
        {
            //Going From Login
            if  (self.isExistUserDefaultKey(USER_DEFAULT_LOGIN_USER_DATA))
            {
                let dic: NSDictionary = self.getUserDefaultDataFromKey(USER_DEFAULT_LOGIN_USER_DATA)
                
                if(dic.isKindOfClass(NSDictionary))
                {
                    if (dic.valueForKey(kAPIAgreeToGpsTracking) != nil)
                    {
                        let isAgreeGPSTracking = dic.valueForKey(kAPIAgreeToGpsTracking) as! Bool
                        
                        /*
                        *  Status of GPS Tracker
                        *  isAgreeGPSTracking == true -> User's GPS Track Flag while signup time(Location Update allow)
                        *  isAgreeGPSTracking == false -> User's GPS Track Flag while signup time(Location Update Don't allow)
                        */
                        
                        //isAgreeGPSTracking == true -> User's GPS Track Flag while signup time(Location Update allow)
                        if isAgreeGPSTracking == true
                        {
                            self.startLocationManager()
                        }
                        else
                        {
                            self.stopLocationManager()
                        }
                    }
                }
            }
            
        }
        else
        {
            //Going From Sign UP
            NSNotificationCenter.defaultCenter().postNotificationName(NOTIFICATION_PUSH_UPDATE, object: self)
            
        }
        
        
    }
    func alertAskForSettingsChange() {
        let alertController = UIAlertController(title: "Push Notification is Turned Off", message: "Turn on push notification to send and receive", preferredStyle: .Alert)
        let settingAction = UIAlertAction(title: "Settings", style: .Default) { (action : UIAlertAction) -> Void in
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(settingAction)
        alertController.addAction(cancelAction)
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            appDelegate.navigationController?.topViewController?.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: - isNetworkReachable -
    
    func isNetworkReachable() -> CBool
    {
        return appDelegate.isNetworkReachable
        
        
//        let reachability: Reachability = Reachability.reachabilityForInternetConnection()
//        let networkStatus: Int = reachability.currentReachabilityStatus().rawValue
//        return networkStatus != 0
    }
    
    
    //MARK: - PUSH POP -
    
    func popViewController(animationType :Int)
    {
        if(animationType == AnimationType.Default)
        {
            appDelegate.navigationController!.popViewControllerAnimated(true)
        }
        else
        {
            let object  = appDelegate.navigationController!.viewControllers[appDelegate.navigationController!.viewControllers.count-2]
            
            let fromViewController: UIViewController = appDelegate.navigationController!.topViewController!
            let toViewController: UIViewController = object
            
            let containerView: UIView? = fromViewController.view.superview
            let screenBounds: CGRect = UIScreen.mainScreen().bounds
            
            let finalToFrame: CGRect = screenBounds
            let finalFromFrame: CGRect = CGRectOffset(finalToFrame, screenBounds.size.width*2 , 0)
            
            toViewController.view.frame = CGRectOffset(finalToFrame, -screenBounds.size.width, 0)
            containerView?.addSubview(toViewController.view)
            
            UIView.animateWithDuration(ANIMATION_SPEED, animations: {
                toViewController.view.frame = CGRectOffset(finalToFrame, 0, 0)
                
                fromViewController.view.frame = finalFromFrame
                }, completion: { finished in
                    appDelegate.navigationController!.popViewControllerAnimated(false)
            })
        }
    }
    
    func popToRootViewController()
    {
        appDelegate.navigationController!.popToRootViewControllerAnimated(true)
    }
    func popSelfViewController()
    {
        appDelegate.navigationController!.popViewControllerAnimated(ANIMATION_FOR_PUSH_POP)
    }
    //TO pop a viewController Without Animation
    func popViewControllerWithoutAnimation()
    {
        appDelegate.navigationController!.popViewControllerAnimated(false)
    }
    
    func popForcefullyLoginScreenWhenSessionTimeOutWithClassName(viewController: UIViewController, identifier: String, animated : Bool  ,animationType :Int)//
    {
        
        if(animationType == AnimationType.Default)
        {
            var stack = appDelegate.navigationController!.viewControllers as Array
            
            var isFoundVC : Bool = false
            
            for i in (0...stack.count-1).reverse()
                {
                let className = NSStringFromClass(stack[i].classForCoder)
                BaseVC.sharedInstance.Log("\(className)")
                
                if (className == ("EthOS.")+identifier)
                {
                    //If navigationController not have login View Controller
                    
                    BaseVC.sharedInstance.DLog("Found Login")
                    appDelegate.navigationController?.popToViewController((appDelegate.navigationController?.viewControllers[i])!, animated: animated)
                    isFoundVC = true
                    break;
                }
            }
            
            if !isFoundVC
            {
                //If navigationController not have login View Controller
                BaseVC.sharedInstance.DLog("Not Found Login")
                
                let objLoginVC   = STORY_BOARD.instantiateViewControllerWithIdentifier(identifier)
                
                appDelegate.navigationController?.pushViewController(objLoginVC, animated: false)
                
                let object  = appDelegate.navigationController!.viewControllers[appDelegate.navigationController!.viewControllers.count-2]
                
                let fromViewController: UIViewController =  appDelegate.navigationController!.topViewController!
                let toViewController: UIViewController =  object
                
                let containerView: UIView? = fromViewController.view.superview
                let screenBounds: CGRect = UIScreen.mainScreen().bounds
                
                let finalToFrame: CGRect = screenBounds
                let finalFromFrame: CGRect = CGRectOffset(finalToFrame, screenBounds.size.width*2 , 0)
                
                toViewController.view.frame = CGRectOffset(finalToFrame, -screenBounds.size.width, 0)
                containerView?.addSubview(toViewController.view)
                
                UIView.animateWithDuration(ANIMATION_SPEED, animations: {
                    toViewController.view.frame = CGRectOffset(finalToFrame, 0, 0)
                    
                    fromViewController.view.frame = finalFromFrame
                    }, completion: { finished in
                        
                        //Remove all other viewController except EthOS.WelComeStep1aLoginVC and EthOS.LandingPageVC
                        var arrayCount = -1
                        
                        for _ in 0..<appDelegate.navigationController!.viewControllers.count
                        {
                            arrayCount += 1
                        }
                        
                        for index in (arrayCount...0).reverse()
                        {
                            if appDelegate.navigationController?.viewControllers.count > 0
                            {
                                let className = NSStringFromClass( appDelegate.navigationController!.viewControllers[index].classForCoder)
                                
                                if (className != ("EthOS.WelComeStep1aLoginVC") && className != ("EthOS.LandingPageVC"))
                                {
                                    appDelegate.navigationController?.viewControllers .removeAtIndex(index)
                                }
                            }
                        }
                })
            }
        }
    }
    func popToViewControllerWithClass(Class: AnyObject ,animated : Bool)
    {
        var index : Int = 0
        
        let array = appDelegate.navigationController!.viewControllers as NSArray
        
        for i in 0..<array.count
        {
            let object: UIViewController = array[i] as! UIViewController
            
            if object == Class as! UIViewController
            {
                index = i
                let object  = appDelegate.navigationController!.viewControllers[index]
                appDelegate.navigationController?.popToViewController(object as UIViewController, animated: animated)
                
                break;
            }
        }
    }
    
    func dismissViewAnimated(animated: Bool)
    {
        self.dismissViewControllerAnimated(animated, completion: nil)
    }
    
    func pushViewControllerWithClass(Class : UIViewController, identifier: String , animated : Bool)
    {
        let object: AnyObject! = STORY_BOARD.instantiateViewControllerWithIdentifier(identifier)
        appDelegate.navigationController?.pushViewController(object as! UIViewController, animated: animated)
    }
    
    func pushViewController(vc : UIViewController)
    {
        appDelegate.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushViewControllerWithOutAnimation(vc : UIViewController)
    {
        appDelegate.navigationController?.pushViewController(vc, animated: false)
    }
    func presentViewControllerWithAnimation(identifier: String , animated : Bool)
    {
        let object: AnyObject! = STORY_BOARD.instantiateViewControllerWithIdentifier(identifier)
        
        appDelegate.navigationController?.presentViewController(object  as! UIViewController, animated: true, completion: { () -> Void in
            
        })
    }
    
    
    func popToLandingScreen()
    {
//        let object = STORY_BOARD.instantiateViewControllerWithIdentifier("LandingPageVC")
//        self.navigationController = UINavigationController()
//        self.navigationController!.pushViewController(object, animated: false)
//        self.navigationController?.navigationBar.hidden = true
//        self.window!.rootViewController = self.navigationController
//        self.window!.makeKeyAndVisible()
        
        //print("top view controller \(appDelegate.window?.rootViewController)")
    
        let array = appDelegate.navigationController!.viewControllers as NSArray
        
        //print("navigation stack \(appDelegate.navigationController!.viewControllers)")
        for vc in array
        {
            let className = NSStringFromClass(vc.classForCoder)
            
            if (className == ("EthOS.LandingPageVC"))
            {
                //poptoviewController
                
               // appDelegate.navigationController?.popToViewController(vc as! UIViewController, animated: true)
                appDelegate.navigationController?.popToRootViewControllerAnimated(true)
                
                appDelegate.navigationController = UINavigationController()
                appDelegate.navigationController?.navigationBar.hidden = true
                appDelegate.navigationController!.pushViewController(vc as! UIViewController, animated: false)
                appDelegate.window!.rootViewController = appDelegate.navigationController

                return
               // break
            }
        }
        
        let object = STORY_BOARD.instantiateViewControllerWithIdentifier("LandingPageVC")
        appDelegate.navigationController = UINavigationController()
        appDelegate.navigationController?.navigationBar.hidden = true
        appDelegate.navigationController!.pushViewController(object, animated: true)
        appDelegate.window!.rootViewController = appDelegate.navigationController
    }
    func pushToViewControllerIfNotExistWithClassName(viewController: UIViewController, identifier: String, animated: Bool)
    {
        
        let array = appDelegate.navigationController!.viewControllers as NSArray
        
        for vc in array
        {
            let className = NSStringFromClass(vc.classForCoder)
            
            if (className == ("DealTaps.")+identifier)
            {
                //poptoviewController
                appDelegate.navigationController?.popToViewController(vc as! UIViewController, animated: animated)
                return;
            }
        }
        
        //push the viewcontroller because it does not exist in navigation stack
        let object: AnyObject! = STORY_BOARD.instantiateViewControllerWithIdentifier(identifier)
        appDelegate.navigationController?.pushViewController(object as! UIViewController, animated: animated)
        
    }
    

    
    func switchToViewController(identifier: String)
    {
        let viewController = STORY_BOARD.instantiateViewControllerWithIdentifier(identifier)
        appDelegate.navigationController?.setViewControllers([viewController], animated: false)
    }

    @IBAction func onPopSelfClicked(sende: UIButton)
    {
        self.popSelfViewController()
    }
    //MARK: - Validation Swift -
    
    func validate(val: String?) -> Bool {
        
        var value = val
        value = value?.trimSpace()
        if value?.characters.count > 0
        {
            return false
        }
        return (value?.isEmpty)!
    }
    func validatePassword(value: String?) -> Bool{

        if(value?.characters.count < 8)
        {
            return true
        }

        let capitalLetterRegExp = ".*[A-Z]+.*"
        let textTestCapital = NSPredicate(format:"SELF MATCHES %@",capitalLetterRegExp)
        let capitalResult = textTestCapital.evaluateWithObject(value)
        //print("Capital Result \(capitalResult)")

        let smallLetterRegExp = ".*[a-z]+.*"
        let textTestSmall = NSPredicate(format: "SELF MATCHES %@", smallLetterRegExp)
        let smallResult = textTestSmall.evaluateWithObject(value)
        //print("Small Result \(smallResult)")

        let numberRegExp = ".*[0-9]+.*"
        let numberTestSmall = NSPredicate(format: "SELF MATCHES %@", numberRegExp)
        let numberResult = numberTestSmall.evaluateWithObject(value)
        //print("Small Result \(numberResult)")

        ///^(?=(.*[^A-Za-z0-9]){3,})(?=(.*[A-Z]){3,})(?=(.*\d){3,})(?=.*[a-z]){3,}).+/
        //regular expression for checking at least 3 uppercase, 3 lowercase, 3 digits and 3 special character any where in string.

        
        return !(capitalResult && smallResult && numberResult)

    }
    func validateNumber(value: String?) -> Bool {

        if value?.characters.count > 0
        {
            let num = Int(value!)
            if num != nil
            {
                //Valid Interger
                return true
            }
            else
            {
                //"Not Valid Integer"
                return false
            }
            
        }
        return false
    }
    
    
    func validatePhoneNumber(value: String) -> Bool {
        
        let DEFAULT_LENGTH: Int = 5
        
        if value.characters.count > DEFAULT_LENGTH
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func convertPhoneNumberInDialFormat(pno: String) ->String{
        
        var phoneNumber = pno
        phoneNumber = phoneNumber.stringByReplacingOccurrencesOfString("(", withString: "")
        phoneNumber = phoneNumber.stringByReplacingOccurrencesOfString(")", withString: "")
        phoneNumber = phoneNumber.stringByReplacingOccurrencesOfString(" ", withString: "")
        phoneNumber = phoneNumber.stringByReplacingOccurrencesOfString("-", withString: "")
        phoneNumber = phoneNumber.stringByReplacingOccurrencesOfString("+", withString: "")
        
        return phoneNumber
    }
    
    //MARK: - BASE64 To String & String To Base64 -
    func encodeStringToBase64(str : String) -> String
    {
        // UTF 8 str from original
        // NSData! type returned (optional)
        let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        // Base64 encode UTF 8 string
        // fromRaw(0) is equivalent to objc 'base64EncodedStringWithOptions:0'
        // Notice the unwrapping given the NSData! optional
        // NSString! returned (optional)
        let base64Encoded = utf8str!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        // BaseVC.sharedInstance.DLog("Encoded:  \(base64Encoded)")
        
        /*// Base64 Decode (go back the other way)
        // Notice the unwrapping given the NSString! optional
        // NSData returned
        let data = NSData(base64EncodedString: base64Encoded, options: NSDataBase64DecodingOptions(rawValue: 0))
        
        // Convert back to a string
        let base64Decoded = NSString(data: data!, encoding: NSUTF8StringEncoding)
        BaseVC.sharedInstance.DLog("Decoded:  \(base64Decoded)")*/
        
        
        return base64Encoded;
    }
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    //MARK: - UserDefault Operation -
    func isExistUserDefaultKey(key : String) -> Bool
    {
        if  (NSUserDefaults.standardUserDefaults().valueForKey(key) != nil)
        {
            return true;
        }
        return false;
    }
    
    func removeUserDefaultKey(key : String)
    {
        if  (NSUserDefaults.standardUserDefaults().valueForKey(key) != nil)
        {
            NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func clearUserDefaultAllKey()
    {
        for key in NSUserDefaults.standardUserDefaults().dictionaryRepresentation().keys {
            //NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
            self.removeUserDefaultKey(key)
        }
    }
    
    func getUserDefaultDataFromKey(key : String) -> NSDictionary
    {
        var dic: NSDictionary = NSDictionary()
        
    //    print("allKeys \(NSUserDefaults.standardUserDefaults().dictionaryRepresentation())")
        
        if  (NSUserDefaults.standardUserDefaults().objectForKey(key) != nil)
        {
            // Retrieving the dictionary
            let outData = NSUserDefaults.standardUserDefaults().dataForKey(key)
            dic = NSKeyedUnarchiver.unarchiveObjectWithData(outData!) as! NSDictionary

            return dic
        }
        return dic
    }
    func getUserDefaultDataFromKey1(key : String) -> NSData?
    {
        var dic: NSData? = nil
        
        if  (NSUserDefaults.standardUserDefaults().objectForKey(key) != nil)
        {
            let outData = NSUserDefaults.standardUserDefaults().dataForKey(key)
            dic = NSKeyedUnarchiver.unarchiveObjectWithData(outData!) as? NSData
            
            return dic
        }
        return dic
    }

    func getUserDefaultIntergerFromKey(key : String) -> Int
    {
        var value : Int = 0
        
        if  (NSUserDefaults.standardUserDefaults().valueForKey(key) != nil)
        {
            value = NSUserDefaults.standardUserDefaults().valueForKey(key) as! Int
            return value
        }
        return value
    }
    func getUserDefaultStringFromKey(key : String) -> String
    {
        var value : String = String()
        
        if  (NSUserDefaults.standardUserDefaults().valueForKey(key) != nil)
        {
            value = NSUserDefaults.standardUserDefaults().valueForKey(key) as! String
            return value
        }
        return value
    }
    func getUserDefaultDictionaryFromKey(key : String) -> NSMutableDictionary
    {
        var dic: NSMutableDictionary = NSMutableDictionary()
        
        if  (NSUserDefaults.standardUserDefaults().objectForKey(key) != nil)
        {
            //dic = NSUserDefaults.standardUserDefaults().valueForKey(key)!.mutableCopy() as! NSMutableDictionary

            let outData = NSUserDefaults.standardUserDefaults().objectForKey(key)
            dic = NSKeyedUnarchiver.unarchiveObjectWithData(outData! as! NSData)!.mutableCopy() as! NSMutableDictionary

            return dic
        }
        return dic
    }
    
    func getUserDefaultBoolFromKey(key : String) -> Bool
    {
        let value : Bool = false
        
        if  (NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA) != nil)
        {
            let dic: NSMutableDictionary = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA)!.mutableCopy() as! NSMutableDictionary
            
            if(dic.isKindOfClass(NSDictionary))
            {
                if (dic.valueForKey(key) != nil)
                {
                    return dic.valueForKey(key) as! Bool
                }
            }
        }
        return value
    }
    
    func setUserDefaultDataFromKey(key : String ,dic : NSDictionary)
    {
        if dic.isKindOfClass(NSDictionary)
        {
            let data = NSKeyedArchiver.archivedDataWithRootObject(dic)
            NSUserDefaults.standardUserDefaults().setObject(data, forKey:key)
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func setUserDefaultDataFromKey1(key : String ,dic : NSData)
    {
        let data = NSKeyedArchiver.archivedDataWithRootObject(dic)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey:key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    
    func setUserDefaultIntergerFromKey(key : String ,value : Int)
    {
        NSUserDefaults.standardUserDefaults().setObject(value, forKey:key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func setUserDefaultStringFromKey(key : String ,value : String)
    {
        if value.isKindOfClass(NSString)
        {
            NSUserDefaults.standardUserDefaults().setObject(value, forKey:key)
            NSUserDefaults.standardUserDefaults().synchronize()

        }
    }
    func setUserDefaultDictionaryFromKey(key : String ,value : NSDictionary)
    {
        if value.isKindOfClass(NSDictionary)
        {
           // NSUserDefaults.standardUserDefaults().setObject(value, forKey:key)

            let data = NSKeyedArchiver.archivedDataWithRootObject(value)
            NSUserDefaults.standardUserDefaults().setObject(data, forKey:key)
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func setUserDefaultIntergerObjectFromKey(key : String ,object : Int)
    {
        if  (NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA) != nil)
        {
            let dic: NSMutableDictionary = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA)!.mutableCopy() as! NSMutableDictionary
            
            if(dic.isKindOfClass(NSDictionary))
            {
                if (dic.valueForKey(key) != nil)
                {
                    dic.setObject(object, forKey: key)
                    
                }
                else
                {
                    dic.setObject(object, forKey: key)
                }
            }
            NSUserDefaults.standardUserDefaults().setObject(dic, forKey: USER_DEFAULT_LOGIN_USER_DATA)
            NSUserDefaults.standardUserDefaults().synchronize()
            
            DLog(NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA)!)
        }
        
    }
    func setUserDefaultStringObjectFromKey(key : String ,object : String)
    {
        if  (NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA) != nil)
        {
            let dic: NSMutableDictionary = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA)!.mutableCopy() as! NSMutableDictionary
            
            if(dic.isKindOfClass(NSDictionary))
            {
                if (dic.valueForKey(key) != nil)
                {
                    dic.setObject(object, forKey: key)
                    
                }
                else
                {
                    dic.setObject(object, forKey: key)
                }
            }
            NSUserDefaults.standardUserDefaults().setObject(dic, forKey: USER_DEFAULT_LOGIN_USER_DATA)
            NSUserDefaults.standardUserDefaults().synchronize()
            
            DLog(NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA)!)
        }
    }
    
    func setUserDefaultBOOLObjectFromKey(key : String ,object : Bool)
    {
        if  (NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA) != nil)
        {
            var dic = self.getUserDefaultDataFromKey(USER_DEFAULT_LOGIN_USER_DATA).mutableCopy() as! NSMutableDictionary
            
            if(dic.isKindOfClass(NSDictionary))
            {
                if (dic.valueForKey(key) != nil)
                {
                    dic.setObject(object, forKey: key)
                }
                else
                {
                    dic.setObject(object, forKey: key)
                }
            }
            dic = self.removeNullFromDictionary(dic).mutableCopy() as! NSMutableDictionary
            self.setUserDefaultDataFromKey(USER_DEFAULT_LOGIN_USER_DATA, dic: dic as NSDictionary)
            DLog(NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_LOGIN_USER_DATA)!)
        }
    }
    
    //MARK: - Age & Gender & Month -
    func calculateAge (month: Int , year : Int) -> Int {
        
        let myDOB = NSCalendar.currentCalendar().dateWithEra(1, year: year, month: month, day: 10, hour: 0, minute: 0, second: 0, nanosecond: 0)!
        
        return NSCalendar.currentCalendar().components(.Year, fromDate: myDOB, toDate: NSDate(), options: []).year
    }
    func getGenderString(gender : Int) -> String
    {
        if gender == Gender.Male.rawValue
        {
            return arrayGender[0]
        }
        else if gender == Gender.Female.rawValue
        {
            return arrayGender[1]
        }
        else
        {
            return arrayGender[0]
        }
    }
    func getGenderEnum(gender :  String!) -> Int
    {
        if(gender != nil)
        {
            if gender == arrayGender[0]
            {
                return Gender.Male.rawValue
            }
            else if gender == arrayGender[1]
            {
                return Gender.Female.rawValue
            }
            else
            {
                return Gender.Male.rawValue
            }
        }
        return Gender.Male.rawValue
    }
    func getMonthEnum(month : String?) -> NSString
    {
        if month != nil
        {
            if month == "Jan"
            {
                return "1"
            }
            else if month == "Feb"
            {
                return "2"
            }
            else if month == "Mar"
            {
                return "3"
            }
            else if month == "Apr"
            {
                return "4"
            }
            else if month == "May"
            {
                return "5"
            }
            else if month == "Jun"
            {
                return "6"
            }
            else if month == "Jul"
            {
                return "7"
            }
            else if month == "Aug"
            {
                return "8"
            }
            else if month == "Sep"
            {
                return "9"
            }
            else if month == "Oct"
            {
                return "10"
            }
            else if month == "Nov"
            {
                return "11"
            }
            else if month == "Dec"
            {
                return "12"
            }
        }
        return "1"
    }
    func getMonth(month: Int!) -> NSString
    {
        if month != nil
        {
            if month == 1
            {
                return "Jan"
            }
            else if month == 2
            {
                return "Feb"
            }
            else if month == 3
            {
                return "Mar"
            }
            else if month == 4
            {
                return "Apr"
            }
            else if month == 5
            {
                return "May"
            }
            else if month == 6
            {
                return "Jun"
            }
            else if month == 7
            {
                return "Jul"
            }
            else if month == 8
            {
                return "Aug"
            }
            else if month == 9
            {
                return "Sep"
            }
            else if month == 10
            {
                return "Oct"
            }
            else if month == 11
            {
                return "Nov"
            }
            else if month == 12
            {
                return "Dec"
            }
        }
        return "Jan"
    }
    func getMonthString(month: Int) -> String {
        if month == 1 {
            return "January"
        } else if month == 2 {
            return "February"
        } else if month == 3 {
            return "March"
        } else if month == 4 {
            return "April"
        } else if month == 5 {
            return "May"
        } else if month == 6 {
            return "June"
        } else if month == 7 {
            return "July"
        } else if month == 8 {
            return "August"
        } else if month == 9 {
            return "September"
        } else if month == 10 {
            return "October"
        } else if month == 11 {
            return "November"
        } else if month == 12 {
            return "December"
        }
        return "January"
    }
    //MARK: - Image Operation -
    func imageFromUrl(url : NSURL) -> UIImage? {
        if let imageData = NSData(contentsOfURL: url) {
            return UIImage(data: imageData)!
        }
        return nil
    }
    
    //MARK: - Dictinary Operation -
    func checkDicHaveKey(key: String ,dic : NSDictionary) -> Bool
    {
        if(dic.isKindOfClass(NSDictionary))
        {
            if dic.valueForKey(key) != nil
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
        
    }
    
    //MARK: - Array Operation -
    func getArrayFromDictinary(key: String ,dic : NSDictionary) -> NSArray
    {
        var array : NSArray = NSArray()
        if(dic.isKindOfClass(NSDictionary))
        {
            if dic.valueForKey(key) != nil
            {
                if dic[key]?.count > 0
                {
                    if ((dic[key]?.isKindOfClass(NSArray)) != nil)
                    {
                        array = dic[key] as! NSArray
                        return array
                    }
                }
            }
        }
        return array
    }
    
    //MARK: - String Operation -
    
    func checkStringContainCharacter(character : String , string : String) -> Bool
    {
        if string.containsString(character)
        {
            return true
        }
        return false
        
    }
    
    func getMemberSinceString(string : String?) -> String? {
        
        if let dateStr = string
        {
            let dateFormmator = NSDateFormatter()
            
            dateFormmator.dateFormat = "yyyy-MM-dd"
            
            var dateLocale = NSDate?()
            
            let char = "T"
            
            if dateStr.containsString(char)
            {
                let index = dateStr.rangeOfString(char)?.startIndex
                let newDateStr = dateStr.substringWithRange(dateStr.startIndex.advancedBy(0)..<index!)
                dateLocale = dateFormmator.dateFromString(newDateStr)
            }
            else
            {
                dateLocale = dateFormmator.dateFromString(dateStr)
            }
            
            if let dateFromString = dateLocale
            {
                
                let calendar = NSCalendar.currentCalendar()
                let monthInt = calendar.component(.Month, fromDate: dateFromString)
                let monthStr = getMonthString(monthInt)
                let yearInt = calendar.component(.Year, fromDate: dateFromString)
                
                return "Member since \(monthStr) \(yearInt)"
                
            }
            else
            {
                return ""
            }
        }
        return ""
    }
    
    
    //MARK: - Date operation -
    func getUTCTime() -> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        //        dateFormatter.lenient = false
        //        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
        dateFormatter.stringFromDate(NSDate())
        let temp =  dateFormatter.stringFromDate(NSDate())
        DLog(temp)
        return temp
    }
    func daysBetweenDate(startDate: NSDate, endDate: NSDate) -> Int
    {
        let calendar = NSCalendar.currentCalendar()
        
        let components = calendar.components([.Day], fromDate: startDate, toDate: endDate, options: [])
        
        return components.month
    }
    func NSDatetoLocalTime() -> String {
        let date = NSDate();
        let dateFormatter = NSDateFormatter()
        //To prevent displaying either date or time, set the desired style to NoStyle.
        dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle //Set time style
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle //Set date style
        dateFormatter.timeZone = NSTimeZone()
        let localDate = dateFormatter.stringFromDate(date)
        return localDate
    }
    func convertDateToUTC(date: String ,dateformatter : String) -> String
    {
        //yyyy-MM-ddTHH:mm:ss.fffffff+zzz
        //2015-11-09T21:30:28.4922328+11:00
        let dateString = date
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateformatter// change to your date format
        
        let date = dateFormatter.dateFromString(dateString)
        
        //        let defaultTimeZoneStr = dateFormatter.stringFromDate(date!);
        // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC");
        let utcTimeZoneStr = dateFormatter.stringFromDate(date!);
        
        DLog(utcTimeZoneStr)
        
        return utcTimeZoneStr
    }
    
    func dateDiffrenceBetweenTwoDates(startDate: String, endDate: String, dateformatter : String) -> Int
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateformatter
        
        let startDate:NSDate = dateFormatter.dateFromString(startDate)!
        let endDate:NSDate = dateFormatter.dateFromString(endDate)!
        
        let cal = NSCalendar.currentCalendar()
        let unit = NSCalendarUnit.Second
        let components = cal.components(unit, fromDate: startDate, toDate: endDate, options: [])
        
        DLog(components.second)
        return components.second
    }
    //To Convert string to date
    func convertDateToString(date : NSDate ,dateformat : String) -> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateformat // superset of OP's format
        let str = dateFormatter.stringFromDate(NSDate())
        return str
    }
    func convertStringToDate(dateInStr: String, dateformat: String) -> NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateformat
        let date = dateFormatter.dateFromString(dateInStr)
        return date!
    }
    //    //To Convert date to string
    //    - (NSString*) convertDateToString:(NSDate*)date  dateformat:(NSString*)dateformat
    //    {
    //
    //    // Convert date object to desired output format
    //    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    //    [dateFormat1 setDateFormat:dateformat];
    //    NSString *dateStr = [dateFormat1 stringFromDate:date];
    //
    //    return dateStr;
    //    }
    
    func convertDateFromOneFormatToOtherFormat(dateToConvert: String, oldDateFormat: String, newDateFormat: String) -> String    {
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = oldDateFormat
        
        let date:NSDate = dateFormatter1.dateFromString(dateToConvert)!
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = newDateFormat // superset of OP's format
        var str = dateFormatter.stringFromDate(date)
        str = str.uppercaseString
        return str
    }

    func convertTimeInSecondToMinute(totalSeconds : Int) -> Int
    {
        let minutes = (totalSeconds / 60) % 60;
        return minutes
    }
    func convertTimeInSecondToSecond(totalSeconds : Int) -> Int
    {
        let second = totalSeconds % 60;
        return second
    }
    
    //MARK: - Document Dir Profile Pic -
    func saveProfilePic(image : UIImage) -> Bool
    {
        if image.isKindOfClass(UIImage)
        {
            let imageData: NSData = UIImagePNGRepresentation(image)!
            
            //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
            let result = imageData.writeToFile(getProfilePicPath(), atomically: true)
            
            return result
            
        }
        return false
    }
    
    func getDocumentDirPath() -> String
    {
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        return path
    }
    func getProfilePicPath() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        
        let path = paths.stringByAppendingPathComponent("ProfilePic.png")
        
        return path
        
    }
    func getContactProfilePicPath(contactName : String) -> String
    {
        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        
        let dataPath = paths.stringByAppendingPathComponent("ContactProfilePicture")
        
        if !NSFileManager.defaultManager().fileExistsAtPath(dataPath)
        {
            do {
                try NSFileManager.defaultManager().createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                NSLog("\(error.localizedDescription)")
            }
            
        }
        
        let path = dataPath.stringByAppendingPathComponent("\(contactName)")
        
        return path
        
    }
    
    func imageInCircle(imgVw: UIImageView, borderColor: UIColor, borderWidth: CGFloat)
    {
        imgVw.layer.borderWidth = borderWidth
        imgVw.layer.masksToBounds = true
        imgVw.layer.cornerRadius = imgVw.frame.size.width / 2
        imgVw.layer.borderColor = borderColor.CGColor
        imgVw.clipsToBounds = true
    }
    
    func saveContactProfilePic(image : UIImage , name : String) -> Bool
    {
        if image.isKindOfClass(UIImage)
        {
            let imageData: NSData = UIImagePNGRepresentation(image)!
            
            //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
            let result = imageData.writeToFile(getContactProfilePicPath(name), atomically: true)
            
            return result
            
        }
        return false
    }
    
    
    func isProfilePicExist() -> Bool
    {
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(getProfilePicPath()))
        {
            return true
        }
        return false
    }
    func deleteProfilePic()
    {
        let fileManager = NSFileManager.defaultManager()
        
        let path = getProfilePicPath()
        if (fileManager.fileExistsAtPath(path))
        {
            do {
                try fileManager.removeItemAtPath(path)
            } catch _ {
            }
        }
        
    }
    func deleteContactProfilePic()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        
        let dataPath = paths.stringByAppendingPathComponent("ContactProfilePicture")
        
        if NSFileManager.defaultManager().fileExistsAtPath(dataPath)
        {
            do {
                try NSFileManager.defaultManager().removeItemAtPath(dataPath)
            } catch _ {
            }
            
        }
        
    }
    func deleteFileAtPath(path: String)
    {
        if NSFileManager.defaultManager().fileExistsAtPath(path)
        {
            do
            {
                try NSFileManager.defaultManager().removeItemAtPath(path)
            }
            catch
            {
                //print("error to delete file \(error)")
            }
        }
    }
    
    //MARK: - Image TO Base64 <-> Base64 TO Image -
    
    // convert images into base64 and keep them into string
    
    func convertImageToBase64(image: UIImage) -> String {
        
        let imageData = UIImageJPEGRepresentation(image,1.0)
        //        let imageData = UIImagePNGRepresentation(image)
        
        if imageData != nil
        {
            let base64String = imageData!.base64EncodedStringWithOptions([.Encoding64CharacterLineLength, .EncodingEndLineWithCarriageReturn])
            
            return base64String
            
        }
        return ""
        
    }// end convertImageToBase64
    
    
    // convert images into base64 and keep them into string
    
    func convertBase64ToImage(base64String: String) -> UIImage {
        
        //        let decodedData = NSData(base64EncodedString: base64String, options:NSDataBase64DecodingOptions(rawValue: 0))
        //        let decodedData = NSData(base64EncodedString: base64String, options: NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        var decodedimage = UIImage(named: "placeHolder")
        
        if let decodedData = NSData(base64EncodedString: base64String, options: NSDataBase64DecodingOptions(rawValue: 0)) {
            decodedimage = UIImage(data: decodedData)
            
        } else {
            //print("Not Base64")
        }
        if decodedimage == nil
        {
            decodedimage = UIImage(named: "placeHolder")
        }
        return decodedimage!
        
    }// end convertBase64ToImage
    
    func clearDataHistoryOfLoginUser()
    {
        self.deleteProfilePic()
        
        self.clearUserDefaultAllKey()
        
        //=====================Stop Location Update Interval time=====================
        appDelegate.stopUpdateLocation()
        
        //=====================Stop Location Update 200m =====================
        if appDelegate.locationManager != nil
        {
            appDelegate.locationManager.stopUpdatingLocation()
            appDelegate.locationManager.stopMonitoringSignificantLocationChanges()
        }
    }
    
    func logToFile(msg: String)
    {
        
        return
        let logFile = "DealTapsLog.txt"
        let filePath = "\(self.getDocumentDirPath())/\(logFile)"
        
        if !NSFileManager.defaultManager().fileExistsAtPath(filePath)
        {
            //Create the file
            do
            {
                try "Hello".writeToFile(filePath, atomically: true, encoding: NSUTF8StringEncoding)
            }
            catch let error {
                //print("error: to create file  \n \(error)")
            }
        }
        
        do {
            var contents = try String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding)
            contents = "\(contents)\n\(msg)"
            try contents.writeToFile(filePath, atomically: true, encoding: NSUTF8StringEncoding)

        }
        catch let error {
            //print("error: \n \(error)")
        }
    }
    /*
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - Empty String -
    //    /**
    //    Determine whether Optional collection is nil or an empty collection
    //    :param: collection Optional collection
    //    :returns: true if collection is nil or if it is an empty collection, false otherwise
    //    */
    //    public func isNilOrEmpty<C: CollectionType>(collection: C?) -> Bool {
    //        switch collection {
    //        case .Some(let nonNilCollection): return countElements(nonNilCollection) == 0
    //        default:                          return true
    //        }
    //    }
    
    /**
    Determine whether Optional NSString is nil or an empty string
    :param: string Optional NSString
    :returns: true if string is nil or if it is an empty string, false otherwise
    */
    internal func isNilOrEmpty(string: NSString?) -> Bool {
        switch string {
        case .Some(let nonNilString): return nonNilString.length == 0
        default:                      return true
        }
    }
    
    func logAllFonts()
    {
        // Find All Available of font and Log its Family Name and Font Name
        for family: String in UIFont.familyNames()
        {
            //print("\(family)")
            for names: String in UIFont.fontNamesForFamilyName(family)
            {
                //print("== \(names)")
            }
        }
    }
    
    //MARK: - Remove Null from Responce Dictionary
    
    //TO remove NULL from data
    func removeNullFromData(data: AnyObject) -> NSDictionary
    {
        var dict:NSMutableDictionary = NSMutableDictionary()
        
        if (data.isKindOfClass(NSArray))
        {
            dict["responceData"] = data;
            dict = self.removeNullFromDictionary(NSDictionary(dictionary: dict)).mutableCopy() as! NSMutableDictionary
        }
        else if (data.isKindOfClass(NSDictionary))
        {
            dict = self.removeNullFromDictionary(NSDictionary(dictionary: data as! NSDictionary)).mutableCopy() as! NSMutableDictionary
        }
        return NSDictionary(dictionary: dict)
    }
    
    func removeNullFromDictionary(dict: NSDictionary) -> NSDictionary
    {
        let dictUpdated = dict.mutableCopy() as! NSMutableDictionary
        
        for (key,value) in dictUpdated
        {
            if(value.isKindOfClass(NSNull))
            {
                    dictUpdated.setValue("", forKey: key as! String)
            }
            else if(value.isKindOfClass(NSDictionary))
            {
                dictUpdated.setObject(self.removeNullFromDictionary(value as! NSDictionary), forKey: key as! String)
            }
            else if(value.isKindOfClass(NSArray))
            {
                dictUpdated.setObject(self.removeNullFromArray(value as! NSArray), forKey: key as! String)
            }
        }
        
        return NSDictionary(dictionary: dictUpdated)
    }
    
    func removeNullFromArray(arr: NSArray) -> NSArray
    {
        let arrayUpdate = arr.mutableCopy() as! NSMutableArray
        
        for index in 0...arrayUpdate.count-1
        {
            if (arrayUpdate[index].isKindOfClass(NSNull))
            {
                arrayUpdate[index] = "";
            }
            else if(arrayUpdate[index].isKindOfClass(NSDictionary))
            {
                arrayUpdate[index] = self.removeNullFromDictionary(arrayUpdate[index] as! NSDictionary)
            }
            else if(arrayUpdate[index].isKindOfClass(NSArray))
            {
                arrayUpdate[index] = self.removeNullFromArray(arrayUpdate[index] as! NSArray)
            }
        }
        return NSArray(array: arrayUpdate);
    }
    
    var CurrentTimestamp: String {
        
        //print("\(NSDate().timeIntervalSince1970 * 1000)")
        
        return "\(NSDate().timeIntervalSince1970 * 1000)"
    }

    func getUUID() -> String
    {
        let uuid = NSUUID().UUIDString
        return uuid
    }
    
    
    func clearTmpDirectory()
    {
        do
        {
            let tmpDirectory:NSArray = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(NSTemporaryDirectory())
            for file in tmpDirectory
            {
                DLog("\(NSTemporaryDirectory())\(file)")
                try NSFileManager.defaultManager().removeItemAtPath("\(NSTemporaryDirectory())\(file)")
            }
        }
        catch
        {
            DLog("error to clear tmp \(error)")
        }
    }
    func removePaddingZeroFromDate(fdate: String) -> String
    {
        //fromDateFormat "02:30 AM"
        //toDateFormat "2:30AM"
        var fromDateFormat = fdate
        if(fromDateFormat.length == 8)
        {
            fromDateFormat = fromDateFormat.stringByReplacingOccurrencesOfString(" ", withString: "")  //"02:30AM"
            if fromDateFormat[0] == "0"
            {
                fromDateFormat.removeAtIndex(fromDateFormat.startIndex)  //"2:30AM"
                
                // if char at index 2 and 3 both are 0 then remove it
                if (fromDateFormat[2] == "0" && fromDateFormat[3] == "0")
                {
                    let stringRange:NSRange = NSMakeRange(1, 3)
                    fromDateFormat.deleteCharactersInRange(stringRange)
                }
            }
            else //11:00PM
            {
                // if char at index 3 and 4 both are 0 then remove it
                if (fromDateFormat[3] == "0" && fromDateFormat[4] == "0")
                {
                    let stringRange:NSRange = NSMakeRange(2, 3)
                    fromDateFormat.deleteCharactersInRange(stringRange)
                }
            }
        }
        
        return fromDateFormat
    }
    //MARK: Fav Deal 
    func addToFavDeal(let newFavDeal:Dictionary<String,AnyObject>, dealId: String, dealType: DealType)
    {
        var favDealList = NSMutableDictionary()
        var oldDealRecord = Dictionary<String,AnyObject>?()
        if let _ = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_FAV_DEALS)
        {
            favDealList = self.getFavListIncludeDeleteDeal(true)!
            
            if(favDealList[dealId] != nil)
            {
                oldDealRecord = favDealList[dealId] as? Dictionary<String, AnyObject>
            }
        }
        
        if let _ = oldDealRecord
        {
            oldDealRecord![kAPIDealLogicalDeletedFromFav] = "0"
            favDealList[dealId] = oldDealRecord
        }
        else
        {
            self.onFavClicked(dealId, dealType: dealType)
            favDealList[dealId] = newFavDeal
 
        }
//        newFavDeal =  self.removeNullFromDictionary(newFavDeal as NSDictionary) as! Dictionary<String, AnyObject>
//        
//        newFavDeal[kAPIDealLogicalDeletedFromFav] = "0"
//        //if user make the deal first time favourite call the API favouriteDeal
//        if let _ = newFavDeal[kAPIIsAlreadyInFavDeal]
//        {
//            
//        }
//        else
//        {
//            newFavDeal[kAPIIsAlreadyInFavDeal] = "1"
//            //call favDealAPI
//            self.onFavClicked(dealId, dealType: dealType)
//        }
//        
//        favDealList[dealId] = newFavDeal
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(favDealList)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey:USER_DEFAULT_FAV_DEALS)
        NSUserDefaults.standardUserDefaults().synchronize()
        
      //  self.getFavList()
        
    }
    
    func removeFromFav(dealId: String)
    {
        if let dictDealList = self.getFavListIncludeDeleteDeal(false)
        {
            let allKeys:NSArray = dictDealList.allKeys
            if(allKeys.containsObject(dealId))
            {
                dictDealList.removeObjectForKey(dealId)
                let data = NSKeyedArchiver.archivedDataWithRootObject(dictDealList)
                NSUserDefaults.standardUserDefaults().setObject(data, forKey:USER_DEFAULT_FAV_DEALS)
                NSUserDefaults.standardUserDefaults().synchronize()
            }
        }
    }
    
    func logicalRemoveFromFav(dealId: String)
    {
        if let favDealList = self.getFavListIncludeDeleteDeal(true)
        {
//            let allKeys:NSArray = dictDealList.allKeys
//            if(allKeys.containsObject(dealId))
//            {
//                dictDealList.removeObjectForKey(dealId)
//                let data = NSKeyedArchiver.archivedDataWithRootObject(dictDealList)
//                NSUserDefaults.standardUserDefaults().setObject(data, forKey:USER_DEFAULT_FAV_DEALS)
//                NSUserDefaults.standardUserDefaults().synchronize()
//            }
            
            //print("\(favDealList[dealId] as! Dictionary<String,AnyObject>)")
            //print("\(favDealList[dealId])")

            var newFavDeal = favDealList[dealId] as! Dictionary<String,AnyObject>
            
            //print("\(newFavDeal)")
            newFavDeal =  self.removeNullFromDictionary(newFavDeal as NSDictionary) as! Dictionary<String, AnyObject>
            newFavDeal[kAPIDealLogicalDeletedFromFav] = "1"
            
            favDealList[dealId] = newFavDeal
            
            let data = NSKeyedArchiver.archivedDataWithRootObject(favDealList)
            NSUserDefaults.standardUserDefaults().setObject(data, forKey:USER_DEFAULT_FAV_DEALS)
            NSUserDefaults.standardUserDefaults().synchronize()
            
          //  self.getFavList()
    
            
        }
        
    }
    
    func getValidFavList() -> NSMutableDictionary?
    {
        self.checkForExpireDeal()
        
        //Apply filter to remove the logical delete deal
        
        if let outData = NSUserDefaults.standardUserDefaults().dataForKey(USER_DEFAULT_FAV_DEALS)
        {
            let dictFavList = NSKeyedUnarchiver.unarchiveObjectWithData(outData)!.mutableCopy() as! NSMutableDictionary
            
            DLog("fav deal list \(dictFavList.allKeys)")
            
            let arrayFavDealList = dictFavList.allValues.filter({ (deal: AnyObject) -> Bool in
                let dealDetails = deal as! Dictionary<String, AnyObject>
                
                if let _ = dealDetails[kAPIDealLogicalDeletedFromFav]
                {
                    return (dealDetails[kAPIDealLogicalDeletedFromFav] as! String == "1")
                }
                return false
            })
            
            
            if arrayFavDealList.count > 0
            {
                
                for dealDetail in arrayFavDealList
                {
                    if let dealId = dealDetail[kAPIScanDealId] as? String
                    {
                        dictFavList.removeObjectForKey("\(dealId)")
                    }
                    else if let dealId = dealDetail[kAPISimpleDealId] as? String
                    {
                        dictFavList.removeObjectForKey("\(dealId)")
                    }
                }
            }
            DLog("fav deal list \(dictFavList)")
            
            return dictFavList
        }
        return nil
    }

    
    func getFavListIncludeDeleteDeal(includeDealList: Bool) -> NSMutableDictionary?
    {
        self.checkForExpireDeal()
        
        //Apply filter to remove the logical delete deal
        
        if let outData = NSUserDefaults.standardUserDefaults().dataForKey(USER_DEFAULT_FAV_DEALS)
        {
            let dictFavList = NSKeyedUnarchiver.unarchiveObjectWithData(outData)!.mutableCopy() as! NSMutableDictionary
            
            DLog("fav deal list \(dictFavList.allKeys)")

            if (!includeDealList)
            {
                let arrayFavDealList = dictFavList.allValues.filter({ (deal: AnyObject) -> Bool in
                    let dealDetails = deal as! Dictionary<String, AnyObject>
                    
                    if let _ = dealDetails[kAPIDealLogicalDeletedFromFav]
                    {
                        return (dealDetails[kAPIDealLogicalDeletedFromFav] as! String == "0")
                    }
                    return false
                })
                
                
                if arrayFavDealList.count > 0
                {
                    let favDealList = NSMutableDictionary()
                    
                    for dealDetail in arrayFavDealList
                    {
                        if let _ = dealDetail[kAPIScanDealId]
                        {
                            favDealList["\(dealDetail[kAPIScanDealId] as! String)"] = dealDetail
                        }
                        else if let _ = dealDetail[kAPISimpleDealId]
                        {
                            favDealList["\(dealDetail[kAPISimpleDealId] as! String)"] = dealDetail
                        }
                    }
                    
                    DLog("fav deal list \(favDealList)")
                    
                    return favDealList
                }
                else
                {
                    return dictFavList
                }
            }
            
            return dictFavList
        }
        
        return nil
        
    }
    
    func checkForExpireDeal()
    {
        //If currentDate > dealEndDate remove it from the favDealList
        let arrayOfExpDeal = NSMutableArray()
        
        if let outData = NSUserDefaults.standardUserDefaults().dataForKey(USER_DEFAULT_FAV_DEALS)
        {
            let dictFavList = NSKeyedUnarchiver.unarchiveObjectWithData(outData)!.mutableCopy() as? NSMutableDictionary
            
            if let dictDealList = dictFavList
            {
                for dealId in dictDealList.allKeys
                {
                    let dealDetails = dictDealList[dealId as! String]
                    
                    let dealEndDateInStr = dealDetails![kAPIDealEndDate] as! String
                    let dealEndDate = self.convertStringToDate(dealEndDateInStr, dateformat: "yyyy-MM-dd")
                    
                    let currentDateInStr = self.convertDateToString(NSDate(), dateformat: "yyyy-MM-dd")
                    let currentDate = self.convertStringToDate(currentDateInStr, dateformat: "yyyy-MM-dd")

                    
                    if (currentDate.isGreaterThanDate(dealEndDate))
                    {
                        //Deal is expire
                        arrayOfExpDeal.addObject(dealId)
                        DLog("deal is expire \(dealId)")
                    }
                }
                DLog("all expireDealList \(arrayOfExpDeal)")
            }
        }
        
        //If number of expire deal is greater than 0
        if arrayOfExpDeal.count > 0
        {
            if let outData = NSUserDefaults.standardUserDefaults().dataForKey(USER_DEFAULT_FAV_DEALS)
            {
                if let dictFavList = NSKeyedUnarchiver.unarchiveObjectWithData(outData)!.mutableCopy() as? NSMutableDictionary
                {
                    //Remove the all the expire deal
                    for dealId in arrayOfExpDeal
                    {
                        dictFavList.removeObjectForKey(dealId)
                    }
                    
                    //Update the fav deal list
                    let data = NSKeyedArchiver.archivedDataWithRootObject(dictFavList)
                    NSUserDefaults.standardUserDefaults().setObject(data, forKey:USER_DEFAULT_FAV_DEALS)
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            }
        }
    }
    func isDealInFav(dealId : String) -> Bool
    {
        if let dictDealList = self.getFavListIncludeDeleteDeal(true)
        {
            let allKeys:NSArray = dictDealList.allKeys
            if(allKeys.containsObject(dealId))
            {
                if let dealDetails = dictDealList[dealId] as? Dictionary<String, AnyObject>
                {
                    if let dealStatus = dealDetails[kAPIDealLogicalDeletedFromFav]
                    {
                        if (dealStatus as! String) == "1"
                        {
                            return false
                        }
                        else
                        {
                            return true
                        }
                    }
                    
                }
                return true
            }
            return false
        }
        
        return false
    }
    
    //MARK: - Play Sound on Scan
    func playSystemSound(fileName:String = "")
    {
        //http://iphonedevwiki.net/index.php/AudioServices
        if (fileName.length == 0)
        {
            AudioServicesPlaySystemSound (SystemSoundID(1313))//1313	sms-received5.caf	SMSReceived_Selection
            //AudioServicesPlaySystemSound (SystemSoundID(1016))//1016	tweet_sent.caf	tweet_sent.caf	SMSSent
        }
        else
        {
            // Load playSystemSound("beep-23.mp3")
            // Load playSystemSound("beep-24.mp3")
            if let soundURL = NSBundle.mainBundle().URLForResource(fileName, withExtension: "mp3") {
                var mySound: SystemSoundID = 0
                AudioServicesCreateSystemSoundID(soundURL, &mySound)
                // Play
                AudioServicesPlaySystemSound(mySound);
                // AudioServicesDisposeSystemSoundID(mySound)
            }
        }
    }
    
    func vibratePhone()
    {
        AudioServicesPlayAlertSound(UInt32(kSystemSoundID_Vibrate))
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    //MARK: - QR Code Scan
    //MARK: - Native QRCode Reader
    func initQRCodeScanner()
    {
        if AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) ==  AVAuthorizationStatus.Authorized
        {
            // Already Authorized
            self.startScanning()
        }
        else
        {
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    // User granted
                    self.startScanning()
                }
                else
                {
                    // User Rejected
                    //print("Not granted")
                    let alertController = UIAlertController(
                        title: "Oops!",
                        message: ALERT_CAMERA_ACCESS_IS_NOT_ALLOWED,
                        preferredStyle: .Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:nil))
                    appDelegate.navigationController?.topViewController?.presentViewController(alertController, animated: true, completion: nil)
                    
                }
            });
        }
    }
    func startScanning()
    {
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            self.captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            self.captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            self.captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            self.videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            self.videoPreviewLayer?.frame = self.view.layer.bounds
            self.view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
            self.vwTmp = UIView()
            self.vwTmp!.frame = self.view.bounds
           // vwTemp.userInteractionEnabled = false
            self.vwTmp!.tag = 2641
            self.view.addSubview(self.vwTmp!)
            
            let btnBack = UIButton()
            btnBack.setImage(UIImage(named: "back_icon"), forState: .Normal)
            btnBack.setTitle("Cancel", forState: .Normal)
            btnBack.tag = 2640
            btnBack.frame = CGRectMake(0, 20, 100, 50)
            btnBack.addTarget(self, action: #selector(BaseVC.stopScanning), forControlEvents: .TouchUpInside)
            self.view.addSubview(btnBack)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            qrCodeFrameView!.frame = CGRectMake(0, 0, 250,250)
            qrCodeFrameView!.center = view.center
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.greenColor().CGColor
                qrCodeFrameView.layer.borderWidth = 2
                self.view.addSubview(qrCodeFrameView)
                
                self.view.bringSubviewToFront(self.vwTmp!)
                self.view.bringSubviewToFront(qrCodeFrameView)
                self.view.bringSubviewToFront(btnBack)
            }
        }
        catch
        {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }

    }
    
    //MARK: - Response of Native QRCode Reader
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        self.scanResultDict = nil;
        // messageLabel.text = "----"
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            
            qrCodeFrameView?.frame = CGRectZero
            //  messageLabel.text = "Last Scan Result :" + "No barcode/QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {
            //        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
            if (barCodeObject != nil)
            {
                qrCodeFrameView?.frame = barCodeObject!.bounds
                NSLog("QR metadataObj \(metadataObj)")
                if metadataObj.stringValue != nil {
                    let message : String = metadataObj.stringValue
                    // messageLabel.text = "Last Scan Result :"+message
                    
                    self.scanResultDict = self.parseScanResult(message) as NSDictionary
                    Log("scanDict = \(self.scanResultDict["ticketId"])")
                    
                    //present the alert view with scanned results
                    // var title : String = "Vuuka"
                    // let alertView = UIAlertView(title: title, message: message, delegate: self, cancelButtonTitle: "OK")
                    // alertView.show()
                    self.stopScanning()
                    
                    if let qrCodeId = self.scanResultDict["ticketId"]
                    {
                        self.playSystemSound("beep-23")
                        self.loadScanResultScreen("\(qrCodeId)")
                        
                    }
                    else
                    {
                        self.playSystemSound("beep-24")
                        self.vibratePhone()
                        self.DAlert(ALERT_TITLE, message: "Sorry! Not Getting QR code Id.", action: "OK", sender: self)
                    }
                }
            }
            else
            {
                let title : String = ALERT_TITLE
                let alertView = UIAlertView(title: title, message: "Invalid Scan Object", delegate: self, cancelButtonTitle: "OK")
                alertView.show()
            }
        }
    }
    
    func loadScanResultScreen(qrCodeId: String)
    {
            let storeDealList = STORY_BOARD.instantiateViewControllerWithIdentifier("StoreDealListVC") as! StoreDealListVC
            storeDealList.qrCodeId = qrCodeId
            appDelegate.navigationController?.pushViewController(storeDealList, animated: ANIMATION_FOR_PUSH_POP)
    }
    func parseScanResult(scanOutput:String = "") -> NSMutableDictionary
    {
        Log("scanOutput = \(scanOutput)")
        /*
        scanOutput = @
        
        ANSI 6360050101DL00300201DLDAQ102245737
        DAASAMPLE,DRIVER,CREDENTIAL,
        DAG1500 PARK ST
        DAICOLUMBIA
        DAJSC
        DAK292012731
        DARD
        DAS
        DAT
        DAU600
        DAW200
        DAY
        DAZ
        DBA20190928
        DBB19780928
        DBC1
        DBD20091026
        DBG2
        DBH1
        */
        
        let scanDict : NSMutableDictionary = ["scanning_timestamp":NSDate().timeIntervalSince1970]
        
        // /*
        //     scanDict["CustomerFirstName"] = "DRIVER"
        //     scanDict["CustomerMiddleName"] = "CREDENTIAL"
        //     scanDict["CustomerFamilyName"] = "SAMPLE"
        //     scanDict["“DateOfBirth"] = "19780928"
        //     scanDict["AddressCity"] = "COLUMBIA"
        //     scanDict["Address_JurisdictionCode"] = "1500 PARK ST"
        //     scanDict["AddressPostalCode"] = "292012731"
        //     scanDict["Sex"] = "1"
        //     scanDict["TimeStemp"] = "897523148"
        //     scanDict["PostalCodeScanAt"] = "12"
        
        scanDict["CustomerIdNumber"] = separateData(scanOutput, mykey: "DAQ")
        
        let result_name = separateData(scanOutput, mykey: "DAA")
        if (result_name.length > 0)
        {
            scanDict["CustomerFullName"] = result_name
            let nameArray = result_name.componentsSeparatedByString(",") as NSArray
            if (nameArray.count > 0)
            {
                scanDict["CustomerFirstName"] = nameArray[0]
            }
            else
            {
                scanDict["CustomerFirstName"] = ""
            }
            
            if (nameArray.count > 1)
            {
                scanDict["CustomerMiddleName"] = nameArray[1]
            }
            else
            {
                scanDict["CustomerMiddleName"] = ""
            }
            
            if (nameArray.count > 2)
            {
                scanDict["CustomerFamilyName"] = nameArray[2]
            }
            else
            {
                scanDict["CustomerFamilyName"] = ""
            }
        }
        else
        {
            scanDict["CustomerFirstName"] = separateData(scanOutput, mykey: "DAC")
            scanDict["CustomerFamilyName"] = separateData(scanOutput, mykey: "DCS")
            scanDict["CustomerFullName"] = "\(scanDict["CustomerFirstName"] as! String) \(scanDict["CustomerFamilyName"] as! String)"
        }
        scanDict["Address_Jurisdiction_code"] = separateData(scanOutput, mykey: "DAG")
        scanDict["AddressCity"] = separateData(scanOutput, mykey: "DAI")
        scanDict["state"] = separateData(scanOutput, mykey: "DAJ")
        scanDict["AddressPostalCode"] = separateData(scanOutput, mykey: "DAK")
        scanDict["class"] = separateData(scanOutput, mykey: "DAR")
        scanDict["height"] = separateData(scanOutput, mykey: "DAU")
        scanDict["weight"] = separateData(scanOutput, mykey: "DAW")
        scanDict["Sex"] = separateData(scanOutput, mykey: "DBC")
        scanDict["scanning_DateOfBirth"] = separateData(scanOutput, mykey: "DBB")
        scanDict["issued"] = separateData(scanOutput, mykey: "DBD")
        scanDict["DocumentExpirationDate"] = separateData(scanOutput, mykey: "DBA")
        //scanDict["DocumentExpirationDate"] = "19780928"
        
        let expireDate = separateData(scanOutput, mykey: "DBA")
        if(expireDate.length == 0)
        {
            scanDict["isIdScan"] = "0"
            scanDict["ticketId"] = scanOutput
        }
        else
        {
            scanDict["isIdScan"] = "1"
        }
        
        DLog("scanDict = \(scanDict)")
        return scanDict
    }
    
    func separateData(scanOutput:String , mykey:String) -> String
    {
        //print("mykey = \(mykey)")
        //print("scanOutput = \(scanOutput)")
        
        var result = ""
        
        if (mykey.characters.count < 2)
        {
            return result
        }
        else if (scanOutput.isEmpty)
        {
            return result
        }
        else
        {
            let asArray = scanOutput.componentsSeparatedByString(mykey) as NSArray
            
            if (asArray.count > 1)
            {
                result =  asArray[1].componentsSeparatedByString("\n")[0]
            }
            
            result = result.stringByReplacingOccurrencesOfString(" ", withString: "")
            return result
        }
    }
    func stopScanning()
    {
        if let btnBack = self.view.viewWithTag(2640)
        {
                btnBack.removeFromSuperview()
        }
        
        if let vwBg = self.view.viewWithTag(2641)
        {
            vwBg.removeFromSuperview()
        }

        
        // manageButtonsVisibility(true)
        if((qrCodeFrameView?.superview) != nil)
        {
            qrCodeFrameView?.removeFromSuperview()
        }
        videoPreviewLayer?.removeFromSuperlayer()
        captureSession?.stopRunning()
        
//        let selectedMenu:UIButton = self.menu.viewWithTag(1) as! UIButton
//        self.menu.setSelectedMenu(selectedMenu)
    }

    func onDealClicked(dealId: String, dealType: DealType)
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIReportDealType] = "\(dealType.rawValue)"
            param[kAPIDealId] = dealId
            
            self.printAPIURL(APINameInString.APIClickedDeal.rawValue, param: param)

            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIClickedDeal.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
//                        if let status = json[kAPIStatus].int
//                        {
//                            if (status == 1)
//                            {
//                                
//                            }
//                        }
                        
                        self.DLog("response \(json)")
                    }
                case .Failure(let error):
                    self.DLog(error)
                    // super.DAlert(ALERT_TITLE, message: error.description, action: "Ok", sender: self)
                }
            }
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            //DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }
    
    func onShareClicked(dealId: String, dealType: DealType)
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIReportDealType] = "\(dealType.rawValue)"
            param[kAPIDealId] = dealId
            
            self.printAPIURL(APINameInString.APIShareDeal.rawValue, param: param)

            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIShareDeal.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        //                        if let status = json[kAPIStatus].int
                        //                        {
                        //                            if (status == 1)
                        //                            {
                        //
                        //                            }
                        //                        }
                        
                       // self.DLog("response \(json)")
                    }
                case .Failure(let error):
                    self.DLog(error)
                    // super.DAlert(ALERT_TITLE, message: error.description, action: "Ok", sender: self)
                }
            }
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            //DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }

    func onFavClicked(dealId: String, dealType: DealType)
    {
        if(appDelegate.isNetworkReachable)
        {
            var param = Dictionary<String,String>()
            
            param[kAPIReportDealType] = "\(dealType.rawValue)"
            param[kAPIDealId] = dealId
            
            self.printAPIURL(APINameInString.APIFavouriteDeal.rawValue, param: param)

            Alamofire.request(.POST, "\(BASE_URL)\(APINameInString.APIFavouriteDeal.rawValue)", parameters: param, encoding: .URL, headers: nil).validate().responseJSON { (response) -> Void in
                
                
                switch response.result
                {
                case .Success:
                    if let value = response.result.value{
                        let json = JSON(value)
                        //                        if let status = json[kAPIStatus].int
                        //                        {
                        //                            if (status == 1)
                        //                            {
                        //
                        //                            }
                        //                        }
                        
                        self.DLog("response \(json)")
                    }
                case .Failure(let error):
                    self.DLog(error)
                    // super.DAlert(ALERT_TITLE, message: error.description, action: "Ok", sender: self)
                }
            }
        }
        else
        {
            Log(ALERT_NO_INTERNET_SEARCH_AGAIN)
            //DAlert(ALERT_TITLE, message:ALERT_NO_INTERNET_SEARCH_AGAIN, action: "OK", sender: self)
        }
    }
    func printAPIURL(apiName: String, param: Dictionary<String,String>)
    {
        var urlStr = "\(BASE_URL)\(apiName)&"
        
        var paramList = ""
        for (key,value) in param
        {
            paramList = "\(paramList)\(key)=\(value)&"
        }
        
        if !paramList.isEmpty
        {
            //Remove last & from URL
            paramList.deleteCharactersInRange(NSMakeRange(paramList.length-1, 1))
        }
        
        urlStr = "\(urlStr)\(paramList)"
        
        DLog("\(urlStr)")
    }
}

/*
23.054598
72.519688*/

/*
AFnetworking
let manager = AFHTTPRequestOperationManager()
manager.GET(
"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.983545825782222,72.60036490407838&radius=5000&sensor=true&key=AIzaSyAGutmDRwFVPpgdmDf7SdJE0Ekcmje8YJo&name=bypeople",
parameters: nil,
success: { (operation: AFHTTPRequestOperation!,
responseObject: AnyObject!) in
//BaseVC.sharedInstance.DLog("JSON: " + responseObject.description)
},
failure: { (operation: AFHTTPRequestOperation!,
error: NSError!) in
BaseVC.sharedInstance.DLog("Error: " + error.localizedDescription)
}
)
*/
/*
Background stuff
//        var bgTask = UIBackgroundTaskIdentifier()
//        bgTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler { () -> Void in
//            UIApplication.sharedApplication().endBackgroundTask(bgTask)
//        }
//        if (bgTask != UIBackgroundTaskInvalid)
//        {
//            UIApplication.sharedApplication().endBackgroundTask(bgTask);
//            bgTask = UIBackgroundTaskInvalid;
//        }



*/

// Register for Push Notitications, if running iOS 8


/*let locale = NSLocale.currentLocale()
let countryCode = locale.objectForKey(NSLocaleCountryCode) as! String
let phoneUtil = NBPhoneNumberUtil.sharedInstance()

do {
let number:NBPhoneNumber = try phoneUtil.parse("8758-966605", defaultRegion: "IN")
BaseVC.sharedInstance.DLog(number)
print(phoneUtil.isValidNumber(number))

} catch let error as NSError {
NSLog("\(error.localizedDescription)")
}*/

/* let headers = ["x-auth": "908dcf1d-cf1d-4cf3-ae0b-63ca0f6c8864"]

let postData = NSMutableData(data: "[0][Latitude]=\(-33.8634)".dataUsingEncoding(NSUTF8StringEncoding)!)
postData.appendData("&[0][Longitude]=\(151.211)".dataUsingEncoding(NSUTF8StringEncoding)!)
postData.appendData("&[0][DateCollected]=2015-07-06T23:59:09.8524911+00:00".dataUsingEncoding(NSUTF8StringEncoding)!)

let request = NSMutableURLRequest(URL: NSURL(string: "http://api.saywhatnow.fail/api/GpsTrackingData")!,
cachePolicy: .UseProtocolCachePolicy,
timeoutInterval: 10.0)
request.HTTPMethod = "POST"
request.allHTTPHeaderFields = headers
request.HTTPBody = postData

let session = NSURLSession.sharedSession()
let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
if (error != nil) {
print(error)
} else {
let httpResponse = response as? NSHTTPURLResponse
print(httpResponse)

}
})

dataTask.resume()*/

/* let documentsDirectoryURL =  NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first as! NSURL
print("Finished downloading!")
println(documentsDirectoryURL)
var err:NSError?

// Here you can move your downloaded file
if NSFileManager().moveItemAtURL(location, toURL: documentsDirectoryURL.URLByAppendingPathComponent(downloadTask.response!.suggestedFilename!), error: &err)
{
print("File saved")
}
else
{
if let err = err
{
print("File not saved.\n\(err.description)")

}
}*/

        