//
//  ReportCell.swift
//  DealTaps
//
//  Created by abc on 3/5/16.
//
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var imgCircle: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
