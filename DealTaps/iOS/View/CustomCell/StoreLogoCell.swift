//
//  StoreLogoCell.swift
//  Dealtaps
//
//  Created by Nimisha on 11/02/16.
//  Copyright © 2016 Nimisha. All rights reserved.
//

import UIKit

class StoreLogoCell: UITableViewCell {

    @IBOutlet weak var lblClosingTime: UILabel!
    @IBOutlet weak var lblShopNumber: UILabel!
    @IBOutlet weak var lblFloorNumber: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var imgMainPage: UIImageView!
    @IBOutlet weak var imgStoreLogo: UIImageView!
}
