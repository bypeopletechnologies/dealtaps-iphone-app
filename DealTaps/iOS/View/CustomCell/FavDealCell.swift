//
//  MallDealCell.swift
//  DealTaps
//
//  Created by Nimisha on 27/02/16.
//
//

import UIKit
import Kingfisher


class FavDealCell: UITableViewCell,iCarouselDataSource, iCarouselDelegate {

    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var lblDealTitle: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var imgStoreLogo: UIImageView!
    @IBOutlet weak var lblDealEndDate: UILabel!
    @IBOutlet weak var lblDealStartDate: UILabel!
    @IBOutlet weak var lblDealClosingTime: UILabel!
    @IBOutlet weak var carousel: iCarousel!
    var numberOfDealImages:Int = 0
    var timerForAutoScroll:NSTimer?
    var arrayDealImgs = [String]()
    var dealId:String!
    var isScanDeal:Bool = false

    @IBAction func onFavClicked(sender: UIButton)
    {
        sender.selected = !sender.selected
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.carousel.clipsToBounds = true
        self.carousel.type = .Rotary
        self.carousel.layer.borderColor = COLOR_LINE_GRAY.CGColor
        self.carousel.layer.borderWidth = 1.0
        self.carousel.delegate = self
        self.carousel.dataSource = self
        self.carousel.scrollEnabled = false

        self.startTimer()

        // Initialization code
    }

    func startTimer()
    {
        self.timerForAutoScroll = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(FavDealCell.autoScrollToNextImage), userInfo: nil, repeats: true)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: - Carousel Delegate and DataSource
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        return self.numberOfDealImages
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        var itemView: UIImageView
        //create new view if no view is available for recycling
        if (view == nil)
        {
            //don't do anything specific to the index within
            //this `if (view == nil) {...}` statement because the view will be
            //recycled and used with other index values later
            
            
            let width = carousel.frame.size.width * 170 / 398;
            itemView = UIImageView(frame:CGRect(x:0, y:0, width:width, height:carousel.frame.size.height))
            //print("2height \(carousel.frame.size.height)")
            itemView.backgroundColor = UIColor.whiteColor()
            //   itemView.image = UIImage(named: "page.png")
            itemView.contentMode = .ScaleAspectFill
            //  itemView.clipsToBounds = true
            /* if(index == 0)
            {
            itemView.backgroundColor = UIColor.lightGrayColor()
            }
            else if(index == 1)
            {
            itemView.backgroundColor = UIColor.greenColor()
            }
            else if(index == 2)
            {
            itemView.backgroundColor = UIColor.purpleColor()
            }
            else if(index == 3)
            {
            itemView.backgroundColor = UIColor.blueColor()
            }
            else if(index == 4)
            {
            itemView.backgroundColor = UIColor.yellowColor()
            }*/
            
            itemView.layer.shadowColor = UIColor.grayColor().CGColor
            itemView.layer.shadowOffset = CGSizeMake(0.0,0.0)
            itemView.layer.shadowOpacity = 1.0
            itemView.layer.shadowRadius = 6.0
            
            let shadowRect:CGRect = CGRectInset(itemView.bounds, 0, 4)  // inset top/bottom
            itemView.layer.shadowPath = UIBezierPath(rect: shadowRect).CGPath
            
        }
        else
        {
            //get a reference to the label in the recycled view
            itemView = view as! UIImageView;
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        
        dispatch_async(dispatch_get_main_queue()) {

        itemView.kf_showIndicatorWhenLoading = true
        }
        
        var URL:NSURL
        if(self.isScanDeal)
        {
            URL = NSURL(string:"\(SCAN_DEAL_IMAGE_BASE_URL)\(self.arrayDealImgs[index])")!
        }
        else
        {
            URL = NSURL(string:"\(DEAL_IMAGE_BASE_URL)\(self.arrayDealImgs[index])")!
        }
        
        //print("image URL \(URL.absoluteString)")
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            itemView.kf_setImageWithURL(URL, placeholderImage: nil,
                                        optionsInfo: [KingfisherOptionsInfoItem.ScaleFactor(UIScreen.mainScreen().scale)],
                                        progressBlock: { receivedSize, totalSize in
                },
                                        completionHandler: { image, error, cacheType, imageURL in
                                            
                                            //print("image downloaded")
            })
        }
        dispatch_async(dispatch_get_main_queue()) {
            itemView.alpha = 1.0
        }
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .Spacing)
        {
            return value * 1.1
        }
        return value
    }
    //MARK: - Load Data
    func autoScrollToNextImage()
    {
        var nextIndex = 0
        if self.carousel.currentItemIndex < self.numberOfDealImages
        {
            nextIndex = self.carousel.currentItemIndex + 1
        }
        self.carousel.scrollToItemAtIndex(nextIndex, duration: 1.5)
    }
}
